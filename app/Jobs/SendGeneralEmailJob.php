<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\GeneralMail;
use Illuminate\Support\Facades\Mail;

class SendGeneralEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $email_address;
    protected $email_content;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email_address, $email_content)
    {
        $this->email_address = $email_address;
        $this->email_content = $email_content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new GeneralMail($this->email_content);
        Mail::to($this->email_address)->send($email);
    }
}
