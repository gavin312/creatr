<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Wishlist extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reel()
    {
        return $this->belongsTo(Reel::class);
    }

    public function collection()
    {
        return $this->belongsTo(Collection::class);
    }

    public function getReels()
    {
        $collection_reels = DB::table('collection_reels')
            ->where('collection_id', $this->attributes['collection_id'])
            ->get()
            ->toArray();
        $reels = [];
        foreach ($collection_reels as $collection_reel) {
            $reel = Reel::find($collection_reel->reel_id);
            if ($reel != null && $reel->status != 'draft') {
                array_push($reels, $reel);
            }
        }
        return $reels;
    }
}
