<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Scopes\BannedReelScope;

class Reel extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];

    

    /**
     * The "booted" method of the model.
     * Adding global scope for reporting status
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new BannedReelScope);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'reel_tags','reel_id','tag_id')->withTimestamps();
    }

    public function platforms(){
        return $this->belongsToMany(Platform::class,'platform_reels','reel_id','platform_id')->withTimestamps();
    }

    public function licences(){
        return $this->belongsToMany(Licence::class,'licence_reels','reel_id','licence_id')->withPivot('price')->withTimestamps();
    }

    public function reportings(){
        return $this->belongsToMany(Reporting::class,'reporting_reels','reel_id','reporting_id')->withPivot('description', 'is_solved')->withTimestamps();
    }

    public function getTags(){
        $reel_tags = DB::table('reel_tags')->where('reel_id',$this->attributes['id'])->get()->toArray();
        $tags = [];
        foreach ($reel_tags as $reel_tag){
            $tag = Tag::find( $reel_tag->tag_id);
            if($tag != null){
                array_push($tags,$tag);
            }
        }
        return $tags;
    }

    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }


    public function rate(){
        return $this->belongsTo(Rate::class);
    }

    /**
     * @return mixed
     */
    public function getCategory(){
        return Category::find($this->attributes['category_id']);
    }


    public function collections(){
        return $this->belongsToMany(Collection::class,'collection_reels','reel_id','collection_id');
    }

    public function getCollections(){
        $reel_collections = DB::table('collection_reels')->where('reel_id',$this->attributes['id'])->get()->toArray();
        $collections = [];
        foreach ($reel_collections as $reel_collection){
            $collection = Collection::find( $reel_collection->collection_id);
            if($collection != null){
                array_push($collections,$collection);
            }
        }
        return $collections;
    }
    public function likeUsers(){
        return $this->belongsToMany(User::class,'user_like_reels','reel_id','user_id')->withTimestamps();
    }

    /**
     * @return HasMany comments
     */
    public function comments(){
        return $this->hasMany(Comment::class);
    }


    public function getComments(){
        return Comment::where('reel_id',$this->attributes['id'])->where('deleted_at',null)->get()->reverse();
    }

    public function getCommentsCount(){
        return Comment::where('reel_id',$this->attributes['id'])->where('deleted_at',null)->get()->count();
    }

    public function getCreatorName(){
        $creatorID = $this->attributes['user_id'];
        $creator = User::find($creatorID);
        return $creator->name;
    }

    /**
     * @return HasMany wishlist
     */
    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function qualities()
    {
        return $this->hasMany(Quality::class);
    }

    public function orderItems() {
        return $this->hasMany(OrderItem::class);
    }
}
