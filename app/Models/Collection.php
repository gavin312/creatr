<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Collection extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];


    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getUser(){
        return User::find($this->getAttribute('user_id'));
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'collection_tags','collection_id','tag_id')->withTimestamps();
    }

    public function platforms(){
        return $this->belongsToMany(Platform::class,'platform_collections','collection_id','platform_id')->withTimestamps();
    }

    public function reportings(){
        return $this->belongsToMany(Reporting::class,'reporting_collections','collection_id','reporting_id')->withPivot('description', 'is_solved')->withTimestamps();
    }

    public function getTags(){
        $collection_tags = DB::table('collection_tags')->where('collection_id',$this->attributes['id'])->get()->toArray();
        $tags = [];
        foreach ($collection_tags as $collection_tag){
            $tag = Tag::find( $collection_tag->tag_id);
            if($tag != null){
                array_push($tags,$tag);
            }
        }
        return $tags;
    }

    public function reels(){
        return $this->belongsToMany(Reel::class,'collection_reels','collection_id','reel_id');
    }

    public function getReels(){
        $collection_reels = DB::table('collection_reels')->where('collection_id',$this->attributes['id'])->get()->toArray();
        $reels = [];
        foreach ($collection_reels as $collection_reel){
            $reel = Reel::find( $collection_reel->reel_id);
            if($reel != null && $reel->status != 'draft'){
                array_push($reels,$reel);
            }
        }
        return $reels;
    }

    public function getReelsCount(){
        $collection_reels = DB::table('collection_reels')->where('collection_id',$this->attributes['id'])->get()->toArray();
        $reels = [];
        foreach ($collection_reels as $collection_reel){
            $reel = Reel::find( $collection_reel->reel_id);
            if($reel != null){
                array_push($reels,$reel);
            }
        }
        return count($reels);
    }



    public function getMostPopularReel(){
        $reels = $this->getReels();

        if(empty($reels)){
            return null;
        }else{
            $mostPopularReel = null;
            foreach ($reels as $reel){
                if($mostPopularReel == null){
                    $mostPopularReel = $reel;
                }else{
                    if($mostPopularReel->likes < $reel->likes){
                        $mostPopularReel = $reel;
                    }
                }
            }
            return $mostPopularReel;
        }
    }



    /**
     * @return mixed
     */
    public function getCategory(){
        return Category::find($this->attributes['category_id']);
    }

    public function likeUsers(){
        return $this->belongsToMany(User::class,'user_like_collections','collection_id','user_id')->withTimestamps();
    }

    public function getLikedUsers(){
        $collection_users = DB::table('user_like_collections')->where('collection_id',$this->attributes['id'])->get()->toArray();
        $users = [];
        foreach ($collection_users as $collection_user){
            $user = User::find( $collection_user->user_id);
            if($user != null){
                array_push($users,$user);
            }
        }
        return $users;
    }

    /**
     * @return HasMany comments
     */
    public function comments(){
        return $this->hasMany(Comment::class);
    }


    public function getComments(){
        return Comment::where('collection_id',$this->attributes['id'])->where('deleted_at',null)->get()->reverse();
    }

    public function getCommentsCount(){
        return Comment::where('collection_id',$this->attributes['id'])->where('deleted_at',null)->get()->count();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function get_random_three_reels()
    {
        return $this->reels()
            ->where('status', 'published')
            ->inRandomOrder()
            ->limit(3)
            ->get();
    }

    /**
     * @return HasMany wishlist
     */
    public function wishlist(){
        return $this->hasMany(Wishlist::class);
    }

    public function rate() {
        return $this->belongsTo(Rate::class);
    }

    public function getRate(){
        return Rate::find($this->attributes['rate_id']);
    }
}
