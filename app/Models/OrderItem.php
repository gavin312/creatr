<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\BannedReelScope;

class OrderItem extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function reel() {
        return $this->belongsTo(Reel::class)->withoutGlobalScope(BannedReelScope::class)->withTrashed();
    }

    public function licence() {
        return $this->belongsTo(Licence::class);
    }

    public function payoutTransactions()
    {
        return $this->hasMany(PayoutTransaction::class);
    }
}
