<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Scopes\BannedReelScope;

class PayoutTransaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function orderItem(){
        return $this->belongsTo(OrderItem::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function licence() {
        return $this->belongsTo(Licence::class);
    }

    public function reel() {
        return $this->belongsTo(Reel::class)->withoutGlobalScope(BannedReelScope::class)->withTrashed();
    }
}
