<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Licence extends Model
{
    use HasFactory,SoftDeletes;

    protected $casts =[
        "is_free"=>"boolean",
        "is_popular"=>"boolean"
    ];
    protected $guarded = [];
}


