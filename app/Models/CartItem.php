<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    function cart(){
        return $this->belongsTo(Cart::class);
    }

    function reel(){
        return $this->belongsTo(Reel::class);
    }

    function licence(){
        return $this->belongsTo(Licence::class);
    }

}
