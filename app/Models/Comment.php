<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];


    /**
     * @return BelongsTo
     */
    public function user(){
      return $this->belongsTo(User::class);
    }

    public function reel(){
        return $this->belongsTo(Reel::class);
    }

    public function collection(){
        return $this->belongsTo(Collection::class);
    }

    public function parent(){
        return $this->belongsTo(Comment::class,"parent","id");
    }

    public function toUser(){
        return $this->belongsTo(User::class,"to_user_id","id");
    }
}
