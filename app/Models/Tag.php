<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    use HasFactory,SoftDeletes;
    protected $guarded = [];

    public function reels(){
        return $this->belongsToMany(Reel::class,'reel_tags','tag_id','reel_id')->withTimestamps();
    }

    public function getReels(){
        $tag_reels = DB::table('reel_tags')->where('tag_id',$this->attributes['id'])->get()->toArray();
        $reels = [];
        foreach ($tag_reels as $tag_reel){
            $reel = Reel::find( $tag_reel->reel_id);
            if($reel != null && $reel->status == 'published' && $reel->only_in_collection == '0'){
                array_push($reels,$reel);
            }
        }
        return $reels;
    }

    public function collections(){
        return $this->belongsToMany(Collection::class,'collection_tags','tag_id','collection_id')->withTimestamps();
    }

    public function getCollections(){
        $tag_collections = DB::table('collection_tags')->where('tag_id',$this->attributes['id'])->get()->toArray();
        $collections = [];
        foreach ($tag_collections as $tag_collection){
            $collection = Collection::find( $tag_collection->collection_id);
            if($collection != null&&$collection->status == 'published'){
                array_push($collections,$collection);
            }
        }
        return $collections;
    }

    public function getTags(){
        $collection_tags = DB::table('collection_tags')->where('collection_id',$this->attributes['id'])->get()->toArray();
        $tags = [];
        foreach ($collection_tags as $collection_tag){
            $tag = Tag::find( $collection_tag->tag_id);
            if($tag != null){
                array_push($tags,$tag);
            }
        }
        return $tags;
    }

    public function getMostPopularReel(){
        $reels = DB::table('reel_tags')
            ->join('reels','reel_tags.reel_id', '=','reels.id')
            ->where('tag_id','=',$this->attributes['id'])
            ->where('status','=','published')
            ->where('only_in_collection','=','0')
            ->where('reels.is_banned','!=',1)
            ->orderBy('reels.views','DESC')
            ->limit(1)
            ->select('reels.id')
            ->get();
        if(count($reels) > 0){
            $reel = Reel::find($reels[0]->id);
            return $reel;
        }else{
            return null;
        }

    }

}
