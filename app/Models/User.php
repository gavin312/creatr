<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Billable;
use Stripe\Payout;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail
{
    use HasFactory, Notifiable, SoftDeletes, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'stripe_id',
        'pm_type',
        'pm_last_four',
        'trial_ends_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile(){
        return $this->hasOne(Profile::class,'user_id');
    }
    public function getProfile(){
        return Profile::where('user_id',$this->attributes['id'])->first();
    }

    public function getFollowers(){
        $user_followers = DB::table('user_follow')->where('following_id',$this->attributes['id'])->get()->toArray();
        $followers = [];
        foreach ($user_followers as $user_follower){
            $follower = User::find( $user_follower->follower_id);
            if($follower != null){
                array_push($followers,$follower);
            }
        }
        return $followers;
    }

    public function getFollowings(){
        $user_followings = DB::table('user_follow')->where('follower_id',$this->attributes['id'])->get()->toArray();
        $followings = [];
        foreach ($user_followings as $user_following){
            $following = User::find( $user_following->following_id);
            if($following != null){
                array_push($followings,$following);
            }
        }
        return $followings;
    }

    public function users(){
        return $this->belongsToMany(User::class,'user_follow','follower_id','following_id')->withTimestamps();
    }

    public function getReelsCount(){

        return Reel::where('user_id',$this->attributes['id'])->where('only_in_collection','0')->where('status','published')->get()->count();;
    }

    public function getCollectionsCount(){
       return Collection::where('user_id',$this->attributes['id'])->where('status','published')->get()->count();
    }

    public function getAllReelsCount(){
        return Reel::where('user_id',$this->attributes['id'])->get()->count();;
    }

    public function getAllCollectionsCount(){
        return Collection::where('user_id',$this->attributes['id'])->get()->count();
    }

    // likes reels
    public function reels(){
        return $this->belongsToMany(Reel::class,'user_like_reels','user_id','reel_id')->withTimestamps();
    }

    public function collections(){
        return $this->belongsToMany(Collection::class,'user_like_collections','user_id','collection_id')->withTimestamps();
    }

    public function created_reels()
    {
        return $this->hasMany(Reel::class);
    }

    public function get_most_viewed_reel()
    {
        return $this->created_reels()
            ->where('status', 'published')
            ->orderByDesc('views')
            ->first();
    }

    public function wishlist() {
        return $this->hadMany(Wishlist::class, 'user_id');
    }

    public function prizes() {
        return $this->belongsToMany(Prize::class, 'user_prizes', 'user_id', 'prize_id')->withTimestamps();
    }

    public function cart(){
        return $this->hasMany(Cart::class,"user_id");
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function payoutMethods(){
        return $this->hasMany(PayoutMethod::class);
    }

}
