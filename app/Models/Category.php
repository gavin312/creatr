<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    use HasFactory,SoftDeletes;


    public function getReelsCount(){
        return Reel::where('category_id',$this->attributes['id'])
            ->where('status','published')
            ->where('only_in_collection','0')
            ->get()
            ->count();
    }

    public function getCollectionCount(){
        return Collection::where('category_id',$this->attributes['id'])
            ->join("collection_reels","collections.id","=","collection_reels.collection_id")
            ->where('reel_id',"!=",null)
            ->where('status','published')
            ->get()
            ->count();
    }

    public function getReelAndCollectionCount(){
        return $this->getCollectionCount() + $this->getReelsCount();

    }

    public function getMostPopularReels(){
        $reels = Reel::where('category_id',$this->attributes['id'])
            ->where('status','published')
            ->where('only_in_collection','0')
            ->orderBy('views','ASC')
            ->limit(4)
            ->get();
        return $reels;
    }

    public function getMostPopularReel(){
        $reel = Reel::where('category_id',$this->attributes['id'])
            ->where('status','published')
            ->where('only_in_collection','0')
            ->orderBy('views','ASC')
            ->select('reels.id')
            ->limit(1)
            ->get();
        return $reel;
    }

}

