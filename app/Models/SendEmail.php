<?php

namespace App\Models;

use function Illuminate\Events\queueable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralMail;

class SendEmail extends Model
{
  protected $guarded = [];

  protected static function boot()
  {
    parent::boot();
    SendEmail::created(queueable(function ($sendEmail) {
      if ($sendEmail->user != null) {
        $user_email = $sendEmail->user->email;
        $user_name = $sendEmail->user->name;
        $data = [
          "title" => $sendEmail->email_title,
          "username" => $user_name,
          "messageBody" => $sendEmail->email_content
        ];
        
        Mail::to($user_email)->send(new GeneralMail($data));
      }
    }));
  }

  public function user()
  {
    return $this->belongsTo(User::class);
  }
}
