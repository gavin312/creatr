<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ReviewReelAction extends AbstractAction
{
  public function getTitle()
  {
    return 'Review';
  }

  public function getIcon()
  {
    return 'voyager-check';
  }

  public function getAttributes()
  {
    return [
      'class' => 'btn btn-sm btn-primary pull-right',
    ];
  }

  public function shouldActionDisplayOnDataType()
  {
    return $this->dataType->slug == 'reporting-reels';
  }

  public function getDefaultRoute()
  {
    return route('reporting-reels.review', array("id" => $this->data->{$this->data->getKeyName()}));
  }
}
