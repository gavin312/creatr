<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class SolveReelAction extends AbstractAction
{
  public function getTitle()
  {
    return $this->data->{'is_solved'} == 0 ? 'Pending' : 'Solved';
  }

  public function getIcon()
  {
    return $this->data->{'is_solved'} == 0 ? 'voyager-x' : 'voyager-check';
  }

  public function getAttributes()
  {
    return $this->data->{'is_solved'} == 0 ? [
      'class' => 'btn btn-sm btn-primary pull-right',
    ] : [
      'class' => 'btn btn-sm btn-success pull-right disabled',
    ];
  }

  public function shouldActionDisplayOnDataType()
  {
    return $this->dataType->slug == 'reporting-reels';
  }

  public function getDefaultRoute()
  {
    return route('reporting-reels.solve', array("id" => $this->data->{$this->data->getKeyName()}));
  }
}
