<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;
use App\Models\Reel;
use Illuminate\Support\Facades\DB;

class RemoveReelAction extends AbstractAction
{
  public function getTitle()
  {
    $reel_id = $this->data->{'reel_id'};
    $is_banned = DB::table('reels')->where('id', $reel_id)->first()->is_banned;
    return $is_banned == 0 ? 'Remove' : 'Banned';
  }

  public function getIcon()
  {
    $reel_id = $this->data->{'reel_id'};
    $is_banned = DB::table('reels')->where('id', $reel_id)->first()->is_banned;
    return $is_banned == 0 ? 'voyager-trash' : 'voyager-warning';
  }

  public function getAttributes()
  {
    return [
      'class' => 'btn btn-sm btn-danger pull-right',
    ];
  }

  public function shouldActionDisplayOnDataType()
  {
    return $this->dataType->slug == 'reporting-reels';
  }

  public function getDefaultRoute()
  {
    return route('reporting-reels.remove', array("id" => $this->data->{$this->data->getKeyName()}));
  }
}
