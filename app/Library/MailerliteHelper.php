<?php
namespace App\Library;
use App\Models\User;
use Faker\Factory;
use http\Client\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Image;
use Intervention\Image\ImageManager;
use League\Flysystem\File;
use TCG\Voyager\Voyager;

//require 'vendor/autoload.php';

class MailerliteHelper{

    Private $groupsApi;
    public function __construct() {
        $this->groupsApi = (new \MailerLiteApi\MailerLite(env('MAILERLITE_API_KEY')))->groups();
    }


    public function getSubscriptionGroups(){

        $groupArray =  $this->groupsApi->get();
        return ResponseMessage::successMessageWithCustomData('Success', 'groupArray',$groupArray->toArray());
    }

    public function subscribeCreatr($email){
//        dd($email,setting('general.subscribeGroupID'));

        $subscriber = [
            'email' => $email,
            'fields'=>[]
        ];

        $response = $this->groupsApi->addSubscriber(setting('general.subscribeGroupID'),$subscriber);
        if ($response->type == 'active'){
            return ResponseMessage::successMessage('Success!','Subscribe Success!');
        }else{
            return ResponseMessage::failMessage('Fail!','Subscribe Fail, Please try again');
        }

    }

    public function removeSubscriberByEmail($email){
        $subscribers =  $this->groupsApi->getSubscriber(setting('general.subscribeGroupID'),$email);

        $selectedSubscriber = null;
        foreach ($subscribers as $subscriber){
            if( strtolower( $subscriber->email) == strtolower($email)){
                $selectedSubscriber = $subscriber;
            }
        }

        if( $selectedSubscriber != null){
            $this->groupsApi->removeSubscriber(setting('general.subscribeGroupID'),$selectedSubscriber->id);
        }

        return true;

    }

}
