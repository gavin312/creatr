<?php
namespace App\Library;
use App\Models\Comment;
use App\Models\Order;
use App\Models\PayoutTransaction;
use App\Models\Reel;
use App\Models\User;
use Faker\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use League\Flysystem\File;
use PhpParser\Node\Expr\Array_;
use Stripe\Exception\ApiErrorException;
use TCG\Voyager\Voyager;

class Helper{

  static public function clean($string){
      $string = str_replace(' ', '_', $string);
      $cleanerName = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
      $newName = "";
      if ($cleanerName == ""){
          $faker = Factory::create();
          $newName = strtolower($faker->userName);
      }else{
          $newName =  strtolower($cleanerName);
      }

      $userArray = User::where('name',$newName)->get()->toArray();
      if(empty($userArray)){
          return  $newName;
      }else{
          return $newName.self::randomNumbers();
      }
  }

    static function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    static function randomNumbers(){
        $digits = 4;
        return rand(pow(10, $digits-1), pow(10, $digits)-1);
    }

    static function downloadImage($url){
        $contents = file_get_contents($url);
        $fileName ='users/'.self::randomPassword().'.jpg';
        $name =  'public/'.$fileName;
        Storage ::put($name, $contents);
        return $fileName;
    }

    static function downloadImageIntoThumbNails($url){
        $contents = file_get_contents($url);
        $fileName ='thumbnails/'.self::randomPassword().'.jpg';
        $name =  'public/'.$fileName;
        Storage ::put($name, $contents);
        return $fileName;
    }

    static function getImageFromBase64($fileString){

        $storageDirectory = public_path('/storage');
        $newFileName = 'thumbnails/'.self::randomPassword().'.jpg';
        $img = Image::make($fileString)->resize(477, 270)->save($storageDirectory.'/'.$newFileName);

        return $newFileName;
    }


    static function downloadImageIntoPreviews($url){

        $contents = file_get_contents($url);
        $fileName ='previews/'.self::randomPassword().'.gif';
        $name =  'public/'.$fileName;
        Storage ::put($name, $contents);
        return $fileName;
    }

    static function getSiteGeneralSetting(){
        $companyInfo = [
            "entityName"=>setting('general.entity_name'),
            "companyAddressLine1"=>setting('general.company_address_line_1'),
            "companyAddressLine2"=>setting('general.company_address_line_2'),
            "companyCity"=>setting('general.company_city'),
            "companyRegion"=>setting('general.company_region'),
            "companyPostalCode"=>setting('general.company_postal_code'),
            "companyCountry"=>setting('general.company_country'),
            "companyEmail"=>setting('general.company_email'),
            "companyTaxId"=>setting('general.company_tax_id'),
            "companyLogo"=>setting('site.logo'),
            "companyNavLogo"=>setting('site.navigationbar_logo'),
        ];

        return  $companyInfo;
    }

    static function convertImageToBase64($path){
        $logoImage =  "storage/".$path;
        $type = pathinfo($logoImage, PATHINFO_EXTENSION);
        $logoData = file_get_contents( $logoImage);

        $logo = 'data:image/'.$type . ';base64,'.base64_encode($logoData);
        return $logo;
    }

    static function isFollowing($followerID, $followingID){
        $result = DB::table('user_follow')->where('follower_id',$followerID)->where('following_id',$followingID)->get()->toArray();

        if(count($result) > 0){
            return true;
        }else{
            return false;
        }
    }

    static function reelLikedByUser($reelID, $userID){
        $result = DB::table('user_like_reels')->where('user_id',$userID)->where('reel_id',$reelID)->get()->toArray();

        if(count($result) > 0){
            return true;
        }else{
            return false;
        }
    }

    static function collectionLikedByUser($collectionID, $userID){
        $result = DB::table('user_like_collections')->where('user_id',$userID)->where('collection_id',$collectionID)->get()->toArray();

        if(count($result) > 0){
            return true;
        }else{
            return false;
        }
    }

    static function convertNameToHandle(String $name){
        $temp1 = strtolower($name);
        $temp2 = preg_replace('/[^A-Za-z0-9\-]/', ' ', $temp1);
        $temp3 = str_replace(' ', '-', $temp2); // Replaces all spaces with hyphens.
        $temp4 = preg_replace('/[\-]{2,}/', '-', $temp3);

        if(substr($temp4, -1) == "-"){
            $temp4 = substr($temp4, 0, -1);
        }

        if(substr($temp4, 0) == "-"){
            $temp4 = substr($temp4, 1, -1);
        }

      return $temp4;
    }

    static function formatComment(Array $comments){
      $formatedComments = [];
      foreach ($comments as $comment){

          $author = User::find($comment['user_id']);
          if($author != null){
              $author["profile"] = $author->getProfile();
              $parent = null;
              if($comment['parent'] != null){
                  $parent = Comment::find($comment['parent']);
              }
              $toUser = null;
              if($comment['to_user_id'] != null){
                  $toUser = User::find($comment['to_user_id']);
                  $toUser["profile"] = $toUser->getProfile();
              }

              $formatedComment = compact(['comment','author','parent','toUser']);
              array_push($formatedComments,$formatedComment);
          }
      }

        return json_encode($formatedComments);
    }

    /**
     * @param String $urlHandle
     * when the url is occupied true, then false
     * @return bool
     */
    static function checkUrlOccupied(String $urlHandle){

       $count =  Reel::where("url_handle",$urlHandle)->get()->count();

       if($count > 0){
           return true;
       }else{
           return false;
       }
    }


    static function convertRelativePathToAbsolute(String $url){

        if(str_starts_with($url,"https://") || str_starts_with($url,"http://")){
            return $url;
        }else{
            return "https://".$url;
        }
    }

    static public function generateInvoiceFromOrder(Order $order){

        $str = date('Ymd') . str_pad($order->id, 5, '0', STR_PAD_LEFT);
        $order->invoice()->create([
            'invoice_number'=>$str,
            'user_id'=>$order->user_id
        ]);

        return $order->invoice()->first();
    }


    static public function transfer(PayoutTransaction $payoutTransaction){
        $user = $payoutTransaction->user()->first();
        $payoutMethod = $user->payoutMethods()->first();
        $stripe = new \Stripe\StripeClient(
            env("STRIPE_SECRET")
        );
        if($payoutMethod != null){
            try {
                $response = $stripe->transfers->create([
                    'amount' =>$payoutTransaction->payout_amount,
                    'currency'=>'usd',
                    'destination' => $payoutMethod->account_id,
                    "metadata" => [
                        "order_id" => $payoutTransaction->order_id,
                        "reel_id"=>$payoutTransaction->reel_id,
                        "licence_id"=>$payoutTransaction->licence_id
                    ]
                ]);
                return [
                    "state"=>true,
                    "content"=>$response->id,
                ];
            }catch (ApiErrorException $exception){
                return [
                    "state"=>false,
                    "content"=>null,
                ];
            }
        }else{
            return [
                "state"=>false,
                "content"=>null,
            ];
        }

    }
}
