<?php
namespace App\Library;
use Composer\Util\Http\Response;

class ResponseMessage{

    /**
     * return message to
     *
     * @param $message
     * @param $content
     * @return mixed
     */
   static public function successMessage($message, $content){

        return response(array("State"=>true, 'Msg' => $message,"Data"=> $content));
    }

    /**
     * return customize data
     *
     * @param $message
     * @param $key
     * @param $value
     * @return mixed
     */
    static public function  successMessageWithCustomData ($message, $key,$value){
        return response(array("State"=>true, 'Msg' => $message, $key =>$value));
    }

    /**
     * @param $errormessage
     * @param $content
     * @return mixed
     */
   static public function failMessage($errormessage, $content){

        return response(array("State"=>false, 'Msg' => $errormessage,"Data"=> $content));
    }

}
