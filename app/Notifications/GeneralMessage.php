<?php

namespace App\Notifications;

use App\Mail\General;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class GeneralMessage extends Notification
{
    use Queueable;

    protected $generalMessage;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($generalMessage)
    {
        $this->$generalMessage = $generalMessage;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return General
     */
    public function toMail($notifiable)
    {
        return (new General($this->generalMessage))->to($notifiable->email);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
