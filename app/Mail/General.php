<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class General extends Mailable
{
    use Queueable, SerializesModels;

    private $generalMessage;

    /**
     * Create a new message instance.
     *
     * @param $generalMessage
     */
    public function __construct($generalMessage)
    {
        $this->$generalMessage = $generalMessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $data = [
            'data' => "abc",
            'password' => "def"
        ];
        return $this->view('emails.general', $data);
    }
}
