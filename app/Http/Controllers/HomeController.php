<?php

namespace App\Http\Controllers;

use App\Library\Helper;
use App\Models\Collection;
use App\Models\Reel;
use App\Models\Wishlist;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function getItemByPage(Request $request)
    {
//        $creators_per_page = 2;
        $collections_per_page = 4;
        $reels_per_page = 16;

        $pageSeed = $request->get('pageSeed');
        $pageindex = intval( $request->get('pageIndex'));

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

//        $creatorStartIndex = ($pageindex-1) * $creators_per_page;
        $reelsStartIndex = ($pageindex-1) * $reels_per_page;
        $collectionStartIndex = ($pageindex-1) * $collections_per_page;


//        $creators = User::with('profile')
//            ->inRandomOrder($pageSeed)
//            ->skip($creatorStartIndex)
//            ->take($creators_per_page)
//            ->get()
//            ->map(function ($creator) {
//                $creator['reel'] = $creator->get_most_viewed_reel();
//                $creator['type'] = "creator";
//                $creator['profile'] = $creator->getProfile();
//                return $creator;
//            })
//            ->toArray();
        $userId = Auth::id();
        $collections = Collection::with('user.profile', 'category', 'reels')
            ->withCount('comments')
            ->inRandomOrder($pageSeed)
            ->where('status', 'published')
            ->skip($collectionStartIndex)
            ->take($collections_per_page)
            ->get()
            ->map(function ($collection) use ($userId) {
                    $collection['reel'] = $collection->getMostPopularReel();
                    $collection['reels'] = $collection->getReels();
                    $collection['type'] = "collection";
                    $collection['reelcount'] = count($collection->getReels());
                    $collection->tags;
                    $creatorID = $collection->user_id;
                    $collection['is_creator'] = $userId === $creatorID ? true : false;
                    $collection['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('collection_id', $collection->id)->exists();

                    return $collection;
            })
            ->toArray();


        $collections = array_filter($collections,function($collection){
           return $collection['reelcount'] > 0;
        });

        $reels = Reel::with('user.profile', 'tags')
            // ->withCount('comments')
            ->where('status', 'published')
            ->inRandomOrder($pageSeed)
            ->skip($reelsStartIndex)
            ->take($reels_per_page)
            ->get()
            ->map(function ($reel) use ($userId) {
                $reel['tags'] = $reel->getTags();
                $reel['creator'] = $reel->getCreatorName();
                $reel_licences = $reel->licences;
                // Check if the reel is free
                if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                    $reel->qualities->makeVisible(['link']);
                } else {
                    $reel->qualities;
                }
                $reel['type'] = "reel";
                $creatorID = $reel->user_id;
                $reel['is_creator'] = $userId === $creatorID ? true : false;
                $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();

                return $reel;
            })
            ->toArray();

//            ->orderByDesc('created_at')
//            ->simplePaginate($reels_per_page);

//        $user = null;
//        if (Auth::check()) {
//            $user = Auth::user();
//        }
//
//        collect($reels_page->items())->each(function ($reel) use ($user) {
//            if ($user) {
//                $reel['is_liked'] = Helper::reelLikedByUser($reel->id, $user->id);
//            } else {
//                $reel['is_liked'] = false;
//            }
//        });

//        $data =  array_merge($reels,$creators,$collections);
        $data =  array_merge($reels,$collections);
        shuffle($data);


        return response()->json([
            "response"=> compact(['data','pageSeed'])
            ],200);
    }

    public function  getPopularCreators(){

        $userIdArray = Reel::select("user_id")
            ->groupBy("user_id")
            ->orderByRaw('COUNT(*) DESC')
            ->limit(10)
            ->get()
            ->toArray();

        $userIds = [];
        foreach ($userIdArray as $item){
            array_push($userIds,$item["user_id"]);
        }

        $creators = User::with("profile")->find($userIds);
        return response()->json([
            "response"=> compact(['creators'])
        ],200);

    }
}
