<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Rate;
use App\Models\Reel;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Jobs\SendGeneralEmailJob;

class CollectionController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified'])->only(['store','edit','update','likeCollection','destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {

        $mostPopularReel = Reel::orderBy('views',"DESC")->firstOrFail();
        $mostPopularReel['creatorName'] =  $mostPopularReel->getCreatorName();

        $tempCollections = Collection::where("status","published")->get();
        $count = 0;
        foreach ($tempCollections as $tempCollection){
            if($tempCollection->getReelsCount() > 0){
                $count ++;
            }
        }

        $collectionsInfo = json_encode(["collection_count"=> $count]);

        return view('collection.index',compact(['mostPopularReel','collectionsInfo']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {


       $data =  $request->validate([
            'title' => 'required',
           'category_id' => 'required',
           'rate_id' => 'required'
        ]);
       $currentUser = Auth::user();
       if(!$this->checkCollectionNameAvailable($data['title'])){
           return Response()->json([
               'response'=>[
                   'errormsg'=>"Title is Occupied! Please try Another one"
               ]
           ],409);
       }

       $collection = Collection::create([
           'title' => $data['title'],
           'category_id' => $data['category_id'],
           'rate_id' => $data['rate_id'],
           'user_id' =>$currentUser->id,
           'description'=>"",
           'status'=>'published'
       ]);

       $this->getCollectionHandleFromCollectionTitle($collection);

       return response()->json([
           "response"=>[
               'msg'=>'Collection Created',
               'collection_id'=>$collection->url_handle,
           ],
       ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param String $collection_title
     * @return void
     */
    public function show(String $collection_url_handle)
    {

        $collection = Collection::where('url_handle',$collection_url_handle)->firstOrFail();
        $tags = json_encode($collection->getTags()) ;
        $creator = User::find($collection->user_id);
        $profile = $creator->getProfile();

        $isCreator = false;
        $collection->views = $collection->views + 1;
        $collection->save();

        $isFollowing = false;
        $category = $collection->getCategory();
        $rate = $collection->getRate();

        $platforms = $collection->platforms;

        $showCollection = true;
        if($collection->status == "draft"){
            $showCollection = false;
        }

        if (Auth::check()){

            $user = Auth::user();

            $isCreator = ($user->id === $collection->user_id);
            if(!$isCreator){
                $isFollowing = Helper::isFollowing($user->id,$collection->user_id);

            }else{
                $showCollection = true;
            }
            $likeCollection = Helper::collectionLikedByUser($collection->id,$user->id);

        }else{
            $isCreator = false;
            $likeCollection = false;
            $user = null;
        }
        if(!$showCollection){
            abort(404);
        }
        $comments = $collection->getComments()->toArray();
        $formatComment = Helper::formatComment($comments);
        $reelCount = $creator->getReelsCount();
        $creator_info = json_encode(compact(['creator','profile','isCreator','isFollowing','user','reelCount']));
        $reels = $collection->getReels();
        $collection_info = json_encode(compact(['collection','reels','likeCollection','platforms']));

        return view('collection.show',compact(['tags','collection_info','creator_info','category','rate','collection','formatComment']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param String $collection_title
     * @return Response
     */
    public function edit(String $collection_url_handle)
    {

        $collection = Collection::where('url_handle',$collection_url_handle)->firstOrFail();

        $author = $collection->getUser();
        $user = Auth::user();
        if($user->id === $author->id){
            $tags = Tag::orderBy('name','ASC')->get();
            $categories = Category::all();
            $rates = Rate::orderBy('rank')->get();
            $selectedTags =  $collection->getTags();
            $reels = $collection->getReels();
            // selectedPlatforms can be empty
            $selectedPlatforms = $collection->platforms;

            $collection_info = json_encode( compact(['collection','tags','categories','rates','selectedTags','reels','selectedPlatforms']));

            return view('collection.edit',compact(['collection_info']));
        }else{
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Collection $collection
     * @return JsonResponse
     */
    public function update(Request $request, Collection $collection)
    {

        $data = $request->validate([
            'title'=>'required',
            'description'=>'sometimes',
            'status'=>'required',
            'tags'=>'sometimes',
            'reels'=>'required',
            'category_id'=>'required',
            'rate_id' => 'required',
            'platforms' => 'sometimes'
        ]);

        $user = Auth::user();

        $isCreator = ($user->id === $collection->user_id);
        if($isCreator){

            if(!$this->checkCollectionNameAvailable($data['title'],$collection->id)){
                return Response()->json([
                    'response'=>[
                        'errormsg'=>"Title is Occupied! Please try Another one"
                    ]
                ],409);
            }

            $collection['title'] = $data['title'];
            $collection['description'] = $data['description'];
            $collection['status'] = $data['status'];
            $collection['category_id'] = $data['category_id'];
            $collection['rate_id'] = $data['rate_id'];
            $collection->save();


            $this->getCollectionHandleFromCollectionTitle($collection);

            $reels = $data['reels'];
            $collection->reels()->sync($reels);

            $thumbNail = Reel::find($reels[0])->thumbnail;
            $tags = $data['tags'];
            $tagIDs = TagController::createTagsFromTagsArray($tags,$thumbNail);
            $collection->tags()->sync($tagIDs);

            $platforms = $data['platforms'];
            $collection->platforms()->sync($platforms);

            return Response()->json([
                'response'=>[
                    'collectionID'=>$collection->url_handle,
                ]
            ],200);
        }else{
            return Response()->json([
                'response'=>[
                    'msg'=>'Permission denied!'
                ]
            ],403);
        }
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Collection $collection)
    {
        if (Auth::check()){

            $user = Auth::user();

            $isCreator = ($user->id === $collection->user_id);

            if($isCreator){
                $collection->delete();
                return Response()->json([
                    'response'=>[
                        'msg'=>'Delete Success'
                    ]
                ],200);
            }else{
                return Response()->json([
                    'response'=>[
                        'msg'=>'Permission denied!'
                    ]
                ],403);
            }

        }
    }

    public function likeCollection(Request $request){
        $user = Auth::user();
        $collection_id = $request->get("collection_id");
        $collection = Collection::find($collection_id);
        if(Helper::collectionLikedByUser($collection->id,$user->id)){
            $collection->likeUsers()->detach($user->id);
            $msg = "Unlike Successfully! ";
            $like = false;
            $collection->likes = $collection->likes - 1;
            $collection->save();
        }else{
            $collection->likeUsers()->attach($user->id);
            $msg = "Like Successfully! ";
            $collection->likes = $collection->likes + 1;
            $collection->save();
            $like = true;

            // Send email
            $temp_collection = Collection::with("user:id,email,name,activity_notification")->where("id", $collection_id)->first();
            $send_to_user = [
                "email" => $temp_collection["user"]["email"],
                "name" => $temp_collection["user"]["name"],
                "activity_notification" => $temp_collection["user"]["activity_notification"]
            ];

            if ($send_to_user["activity_notification"] == 1) {
                $data = [
                    'title' => 'New Notification',
                    'username' => $send_to_user["name"],
                    'messageBody' => 'Good job! Someone likes your collection ['.$temp_collection->title.']!'
                ];
                dispatch(new SendGeneralEmailJob($send_to_user["email"], $data));
            }
        }

        return Response()->json([
            'response'=>[
                'msg'=>$msg,
                'likes'=>$collection->likes,
                'like'=>$like
            ]
        ],200);

    }

    public function checkCollectionNameAvailable(String $name,$collectionId = null){
        $validate = true;
        $collection = null;
        $collections = DB::table('collections')->where('title',$name)->get()->toArray();
        if(count($collections) == 0 ){
            return $validate;
        }else{
            if($collectionId){
                $collection = Collection::find($collectionId);
                if($collection->title == $name){
                    $validate = true;
                }else{
                    $validate = false;
                }
            }else{
                $validate = false;
            }
            return $validate;
        }
    }

    public function getAllCollectionsByRandomOrder(Request $request){
        $pageindex = intval($request->get("collectionPageIndex")) ;
        $pageSeed = intval($request->get("collectionPageSeed")) ;
        $pagesize = intval($request->get("collectionPageSize")) ;

        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $tempcollections = Collection::where('status','published')
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get();

        $collections = [];
        foreach ($tempcollections as $collection){
            $collection['reel'] = $collection->getMostPopularReel();
            $collection['reels'] = $collection->getReels();
            if(count($collection['reels'] ) > 0){
                array_push($collections,$collection);
            }
        }

        return response()->json([
            "response"=>compact(['collections','pageSeed'])
        ],200);

    }


    public function getCollectionHandleFromCollectionTitle(Collection $collection){
        $temp_url_handle = Helper::convertNameToHandle($collection->title);
        $url_handle = $temp_url_handle;
        if($temp_url_handle  == $collection->url_handle){
            return;
        }else{

            $num = 0;
            while(true){

                if (Helper::checkUrlOccupied($url_handle)){
                    $url_handle = $temp_url_handle.$num;
                }else{
                    $collection->url_handle = $url_handle;
                    $collection->save();
                    return;
                }
                $num++;

            }
        }
    }


}
