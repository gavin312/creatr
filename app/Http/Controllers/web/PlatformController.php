<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Platform;
use App\Models\Rate;
use Illuminate\Http\Request;

class PlatformController extends Controller
{
    public function getAllPlatforms(){
        $platforms = Platform::orderBy("rank")->get();

        return Response()->json([
            "response"=>compact(["platforms"])
        ],200);
    }
}
