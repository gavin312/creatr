<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\Reel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['getUserLoginStatus', 'getUserReels', 'getUserCollections', 'getUserUploadReels', 'getUserUploadCollections']);
    }

    public function getUserLoginStatus(Request $request) {
        $userId = Auth::id();

        return response()->json([
            'response' => compact(['userId'])
        ],200);
    }

    public function getUserReels(Request $request)
    {
        $user = Auth::user();

        if ($request->input("pageIndex") != null) {
            $pageindex = intval($request->input("pageIndex"));
            $pageindex = $pageindex < 1 ? 1 : $pageindex;
            $pagesize = intval($request->input("pageSize"));;
            $startIndex = ($pageindex - 1) * $pagesize;

            $reels = Reel::where('user_id', $user->id)->where('deleted_at', null)->skip($startIndex)->take($pagesize)->get();

            return response()->json([
                'response' => compact(['reels'])
            ], 200);
        } else {
            $reels = Reel::where('user_id', $user->id)->get();
            return response()->json([
                'response' => compact(['reels'])
            ], 200);
        }
    }

    public function getUserCollections(Request $request)
    {
        $user = Auth::user();
        if ($request->input("pageIndex") != null) {
            $pageindex = intval($request->input("pageIndex"));
            $pageindex = $pageindex < 1 ? 1 : $pageindex;
            $pagesize = intval($request->input("pageSize"));;
            $startIndex = ($pageindex - 1) * $pagesize;

            $collections = Collection::where('user_id', $user->id)->skip($startIndex)->take($pagesize)->get();
            foreach ($collections as $collection) {
                $collection['reel'] = $collection->getMostPopularReel();
            }

            return response()->json([
                'response' => compact(['collections'])
            ], 200);
        } else {
            $collections = Collection::where('user_id', $user->id)->get();
            foreach ($collections as $collection) {
                $popularReel = $collection->getMostPopularReel();
                if (!empty($popularReel)) {
                    $collection['thumbnail'] = $popularReel->thumbnail;
                } else {
                    $collection['thumbnail'] = null;
                }
            }
            return response()->json([
                'response' => compact(['collections'])
            ], 200);
        }
    }

    public function getUserUploadReels()
    {

        $user = Auth::user();

        $reels = Reel::where('user_id', $user->id)->paginate(10);

        return response()->json($reels, 200);
    }

    public function getUserUploadCollections()
    {
        $user = Auth::user();

        $collections = Collection::where('user_id', $user->id)->where('deleted_at', null)->paginate(10);
        $newCollections = $collections->items();
        foreach ($newCollections as $gavinCollection) {
            $gavinCollection['reel'] = $gavinCollection->getMostPopularReel();
            $gavinCollection['reelsCount'] = $gavinCollection->getReelsCount();
        }

        $collections->data = $newCollections;

        return response()->json($collections, 200);
    }

    public function getrandomReelsOtherThan(Request $request)
    {

        $pageindex = intval($request->get("pageIndex"));
        $pageSeed = intval($request->get("pageSeed"));
        $pagesize = intval($request->get("pageSize"));;
        $creatorID = $request->get("creatorID");
        $startIndex = ($pageindex - 1) * $pagesize;

        if ($pageSeed == "") {
            $pageSeed = strval(random_int(1, 100));
        }

        $reelID = $request->get('reelID');

        if ($reelID) {
            $reels = Reel::where('id', '!=', $reelID)->where('user_id', $creatorID)->where('status', 'published')->skip($startIndex)->take($pagesize)->inRandomOrder($pageSeed)->get();
        } else {
            $reels = Reel::where('status', 'published')->where('user_id', $creatorID)->skip($startIndex)->take($pagesize)->inRandomOrder($pageSeed)->get();
        }

        return response()->json([
            "response" => compact(['reels', 'pageSeed'])
        ], 200);
    }
}
