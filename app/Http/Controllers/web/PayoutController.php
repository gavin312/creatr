<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\PayoutMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Payout;
use Stripe\Stripe;
use TusPhp\Response;

class PayoutController extends Controller
{

//    https://stripe.com/docs/api/accounts/create#create_account-type

    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    public function createTestAccount(Request $request){

        $stripe = new \Stripe\StripeClient(
            env("STRIPE_SECRET")
        );
        $response = $stripe->accounts->create([
            'type' => 'custom',
            'country' => 'US',
            'email' => 'salamender336@gmail.com',
            'business_type'=>"individual",
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
            'business_profile' => [
                'mcc' => '5815',
                'url'=>"www.my.creatrhq.com",
                "support_address"=>[
                    'city'=>'test',
                    'country'=>'US',
                    'line1'=>'address 1',
                    'line2'=>'address 2',
                    'postal_code'=>'12345',
                    'state'=>'NY',
//                    首字母
                ],
            ],
            'individual'=>[
                'address'=>[
                    'city'=>'test',
                    'country'=>'US',
                    'line1'=>'address 1',
                    'line2'=>'address 2',
                    'postal_code'=>'12345',
                    'state'=>'NY',
                ],
                'dob'=>[
                    'day'=>12,
                    'month'=>06,
                    'year'=>1997
                ],
                'email' => 'salamender336@gmail.com',
                'first_name'=>'first name',
                'last_name'=>'last name',
                'phone'=>"859-669-1849",
                'ssn_last_4'=>1234,
                'id_number'=>123121234
            ],
            'tos_acceptance'=>[
                'date'=>time(),
                'ip'=>$request->ip(),
            ],
            'external_account'=>[
                'object'=>"bank_account",
                'country'=>"US",
                'currency'=>"usd",
                "account_holder_name"=>"Gavin test2",
                "account_holder_type"=>"individual",
                "routing_number"=>"110000000",
                "account_number"=>"000123456789"
            ]
        ]);


        return response()->json([
            'response'=>compact(['response'])
        ],200);
//        return Response()->json()
    }

    public function updateAccount(Request $request){
        $data = $request->validate([
            "firstName"=>"required",
            "lastName"=>"required",
            "streetAddress"=>"required",
            "state"=>"required",
            "postalCode"=>"required",
            "country"=>"required",
            "city"=>"required",
            "phoneNumber"=>"required",
            "dateOfBirth"=>"required",
            "payoutBankAccount"=>"sometimes",
            "routingNumber"=>"sometimes",
            "accountNumber"=>"sometimes",

            "dob_day"=>"required",
            "dob_month"=>"required",
            "dob_year"=>"required",

            "ssnNumber"=>"sometimes",
        ]);

        $stripe = new \Stripe\StripeClient(
            env("STRIPE_SECRET")
        );
        $user = Auth::user();
        $firstPaymentMethod = $user->payoutMethods()->first();
        $param = [
            'email' => $user->email,
            'business_type'=>"individual",
            'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
            ],
            'business_profile' => [
                'mcc' => '5815',
                'url'=>'www.my.creatrhq.com/hq/'.$user->name,
                "support_address"=>[
                    'city'=>$data["city"],
                    'country'=>'US',
                    'line1'=>$data["streetAddress"],
                    'line2'=>'',
                    'postal_code'=>$data["postalCode"],
                    'state'=>$data["state"],
                ],
            ],
            'tos_acceptance'=>[
                'date'=>time(),
                'ip'=>$request->ip(),
            ],
            'individual'=>[
                'address'=>[
                    'city'=>$data["city"],
                    'country'=>'US',
                    'line1'=>$data["streetAddress"],
                    'line2'=>'',
                    'postal_code'=>$data["postalCode"],
                    'state'=>$data["state"],
                ],
                'dob'=>[
                    'day'=>$data["dob_day"],
                    'month'=>$data["dob_month"],
                    'year'=>$data["dob_year"]
                ],
                'email' => $user->email,
                'first_name'=>$data["firstName"],
                'last_name'=>$data["lastName"],
                'phone'=>$data["phoneNumber"],
            ],

        ];

        if(!empty($data["ssnNumber"])){
            $param['individual']["ssn_last_4"] = substr($data["ssnNumber"], -4);
            $param['individual']["id_number"] = $data["ssnNumber"];
        }

        if(!empty($data["payoutBankAccount"]) && !empty($data["routingNumber"]) && !empty($data["accountNumber"]) ){
            $param["external_account"] = [
                'object'=>"bank_account",
                'country'=>"US",
                'currency'=>"usd",
                "account_holder_name"=>$data["payoutBankAccount"],
                "account_holder_type"=>"individual",
                "routing_number"=>$data["routingNumber"],
                "account_number"=>$data["accountNumber"]
            ];
        }

        $response = $stripe->accounts->update($firstPaymentMethod->account_id,$param);

        return Response()->json([
            'response'=>[
                'state'=>true,
                'msg'=>"Account created! Please provide your verified Profile!"
            ]
        ],200);
    }

    public function createAccount(Request $request){
        $data = $request->validate([
            "firstName"=>"required",
            "lastName"=>"required",
            "streetAddress"=>"required",
            "state"=>"required",
            "postalCode"=>"required",
            "country"=>"required",
            "city"=>"required",
            "phoneNumber"=>"required",
            "dateOfBirth"=>"required",
            "payoutBankAccount"=>"required",
            "routingNumber"=>"required",
            "accountNumber"=>"required",
            "dob_day"=>"required",
            "dob_month"=>"required",
            "dob_year"=>"required",
            "ssnNumber"=>"required",
        ]);


        $stripe = new \Stripe\StripeClient(
            env("STRIPE_SECRET")
        );

            $user = Auth::user();
            $response = $stripe->accounts->create([
                'type' => 'custom',
                'country' => 'US',
                'email' => $user->email,
                'business_type'=>"individual",
                'capabilities' => [
                    'card_payments' => ['requested' => true],
                    'transfers' => ['requested' => true],
                ],
                'business_profile' => [
                    'mcc' => '5815',
                    'url'=>'www.my.creatrhq.com/hq/'.$user->name,
                    "support_address"=>[
                        'city'=>$data["city"],
                        'country'=>'US',
                        'line1'=>$data["streetAddress"],
                        'line2'=>'',
                        'postal_code'=>$data["postalCode"],
                        'state'=>$data["state"],
                    ],
                ],
                'individual'=>[
                    'address'=>[
                        'city'=>$data["city"],
                        'country'=>'US',
                        'line1'=>$data["streetAddress"],
                        'line2'=>'',
                        'postal_code'=>$data["postalCode"],
                        'state'=>$data["state"],
                    ],
                    'dob'=>[
                        'day'=>$data["dob_day"],
                        'month'=>$data["dob_month"],
                        'year'=>$data["dob_year"]
                    ],
                    'email' => $user->email,
                    'first_name'=>$data["firstName"],
                    'last_name'=>$data["lastName"],
                    'phone'=>$data["phoneNumber"],
                    'ssn_last_4'=>substr($data["ssnNumber"], -4),
                    'id_number'=>$data["ssnNumber"]
                ],
                'tos_acceptance'=>[
                    'date'=>time(),
                    'ip'=>$request->ip(),
                ],
                'external_account'=>[
                    'object'=>"bank_account",
                    'country'=>"US",
                    'currency'=>"usd",
                    "account_holder_name"=>$data["payoutBankAccount"],
                    "account_holder_type"=>"individual",
                    "routing_number"=>$data["routingNumber"],
                    "account_number"=>$data["accountNumber"]
                ]
            ]);

            $payoutMethod = PayoutMethod::create([
                "user_id"=>$user->id,
                "account_id"=>$response->id,
                "person_id"=>$response->individual->id,
                "last_four"=>substr($data["accountNumber"], -4)
            ]);
            $payoutMethod->save();

            return Response()->json([
                'response'=>[
                    'state'=>true,
                    'msg'=>"Account created! Please provide your verified Profile!"
                ]
            ],200);

    }

    public function getPayoutAccount(Request $request){

        $user = Auth::user();
        $firstPaymentMethod = $user->payoutMethods()->first();
        if($firstPaymentMethod !== null){
            return Response()->json([
                "response"=>[
                    "payoutMethod"=>$firstPaymentMethod
                ]
            ],200);
        }else{
            return Response()->json([
                "response"=>[
                    "state"=>false,
                    "msg"=>"Payout method not set!"
                ]
            ],404);
        }

    }

    public function getPayoutAccountDetail(Request $request){

        $user = Auth::user();
        $firstPaymentMethod =  $user->payoutMethods()->first();
        if ($firstPaymentMethod != null){
            $account_id = $firstPaymentMethod->account_id;
            $stripe = new \Stripe\StripeClient(
                env("STRIPE_SECRET")
            );
            $response = $stripe->accounts->retrieve($account_id);

            $accountDetail = [
                "dob"=>[
                    "day"=>$response->individual->dob->day,
                    "month"=>$response->individual->dob->month,
                    "year"=>$response->individual->dob->year,
                ],
                "firstName"=>$response->individual->first_name,
                "lastName"=>$response->individual->last_name,
                "phoneNumber"=>substr( $response->individual->phone,2),
                "address"=>[
                  "streetAddress"=>$response->individual->address->line1,
                    "city"=>$response->individual->address->city,
                    "state"=>$response->individual->address->state,
                  "postal_code"=>$response->individual->address->postal_code,
                ],
                "bankAccount"=>[
                    "accountName" =>$response->external_accounts->data[0]->account_holder_name,
                    "routingNumber" =>$response->external_accounts->data[0]->routing_number,
                    "last4" =>$response->external_accounts->data[0]->last4,
                ],
            ];

            return Response()->json([
                "response"=>$accountDetail
            ],200);
        }else{
            return Response()->json([
                "response"=>[
                    "state"=>false,
                    "msg"=>"Payout method not set!"
                ]
            ],404);
        }

    }

    public function uploadVefiedFile(Request $request){
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png|max:2048'
        ]);

        Stripe::setApiKey(env("STRIPE_SECRET"));

        $user = Auth::user();
        $firstPaymentMethod =  $user->payoutMethods()->first();

        if($firstPaymentMethod != null){
            if($request->file()) {
                $fileController= new FileController();

                $filePath = $fileController->saveImageToFolder($request,'/validationProfile');
                $response = \Stripe\File::create([
                    'purpose' => 'identity_document',
                    'file' => fopen(public_path("/storage".$filePath) , 'r'),
                ], [
                    'stripe_account' => $firstPaymentMethod->account_id,
                ]);

                $fileID = $response->id;
                $stripe = new \Stripe\StripeClient(
                    env("STRIPE_SECRET")
                );
               $response = $stripe->accounts->updatePerson($firstPaymentMethod->account_id,$firstPaymentMethod->person_id, [
                    'verification' => [
                        'document' => [
                            'front' => $fileID,
                        ],
                    ],
                ]);

               $errors = $response->requirements->errors;

               if(count($errors) == 0){
                   $firstPaymentMethod->profile_provided = true;
                   $firstPaymentMethod->verified = true;
                   $firstPaymentMethod->save();
                   return Response()->json([
                       "response"=>[
                         'state'=>true,
                         'msg'=>"file Updated Success!"
                       ],
                   ],200);
               }else{
                   return Response()->json([
                       "response"=>[
                           'state'=>false,
                           'errors'=>$errors
                       ]
                   ],200);
               }


            }
        }else{
            return Response()->json([
                "response"=>[
                    "state"=>false,
                    "msg"=>"Payout method not set!"
                ]
            ],404);
        }
    }


    public function testPayout(Request $request){

    }

}
