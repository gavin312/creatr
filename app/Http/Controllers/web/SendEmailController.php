<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
}
