<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Mail\GeneralMail;
use App\Models\Reporting;
use App\Models\ReportingReel;
use App\Models\ReportingCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Jobs\SendGeneralEmailJob;
use App\Library\Helper;

class ReportingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->except('getAllReportingTypes');
    }

    public function index()
    {
    }

    public function getAllReportingTypes()
    {
        $reporting_types = Reporting::orderBy("rank")->select("id", "type", "description")->get();

        return response()->json([
            "response" => compact(["reporting_types"])
        ], 200);
    }

    public function reportReel(Request $request)
    {
        $data = $request->validate([
            "reel_id" => "required",
            "reporting_id" => "required",
            "description" => "sometimes",
        ]);

        $user_id = Auth()->id();
        // 0: pending, 1: solved
        $is_reported = ReportingReel::where("reel_id", $data["reel_id"])->where("user_id", $user_id)->where("is_solved", 0)->exists();
        if ($is_reported) {
            return response()->json([
                'response' => [
                    'msg' => 'Already reported'
                ]
            ], 400);
        }
        $reportingItem = new ReportingReel;
        $reportingItem->fill([
            "reel_id" => $data["reel_id"],
            "reporting_id" => $data["reporting_id"],
            "user_id" => $user_id,
            "description" => $data["description"]
        ]);

        // $reportingItem["description"] = $data["description"];
        $reportingItem->save();

        // Mailing: Send reporting message to admin
        $data = [
            "title" => "New Reported Reel",
            "username" => "Admin",
            "messageBody" => "You have received a new request for reporting an reel. Please check it inside the system."
        ];

        // Get admin email address
        $admin_email =  Helper::getSiteGeneralSetting()['companyEmail'];

        // Send reporting record to the address
        dispatch(new SendGeneralEmailJob($admin_email, $data));

        return response()->json([
            'response' => [
                'msg' => 'Success!'
            ]
        ], 200);
    }

    public function reportCollection(Request $request)
    {
        $data = $request->validate([
            "collection_id" => "required",
            "reporting_id" => "required",
            'description' => "sometimes",
        ]);
        $user_id = Auth()->id();

        // 0: pending, 1: solved
        $is_reported = ReportingCollection::where("collection_id", $data["collection_id"])->where("user_id", $user_id)->where("is_solved", 0)->exists();
        if ($is_reported) {
            return response()->json([
                'response' => [
                    'msg' => 'Already reported'
                ]
            ], 400);
        }

        $reportingItem = new ReportingCollection;
        $reportingItem->fill([
            "collection_id" => $data["collection_id"],
            "reporting_id" => $data["reporting_id"],
            "description" => $data["description"],
            "user_id" => $user_id
        ]);

        $reportingItem->save();
        return response()->json([
            'response' => [
                'msg' => 'Success!'
            ]
        ], 200);
    }
}
