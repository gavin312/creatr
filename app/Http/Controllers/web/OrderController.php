<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Reel;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    // public function showThankyou($order_number)
    // {

    //     $order = Order::with("orderItems")->where('order_number', $order_number)->first();
    //     if ($order->user_id == Auth()->id()) {
    //         foreach ($order->orderItems as $order_item) {
    //             $reel_id = $order_item->reel_id;
    //             // TODO: What if reel is in deleted status or draft status
    //             $reel = Reel::with("user")->where("id", $reel_id)->first();
    //             $order_item["reel"] = $reel;
    //         }

    //         $user = User::with("profile")->where('id', Auth()->id())->first();
    //         return view("order.thankyou", compact(['order', 'user']));
    //     } else {
    //         return redirect()->back();
    //     }
    // }

    public function showThankyou($order_number)
    {
        $order_number = decrypt($order_number);
        $is_order_user = Order::where("order_number", $order_number)->where("user_id", Auth()->id())->exists();

        if (!$is_order_user) {
            return redirect()->back();
        }

        $order = Order::with(
            "orderItems.licence:id,type",
            "orderItems.reel.qualities:id,type,public_name,size_short,reel_id",
            "orderItems.reel.user:id,name",
            "orderItems.order.invoice:id,order_id,invoice_number",
            "orderItems.order:id,order_number",
            "user:id,name"
        )
            ->where('order_number', $order_number)
            ->where('status', 'completed')
            ->first();
        // foreach ($order->orderItems as $orderItem) {
        //     $orderItem["order"]["order_number"] = $order_number;
        // }

        return view("order.thankyou", compact(['order']));
    }

    // showOrderHistory
    public function show()
    {
        if (!Auth::check()) {
            return redirect()->back();
        }

        return view('order.show');
    }

    // public function getMyOrders(Request $request)
    // {
    //     $pageSize = 10;
    //     $pageIndex = intval($request->get("page"));
    //     dd($pageIndex);
    //     $pageNumber =  $pageIndex < 1 ? 1 : $pageIndex;

    //     $userId = Auth::id();
    //     $query = Order::with("orderItems")->where('user_id', $userId)->orderByDesc('created_at');

    //     $orders = $query->skip($pageSize * ($pageNumber - 1))->take($pageSize)->get();

    //     foreach ($orders as $order) {
    //         foreach ($order->orderItems as $order_item) {
    //             $reel_id = $order_item->reel_id;
    //             $reel = Reel::with("user")->where("id", $reel_id)->first();
    //             $order_item["reel"] = $reel;
    //         }
    //     }

    //     return response()->json([
    //         'response' => compact(['orders'])
    //     ], 200);
    // }

    public function getMyOrders(Request $request)
    {

        $creatorIndex = intval($request->input('orderSelectedCreator', -1));
        $dates = $request->validate([
            'from' => 'date_format:Y-m-d H:i:s|before_or_equal:now',
            'to' => 'date_format:Y-m-d H:i:s|after_or_equal:from' . "|before_or_equal:tomorrow"
        ]);
        $dateIndex = 1;

        $pageSize = 10;
        $userId = Auth::id();
        $query = OrderItem::with("order.invoice:id,order_id,invoice_number", "licence:id,type", "reel.user", "reel.qualities")->whereHas('order', function ($order) use ($userId) {
            $order->where('user_id', $userId)->where('status', 'completed');
        });

        if ($creatorIndex != -1) {
            $query = $query->whereHas("reel", function ($order) use ($creatorIndex) {
                $order->where('user_id', $creatorIndex);
            });
        }

        if (array_key_exists('from', $dates)) {
            $query = $query->where('created_at', '>=', $dates['from']);
        }

        if (array_key_exists('to', $dates)) {
            $query = $query->where('created_at', '<=', $dates['to']);
        }

        // if ($dateIndex = 1) {
        //     $query = $query->where('created_at', ">=", '2021-10-25')->where('created_at', '<=', '2021-10-26');
        // }

        $orders = $query->orderByDesc('created_at')->paginate($pageSize);

        return response()->json([
            'response' => compact(['orders'])
        ], 200);
    }

    // return all order items' creator's id and name.
    public function getMyOrdersAllCreators()
    {
        $userId = Auth::id();
        $users = OrderItem::with("order", "reel.user")->whereHas('order', function ($order) use ($userId) {
            $order->where('user_id', $userId);
        })->get()->map(function ($orderItem) {
            $user["id"] = $orderItem->reel->user->id;
            $user["name"] = $orderItem->reel->user->name;
            return $user;
        });

        return response()->json([
            'response' => compact(['users'])
        ], 200);
    }
}
