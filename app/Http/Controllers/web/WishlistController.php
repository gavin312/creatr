<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Wishlist;
use App\Models\User;


class WishlistController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->only(['index', 'getItemWishlistStatus', 'addToWishlist', 'removeFromWishlist', 'getWishlistedReelsFromWishlist', 'getWishlistedCollectionsFromWishlist']);
    }

    public function index()
    {
        return view('wishlist.index');
    }


    public function getItemWishlistStatus(Request $request)
    {
        $type = $request->get('type');
        $content_id = $request->get('content_id');

        // If user is logged in
        if (!Auth::check()) {
            return response()->json([
                'response' => [
                    'msg' => 'Not login'
                ]
            ], 200);
        }

        // Decide it is reel or collection
        $content_type = $type === 'reel' ? 'reel_id' : 'collection_id';
        $exists = Wishlist::where('user_id', Auth::id())->where($content_type, $content_id)->first();
        $response = [];
        $exists ? $response = ['msg' => 'true'] : $response = ['msg' => 'false'];
        return response()->json($response, 200);
    }

    // Add to wishlist method
    public function addToWishlist(Request $request)
    {
        $type = $request->get('type');
        $target_id = $request->get('content_id');
        $content_type =  $type === 'reel' ? 'reel_id' : 'collection_id';
        $exists = Wishlist::where('user_id', Auth::id())->where($content_type, $target_id)->first();

        if (!$exists) {
            Wishlist::create([
                'user_id' => Auth::id(),
                $content_type => $target_id
            ]);
            // Wishlist::insert([
            //     'user_id' => Auth::id(),
            //     'reel_id' => $target_id,
            //     'collection_id' => null,
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now()
            // ]);

            return response()->json([
                'response' => [
                    'msg' => 'Added successfully.'
                ]
            ], 200);
        } else {
            return response()->json([
                'response' => [
                    'msg' => 'This item has already on your wishlist.'
                ]
            ], 403);
        }
    }
    // Remove from wishlist method
    public function removeFromWishlist(Request $request)
    {
        $target_id = $request->get('content_id');
        $content_type = $request->get('type') === 'reel' ? 'reel_id' : 'collection_id';
        $exists = Wishlist::where('user_id', Auth::id())->where($content_type, $target_id)->first();

        if ($exists) {
            $exists->delete();

            return response()->json([
                'response' => [
                    'msg' => 'Removed successfully.'
                ]
            ], 200);
        } else {
            return response()->json([
                'response' => [
                    'msg' => 'Cannot find this item on your wishlist.'
                ]
            ], 403);
        }
    }

    public function getWishlistedReelsFromWishlist(Request $request)
    {
        $userID = Auth::id();
        // 'reel' or 'collection'
        // $content_type = $request->get('type');
        // $content_type_id = $content_type === 'reel' ? 'reel_id' : 'collection_id';

        // $wishlist = Wishlist::with($content_type)
        //     ->where('user_id', $userID)
        //     ->whereNotNull($content_type_id)
        //     ->get();

        $pageindex = intval($request->input('reelPageIndex'));
        $pageindex = $pageindex < 1 ? 1 : $pageindex;
        $pagesize = intval($request->input('reelPageSize'));
        $startIndex = ($pageindex - 1) * $pagesize;

        // Only selects reel in published status
        $query = Wishlist::with('reel')
            ->where('user_id', $userID)
            ->whereNotNull('reel_id')
            ->whereHas('reel', function ($query) {
                $query->where('status', 'published');
            });

        // $reels_count = $query->count();

        $wishlist = $query
            ->skip($startIndex)
            ->take($pagesize)
            ->get();

        foreach ($wishlist as $wishlist_item) {
            $creator_id = $wishlist_item->reel->user_id;
            $creator = User::find($creator_id);
            $wishlist_item['reel']['creator'] = $creator->name;
            // $wishlist_item->reel->licences;
            // $wishlist_item->reel->qualities;
            
            $reel_licences = $wishlist_item->reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $wishlist_item->reel->qualities->makeVisible(['link']);
            } else {
                $wishlist_item->reel->qualities;
            }
            $wishlist_item['reel']['is_in_wishlist'] = true;
        }

        return response()->json([
            'response' => compact(['wishlist'])
        ], 200);
    }

    public function getWishlistedCollectionsFromWishlist(Request $request)
    {
        $userID = Auth::id();

        $pageindex = intval($request->input('collectionPageIndex'));
        $pageindex = $pageindex < 1 ? 1 : $pageindex;
        $pagesize = intval($request->input('collectionPageSize'));
        $startIndex = ($pageindex - 1) * $pagesize;

        // Only selects reel in published status
        $wishlist = Wishlist::with('collection')
            ->where('user_id', $userID)
            ->whereNotNull('collection_id')
            ->whereHas('collection', function ($query) {
                $query->where('status', 'published');
            })
            ->skip($startIndex)
            ->take($pagesize)
            ->get();

        foreach ($wishlist as $wishlist_item) {
            $creator_id = $wishlist_item->collection->user_id;
            $creator = User::find($creator_id);
            $wishlist_item['collection']['creator_name'] = $creator->name;
            $wishlist_item['collection']['is_in_wishlist'] = true;

            $wishlist_item['collection']['reels'] = $wishlist_item->getReels();
        }

        return response()->json([
            'response' => compact(['wishlist'])
        ], 200);
    }
}
