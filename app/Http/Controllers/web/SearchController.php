<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Reel;
use App\Models\Tag;
use App\Models\Platform;
use App\Models\Wishlist;
use App\Models\Rate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function Doctrine\Common\Cache\Psr6\get;
use Illuminate\Support\Facades\Auth;

class SearchController extends Controller
{

    public function show(Request $request){

        $keyword = $request->get('keyword') == null? "": $request->get('keyword');

        if($keyword == ""){
            $reelsCount = Reel::where('status','published')->where('only_in_collection','0')->get()->count();
            $tempCollections = Collection::where('status','published')->get();
            $collectionsCount = 0;
            foreach ($tempCollections as $tempCollection){
                if($tempCollection->getReelsCount() > 0){
                    $collectionsCount ++;
                }
            }
            $creatorsCount = User::all()->count();
        }else{
            $reelsCount = Reel::where('status','published')->where('only_in_collection','0')->where('title','like','%'.$keyword.'%')->get()->count();
            $tempCollections = Collection::where('status','published')->where('title','like','%'.$keyword.'%')->get();
            $collectionsCount = 0;
            foreach ($tempCollections as $tempCollection){
                if($tempCollection->getReelsCount() > 0){
                    $collectionsCount ++;
                }
            }
            $creatorsCount = User::where('name','like','%'.$keyword.'%')->count();
        }

        $tags = Tag::orderBy('name','ASC')->get();
        $platforms = Platform::orderBy('rank')->get();
        $rates = Rate::orderBy('rank')->get();
        $categories = Category::all();

        $searchBasics = json_encode(compact(['keyword','reelsCount','collectionsCount','creatorsCount']));

        return view('search.index',compact(["searchBasics",'tags','categories','rates','platforms']));
    }

    public function getReels(Request $request){
        $pageindex = intval($request->get("reelPageIndex")) ;
        $pageSeed = intval($request->get("reelPageSeed")) ;
        $pagesize = intval($request->get("reelPageSize")) ;

        $tagIndex = intval($request->get('reelSelectedTag'));
        $categoryIndex = intval($request->get('reelSelectedCategory'));
        $rateIndex = intval($request->get('reelSelectedRate'));
        $platformIndex = intval($request->get('reelSelectedPlatform'));

        $keyword = $request->get('keyword');

        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        // TODO: Maybe optimise to lazy loading
        $query = DB::table('reels');

        // Filter banned reels.
        $query = $query->where('reels.is_banned', '!=', 1);


        if( $tagIndex != -1){
            $query = $query
                ->join('reel_tags','reels.id', '=','reel_tags.reel_id')
                ->where('reel_tags.tag_id','=',$tagIndex);
        }

        if($categoryIndex != -1){
            $query = $query->where('reels.category_id','=',$categoryIndex);
        }

        if($rateIndex != -1) {
            $query = $query->where('reels.rate_id','=',$rateIndex);
        }

        if($platformIndex != -1) {
            // Needs $query = Reel::with();
            // $query = $query->whereHas('platforms', function($q) use ($platformIndex) {
            //     $q->where('platforms.id', $platformIndex);
            // });
            $query = $query
                ->join('platform_reels','reels.id', '=','platform_reels.reel_id')
                ->where('platform_reels.platform_id','=',$platformIndex);
        }

        if($keyword != "" ){
            $query = $query->where('reels.title','like','%'.$keyword.'%');
        }

        $reelIds =  $query->where('reels.only_in_collection','=',"0")
            ->where('reels.status','=','published')
            ->where('reels.deleted_at','=',null)
            ->select("reels.id")
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get()
            ->toArray();

        $ids = [];

        foreach ($reelIds as $id ){
            if(!in_array($id->id, $ids)){
                array_push($ids,$id->id);
            }
        }
//        dd($reelIds,$ids);
        $reels = Reel::find($ids);
        $userId = Auth::id();

        foreach ($reels as $reel){
            $reel['tags'] = $reel->getTags();
            $reel['creator'] = $reel->getCreatorName();

            $reel_licences = $reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $reel->qualities->makeVisible(['link']);
            } else {
                $reel->qualities;
            }
            
            $creatorID = $reel->user_id;
            $reel['is_creator'] = $userId === $creatorID ? true : false;
            $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();
        }

        return response()->json([
            "response"=>compact(['reels','pageSeed'])
        ],200);
    }
    public function getCollections(Request $request){

        $pageindex = intval($request->get("collectionPageIndex")) ;
        $pageSeed = intval($request->get("collectionPageSeed")) ;
        $pagesize = intval($request->get("collectionPageSize")) ;

        $tagIndex = intval($request->get('collectionSelectedTag'));
        $categoryIndex = intval($request->get('collectionSelectedCategory'));
        $rateIndex = intval($request->get('collectionSelectedRate'));
        $platformIndex = intval($request->get('collectionSelectedPlatform'));

        $keyword = $request->get('keyword');

        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $query = DB::table('collections');


        if( $tagIndex != -1){
            $query = $query
                ->join('collection_tags','collections.id', '=','collection_tags.collection_id')
                ->where('collection_tags.tag_id','=',$tagIndex);
        }

        if($categoryIndex != -1){
            $query = $query->where('collections.category_id','=',$categoryIndex);
        }

        if($rateIndex != -1) {
            $query = $query->where('collections.rate_id','=',$rateIndex);
        }

        if($platformIndex != -1) {
            // Needs $query = Collection::with();
            // $query = $query->whereHas('platforms', function($q) use ($platformIndex) {
            //     $q->where('platforms.id', $platformIndex);
            // });
            $query = $query
                ->join('platform_collections','collections.id', '=','platform_collections.collection_id')
                ->where('platform_collections.platform_id','=',$platformIndex);
        }

        if($keyword != "" ){
            $query = $query->where('collections.title','like','%'.$keyword.'%');
        }

        $collectionIds =  $query->where('collections.status','=','published')
            ->where('collections.deleted_at','=',null)
            ->select("collections.id")
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get()
            ->toArray();

        $ids = [];

        foreach ($collectionIds as $id ){
            if(!in_array($id->id, $ids)){
                array_push($ids,$id->id);
            }
        }
//        dd($reelIds,$ids);
        $userId = Auth::id();

        $tempCollections = Collection::find($ids);
        $collections = [];
        foreach ($tempCollections as $collection){
            $collection['reel'] = $collection->getMostPopularReel();
            $collection['reels'] = $collection->getReels();
            $collection->tags;
            $collection->user;
            $creatorID = $collection->user_id;
            $collection['is_creator'] = $userId === $creatorID ? true : false;
            $collection['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('collection_id', $collection->id)->exists();

            if(count($collection['reels'] ) > 0){
                array_push($collections,$collection);
            }
        }

        return response()->json([
            "response"=>compact(['collections','pageSeed'])
        ],200);
    }
    public function getCreators(Request $request){

        $pageindex = intval($request->get("creatorPageIndex")) ;
        $pageSeed = intval($request->get("creatorPageSeed")) ;
        $pagesize = intval($request->get("creatorPageSize")) ;
        $keyword = $request->get('keyword');
        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $tempCreators = [];
        if($keyword != ""){
            $tempCreators = User::where('name','like','%'.$keyword.'%')
                ->skip($startIndex)
                ->take($pagesize)
                ->inRandomOrder($pageSeed)
                ->get()
                ->toArray();
        }else{
            $tempCreators = User::skip($startIndex)
                ->take($pagesize)
                ->inRandomOrder($pageSeed)
                ->get()
                ->toArray();
        }


        $creators = [];

        foreach ($tempCreators as $tempCreator){
            $creator = User::find($tempCreator['id']);
            $tempCreator['profile'] = $creator->getProfile();
            $tempCreator['reel'] = $creator->get_most_viewed_reel();
            $tempCreator['prizes'] = $creator->prizes;
            array_push($creators,$tempCreator);
        }



        return response()->json([
            "response"=>compact(['creators','pageSeed'])
        ],200);
    }
}
