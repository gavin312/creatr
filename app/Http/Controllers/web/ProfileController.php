<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Library\MailerliteHelper;
use App\Models\Collection;
use App\Models\Profile;
use App\Models\Wishlist;
use App\Models\Reel;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Voyager;
use App\Jobs\SendGeneralEmailJob;

class ProfileController extends Controller
{

    public function __construct() {
        $this->middleware('auth')->only(['edit','updateProfileSection','updateProfileSocialLink',"changeNotificationSetting"]);
    }

    /**
     * @param $user_name
     */
    public function show($user_name){
        $user =  User::where('name',$user_name)->firstOrFail();
        $profile = $user->getProfile();
        $followers_count = count($user->getFollowers());
        $following_count = count($user->getFollowings());
        $isFollowing = false;
        $isUser = false;
        if(Auth::check()){
            $currentUser = Auth::user();
            $isUser = $currentUser->id === $user->id;
            if(!$isUser){
                $isFollowing = Helper::isFollowing($currentUser->id, $user->id);
            }
        }
        $userDetail = [
            'id'=>$user->id,
            'name'=>$user->name,
            'email'=>$user->email,
            'avatar'=>$user->avatar,
        ];
        $prizes = $user->prizes;
        $reelsCount = $user->getReelsCount();
        $collectionCount = $user->getCollectionsCount();

        $user_info = json_encode( compact(['userDetail','prizes','profile','followers_count','following_count','isFollowing','isUser','reelsCount','collectionCount']));

        return view('profile.show',compact(['user_info']));
    }

    public function edit($user_name){

        $user =  User::where('name',$user_name)->firstOrFail();

        $login_user = Auth::user();


        if($login_user->id === $user -> id){
            $profile = Profile::where('user_id',$user->id)->first();
            if(!$profile){
                $profile = Profile::create([
                    'user_id' => $user->id,
                    'user_name'=> $user->name,
                ]);
                $profile->save();
            }
            $profile["isVerified"] = $user->hasVerifiedEmail();

            return view('profile.edit',compact(['user','profile']));

        }else{
            return redirect()->route('showProfile',['user_name'=>$user_name]);
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateProfileSection(Request $request){

        $data = $request->validate([
            'userName' =>'required',
            'name' => 'required',
            'avatar' => 'required',
            'bio' => 'max:100'
        ]);

        $currentUser = Auth::user();
        $user = User::find($currentUser->id);
        $user->name =$data['userName'];
        $user->avatar =$data['avatar'];
        $user->save();

        $profile = Profile::where('user_id',$user->id)->first();

        $profile->user_name = $data["name"];
        $profile->bio = $data["bio"];
        $profile->save();

        return response()->json(
            [
                'msg'=> 'Update Success!'
            ],200
        );
    }

    /**
     * @param Request $request
     */
    public function updateProfileSocialLink(Request $request){

        $currentUser = Auth::user();

        $profile = Profile::where('user_id',$currentUser->id)->first();

        $personal_website = $request->get("personal_website");
        $youtube_link = $request->get("youtube_link");
        $tiktok_link = $request->get("tiktok_link");
        $instagram_link = $request->get("instagram_link");
        $facebook_link = $request->get("facebook_link");
        $snapchat_link = $request->get("snapchat_link");
        $twitter_link = $request->get("twitter_link");


        if ($personal_website != null){
            $profile->personal_website = Helper::convertRelativePathToAbsolute($personal_website);
        }else{
            $profile->personal_website = null;
        }

        if ($youtube_link != null){
            $profile->youtube_link = Helper::convertRelativePathToAbsolute($youtube_link);
        }else{
            $profile->youtube_link = null;
        }

        if ($tiktok_link != null){
            $profile->tiktok_link = Helper::convertRelativePathToAbsolute($tiktok_link);
        }else{
            $profile->tiktok_link = null;
        }

        if ($instagram_link != null){
            $profile->instagram_link = Helper::convertRelativePathToAbsolute($instagram_link);
        }else{
            $profile->instagram_link = null;
        }

        if ($facebook_link != null){
            $profile->facebook_link = Helper::convertRelativePathToAbsolute($facebook_link);
        }else{
            $profile->facebook_link = null;
        }

        if ($snapchat_link != null){
            $profile->snapchat_link = Helper::convertRelativePathToAbsolute($snapchat_link);
        }else{
            $profile->snapchat_link = null;
        }

        if ($twitter_link != null){
            $profile->twitter_link = Helper::convertRelativePathToAbsolute($twitter_link);
        }else{
            $profile->twitter_link = null;
        }

        $profile->save();

        return response()->json(
            [
                'msg'=> 'Update Success!'
            ],200
        );
    }

    /**
     * @param Request $request
     */
    public function changePassword(Request $request){
        $data = $request->validate([
            'currentPassword'=>'required',
            'newPassword'=>'required',
        ]);

        $currentUser = Auth::user();
        if(Hash::check($data['currentPassword'],$currentUser->password)){
            User::find(auth()->user()->id)->update(['password'=> Hash::make($data['newPassword'])]);

            $data = [
                'title' => 'Password Changed',
                'username' => $currentUser["name"],
                'messageBody' => 'Your password has been changed successfully. If you did not recently try to change your password, we recommend that you change your password.'
            ];
            dispatch(new SendGeneralEmailJob($currentUser["email"], $data));

            return response()->json(
                [
                    'msg'=> 'Update Success!'
                ],200
            );
        }else{
            return response()->json(
                [
                    'msg'=> 'Current Password is not correct, please try again!'
                ],401
            );
        }

    }

    /**
     * @param Request $request
     */
    public function changeEmail(Request $request){

        $email = $request->get('email');
        $currentUser = Auth::user();
        $user = User::find(auth()->user()->id);
        $userArray = User::where('email',$email)->get()->toArray();
        if(empty($userArray)){
            $user->email_verified_at = null;
            $old_email = $user->email;
            $user->update(['email'=> $email]);
            $user->save();
            $user->sendEmailVerificationNotification();

            $data = [
                'title' => 'Email Changed',
                'username' => $currentUser["name"],
                'messageBody' => 'Your email has been changed successfully. If you did not recently try to change your email, we recommend that you change your password.'
            ];
            dispatch(new SendGeneralEmailJob($old_email, $data));

            return response()->json(
                [
                    'msg'=> 'Update Success!'
                ],200
            );
        }else{
            return response()->json(
                [
                    'msg'=> 'This email is occupied, please double check!'
                ],401
            );
        }

    }

    public function getReelsByCreator(Request $request){
        $creatorID = intval($request->input('creatorID'));
        $pageindex = intval($request->input('pageIndex')) ;
        $pageindex = $pageindex < 1? 1 : $pageindex;
        $pagesize = intval($request->input('pageSize')) ;
        $startIndex = ($pageindex-1) * $pagesize;
        $reels = Reel::where('user_id',$creatorID)->where('only_in_collection','0')->where('status','published')->orderByDesc('created_at')->skip($startIndex)->take($pagesize)->get();

        $userId = Auth::id();
        foreach ($reels as $reel){
            $reel['comments'] = $reel->getCommentsCount();
            $reel['creator'] = $reel->getCreatorName();
            
            $reel_licences = $reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $reel->qualities->makeVisible(['link']);
            } else {
                $reel->qualities;
            }
            $reel['is_creator'] = $userId === $creatorID ? true : false;
            $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();
        }
        return response()->json([
            'response'=>compact(['reels'])
        ],200);
    }

    public function getCollectionsByCreator(Request $request){
        $creatorID = intval($request->input('creatorID'));
        $pageindex = intval($request->input('pageIndex')) ;
        $pageindex = $pageindex < 1? 1 : $pageindex;
        $pagesize = intval($request->input('pageSize')) ;
        $startIndex = ($pageindex-1) * $pagesize;

        $collections = Collection::with('user')->where('user_id',$creatorID)->where('status','published')->orderByDesc('created_at')->skip($startIndex)->take($pagesize)->get();
        $userId = Auth::id();

        foreach ($collections as $collection){
            $collection['reels'] = $collection->getReels();
            $collection['creator_name'] = $collection->user->name;
            $reel['comments'] = $collection->getCommentsCount();
            $collection['is_creator'] = $userId === $creatorID ? true : false;
            $collection['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('collection_id', $collection->id)->exists();
        }
        return response()->json([
            'response'=>compact(['collections'])
        ],200);
    }

    public function changeNotificationSetting(Request $request){
        $data = $request->validate([
            "promotion_notification"=>"required",
            "activity_notification"=>"required",
        ]);

        $promotion_notification = $data["promotion_notification"];
        $activity_notification = $data["activity_notification"];

        $user = Auth::user();
        $user->promotion_notification = $promotion_notification;
        $user->activity_notification = $activity_notification;
        $user->save();

        if(!$user->promotion_notification){
            $mailerHelper = new MailerliteHelper();
            $mailerHelper->removeSubscriberByEmail($user->email);
        }else{
            $mailerHelper = new MailerliteHelper();
            $mailerHelper->subscribeCreatr($user->email);
        }

        return response()->json(
            [
                'msg'=> 'Update Success!'
            ],200
        );
    }
}
