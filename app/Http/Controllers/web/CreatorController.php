<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Resources\CreatorIndexResource;
use App\Models\User;
use Illuminate\Http\Request;

class CreatorController extends Controller
{
  // Get all creators with their profiles
  // public function getCreators()
  // {
  //   $creators = CreatorIndexResource::collection(
  //     User::with('profile')->get()
  //   );

  //   return response()->json($creators, 200);
  // }

  public function getCreators(Request $request)
  {
    $pageindex = intval($request->get("creatorPageIndex"));
    $pageSeed = intval($request->get("creatorPageSeed"));
    $pagesize = intval($request->get("creatorPageSize"));
    $keyword = $request->get('keyword');
    $startIndex = ($pageindex - 1) * $pagesize;

    if ($pageSeed == "") {
      $pageSeed = strval(random_int(1, 100));
    }

    $tempCreators = [];
    if ($keyword != "") {
      $tempCreators = User::where('name', 'like', '%' . $keyword . '%')
        ->skip($startIndex)
        ->take($pagesize)
        ->inRandomOrder($pageSeed)
        ->get()
        ->toArray();
    } else {
      $tempCreators = User::skip($startIndex)
        ->take($pagesize)
        ->inRandomOrder($pageSeed)
        ->get()
        ->toArray();
    }

    $creators = [];

    foreach ($tempCreators as $tempCreator) {
      $creator = User::find($tempCreator['id']);
      $tempCreator['profile'] = $creator->getProfile();
      $tempCreator['reel'] = $creator->get_most_viewed_reel();
      $tempCreator['prizes'] = $creator->prizes;
      array_push($creators, $tempCreator);
    }

    return response()->json([
      "response" => compact(['creators', 'pageSeed'])
    ], 200);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    return view("creator.index");
  }
}
