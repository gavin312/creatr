<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Licence;
use App\Models\Order;
use App\Models\OrderItem;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Voyager;
use TusPhp\Response;

class LicenceController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth','verified'])->except('getAllLicences');

    }

    public function index(){

    }



    public function getAllLicences(){
        $licences = Licence::orderBy("rank")->get();

        return Response()->json([
            "response"=>compact(["licences"])
        ],200);
    }

    public function getOrderItemLicence(OrderItem $orderItem){

        $licence = $orderItem->licence()->first();
        $reel = $orderItem->reel()->with("user","user.profile")->first();
        $order = $orderItem->order()->with("user","user.profile")->first();
        $companyInfo =  (object)Helper::getSiteGeneralSetting();

        $user = Auth::user();
        if($user->id === $order->user_id){
            $view = View('pdf.licence', compact(["companyInfo","order","reel","licence"]));


            $logo = Helper::convertImageToBase64($companyInfo->companyNavLogo);
            $reelThumb = Helper::convertImageToBase64($reel->thumbnail);


            $pdf = PDF::loadView('pdf.licence', compact(["companyInfo","order","reel","licence","logo","reelThumb"]))
                ->setPaper('a4')
                ->setOptions([
                    'isHtml5ParserEnabled'=>true,
                    "isPhpEnabled"=>true,

                ]);
            return $pdf->stream();
        }else{
            abort(403);
        }





    }




}
