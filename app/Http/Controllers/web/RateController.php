<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;

use App\Models\Licence;

use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{

    public function getAllRates(Request $request)
    {

        $rates = Rate::orderBy("rank")->get();
        return response()->json([

            "response"=>compact(["rates"])
        ],200
     );
    }

}
