<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Quality;
use App\Models\Reel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class OrderItemController extends Controller
{


    public function __construct(){
        $this->middleware(['auth','verified']);
    }


    /**
     * @param OrderItem $orderItem
     *
     */
    function downloadReel(OrderItem $orderItem){
        $data = Request()->validate([
            "qualityId"=>"required"
        ]);
        $user = Auth::user();
        $qualityId = $data["qualityId"];

        $order = Order::find($orderItem->order_id);

        if($user->id == $order->user_id){
            $quality = Quality::findOrFail($qualityId);
            $reel = $quality->reel()->withTrashed()->first();
            if($reel->id != $orderItem->reel_id){
                abort(404);
            }else{
                $now = new \DateTime();
                $expireTime = new \DateTime($quality->expires);
                if($now > $expireTime){
                    $reelController =new ReelController();
                    $reelController->getQualityDetailfromVimeoByVideoID($reel);
                    $newQuality = Quality::find($qualityId);
                    $expireTime = new \DateTime($newQuality->expires);
                    if($now > $expireTime){
                        $newQuality->delete();
                        return Redirect::back();
                    }else{
                        return Redirect::to($newQuality->link);
                    }

                }else{
                    return Redirect::to($quality->link);
                }
            }
        }else{
            abort(403);
        }
    }



    function downloadFreeReel(Reel $reel){
        $licence = $reel->licences()->first();
        $data = Request()->validate([
            "qualityId"=>"required"
        ]);
        $quality = Quality::findOrFail($data["qualityId"]);
        if($licence->is_free){
            if($reel->id == $quality->reel_id){
                $now = new \DateTime();
                $expireTime = new \DateTime($quality->expires);
                if($now > $expireTime){
                    $reelController =new ReelController();
                    $reelController->getQualityDetailfromVimeoByVideoID($reel);
                    $newQuality = Quality::find($data["qualityId"]);
                    $expireTime = new \DateTime($newQuality->expires);
                    if($now > $expireTime){
                        $newQuality->delete();
                        return Redirect::back();
                    }else{
                        return Redirect::to($newQuality->link);
                    }

                }else{
                    return Redirect::to($quality->link);
                }
            }else{
                abort(404);
            }
        }else{
            abort(403);
        }
    }
}
