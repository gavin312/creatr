<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Wishlist;
use App\Models\Reel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        foreach ($categories as $category){
            $category['count'] = $category->getReelAndCollectionCount();
            $category['reels'] = $category->getMostPopularReels();
        }
        $topCategories = Category::find($this->getTopCategories());
        return view("category.index",compact(['categories','topCategories']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param String $category_title
     * @return void
     */
    public function show(String $category_title)
    {

        $category = Category::where('name',$category_title)->firstOrFail();

        $category['reelsCount'] = $category->getReelsCount();
        $category['collectionsCount'] = $category->getCollectionCount();

        $mostPopularIds = $category->getMostPopularReel();
        if( count($mostPopularIds)<=0){
            $category['mostPopularReel'] = null;
        }else{

            $mostPopularReel = Reel::find($mostPopularIds[0])->first();
            $mostPopularReel['creatorName'] =  $mostPopularReel->getCreatorName();
            $category['mostPopularReel'] = $mostPopularReel;
        }

        return view('category.show',compact(['category']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCategories(){
        $categories = Category::all();
        return response()->json([
            'response'=>compact(['categories'])
        ],200);
    }

    public function getTopCategories(){
        $mostPopularCategories = DB::table('categories')
            ->join('reels','categories.id','=','reels.category_id')
            ->select('categories.id')
            ->where('categories.deleted_at','=',null)
            ->groupBy('categories.id')
            ->orderByRaw('COUNT(*) DESC')
            ->get()
            ->toArray();
        $mostPopularCategoriesIds = [];

        foreach ($mostPopularCategories  as $category){
            if(!in_array($category->id,$mostPopularCategoriesIds)){
                array_push($mostPopularCategoriesIds,$category->id);
            }
        }
        return $mostPopularCategoriesIds;

    }


    public function getCategoryReels(Request $request){

        $pageindex = intval($request->get("reelPageIndex")) ;
        $pageSeed = intval($request->get("reelPageSeed")) ;
        $pagesize = intval($request->get("reelPageSize")) ;
        $categoryIndex = intval($request->get('categoryIndex'));
        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $userId = Auth::id();

        $reels = Reel::where('category_id',$categoryIndex)
            ->where('status','published')
            ->where('only_in_collection','0')
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get();

        foreach ($reels as $reel){
            $reel['tags'] = $reel->getTags();
            $reel['creator'] = $reel->getCreatorName();
            
            $reel_licences = $reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $reel->qualities->makeVisible(['link']);
            } else {
                $reel->qualities;
            }
            $creatorID = $reel->user_id;
            $reel['is_creator'] = $userId === $creatorID ? true : false;
            $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();

        }

        return response()->json([
            "response"=>compact(['reels','pageSeed'])
        ],200);

    }

    public function getCategoryCollections(Request $request){
        $pageindex = intval($request->get("collectionPageIndex")) ;
        $pageSeed = intval($request->get("collectionPageSeed")) ;
        $pagesize = intval($request->get("collectionPageSize")) ;
        $categoryIndex = intval($request->get('categoryIndex'));
        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $tempcollections = Collection::with('user.profile', 'category', 'reels')
            ->where('category_id',$categoryIndex)
            ->where('status','published')
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get();

        $userId = Auth::id();

        $collections = [];
        foreach ($tempcollections as $collection){
            $collection['reel'] = $collection->getMostPopularReel();
            $collection['reels'] = $collection->getReels();
            $collection['reelcount'] = count($collection->getReels());
            $collection->tags;
            $creatorID = $collection->user_id;
            $collection['is_creator'] = $userId === $creatorID ? true : false;
            $collection['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('collection_id', $collection->id)->exists();

            if(count($collection['reels'] ) > 0){
                array_push($collections,$collection);
            }
        }




        return response()->json([
            "response"=>compact(['collections','pageSeed'])
        ],200);

    }

}
