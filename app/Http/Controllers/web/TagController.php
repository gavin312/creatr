<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Collection;
use App\Models\Reel;
use App\Models\Tag;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{
    //

    static public function createTagsFromTagsArray($tags,$thumbnail){

        $selectedTagIds = [];
        foreach($tags as $tag){
            $selectedTag = Tag::where('name',$tag)->first();
            if($selectedTag){
                array_push($selectedTagIds,$selectedTag->id);
            }else{
                $selectedTag = Tag::create([
                    'name'=>$tag,
                    'image_path'=>$thumbnail
                ]);
                $selectedTag->save();

                array_push($selectedTagIds,$selectedTag->id);
            }
        }
        return $selectedTagIds;

    }


    /**
     * Display the specified resource.
     *
     * @param String $tag_title
     * @return Response
     */
    public function show(String $tag_title)
    {

        $tag = Tag::where('name', $tag_title)->firstOrFail();

        $mostPopularTagIDs = $this->getMostPopularTags($tag->id)['mostPopularTagIDs'];
        $mostPopularTags = Tag::find($mostPopularTagIDs)->toArray();
        shuffle($mostPopularTags);
        $mostPopularTags = json_encode($mostPopularTags);
        $tag['reelsCount'] = DB::table('reel_tags')
            ->join('reels','reel_tags.reel_id','=','reels.id')
            ->where('reels.status','=','published')
            ->where('reels.only_in_collection','=','0')
            ->where('reels.is_banned', '!=', 1)
            ->where('reels.deleted_at','=', null)
            ->where('tag_id',$tag->id)
            ->count();

        $tag['collectionsCount'] = DB::table('collection_tags')
            ->join('collections','collection_tags.collection_id','=','collections.id')
            ->join("collection_reels","collections.id","=","collection_reels.collection_id")
            ->where('reel_id',"!=",null)
            ->where('collections.status','=','published')
            ->where('collections.deleted_at','=', null)
            ->where('tag_id',$tag->id)
            ->groupBy('collections.id')
            ->count();
        $mostPopularReel = $tag->getMostPopularReel();
        if($mostPopularReel != null){
            $mostPopularReel['creatorName'] =  $mostPopularReel->getCreatorName();
        }
        $tag['mostPopularReel'] = $mostPopularReel;

        return view('tag.show',compact(['tag','mostPopularTags']));
    }


    /**
     * @return array
     */
    public function getMostPopularTags($id){
        $mostPopularCollectionTags = DB::table('collection_tags')
            ->where('tag_id',"!=" ,$id)
            ->select('tag_id')
            ->groupBy('tag_id')
            ->orderByRaw('COUNT(*) DESC')
            ->limit(4)
            ->get();

        $mostPopularReelTags = DB::table('reel_tags')
            ->where('tag_id',"!=" ,$id)
            ->select('tag_id')
            ->groupBy('tag_id')
            ->orderByRaw('COUNT(*) DESC')
            ->limit(4)
            ->get();
        $mostUsedTags = $mostPopularCollectionTags->concat($mostPopularReelTags);

        $mostPopularTagIDs = [];
        foreach ($mostUsedTags as $tag){
            if(!in_array($tag->tag_id,$mostPopularTagIDs)){
                array_push($mostPopularTagIDs,$tag->tag_id);
            }
        }

        shuffle($mostPopularTagIDs) ;
        return compact(['mostPopularTagIDs']);
    }


    public function getTagReels(Request $request){

        $pageindex = intval($request->get("reelPageIndex")) ;
        $pageSeed = intval($request->get("reelPageSeed")) ;
        $pagesize = intval($request->get("reelPageSize")) ;
        $tagIndex = intval($request->get('tagIndex'));
        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $reelIds = DB::table('reel_tags')
            ->join('reels','reel_tags.reel_id', '=','reels.id')
            ->where('reel_tags.tag_id','=',$tagIndex)
            ->where('reels.only_in_collection','=',"0")
            ->where('reels.is_banned','!=',1)
            ->where('reels.status','=','published')
            ->where('reels.deleted_at','=',null)
            ->select("reels.id")
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get()
            ->toArray();

        $ids = [];

        foreach ($reelIds as $id ){
            if(!in_array($id->id, $ids)){
                array_push($ids,$id->id);
            }
        }

        $userId = Auth::id();
        $reels = Reel::find($ids);

        foreach ($reels as $reel){
            $reel['tags'] = $reel->getTags();
            $reel['creator'] = $reel->getCreatorName();
            
            $reel_licences = $reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $reel->qualities->makeVisible(['link']);
            } else {
                $reel->qualities;
            }
            
            $creatorID = $reel->user_id;
            $reel['is_creator'] = $userId === $creatorID ? true : false;
            $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();
        }

        return response()->json([
            "response"=>compact(['reels','pageSeed'])
        ],200);

    }

    public function getTagCollections(Request $request){
        $pageindex = intval($request->get("collectionPageIndex")) ;
        $pageSeed = intval($request->get("collectionPageSeed")) ;
        $pagesize = intval($request->get("collectionPageSize")) ;
        $tagIndex = intval($request->get('tagIndex'));
        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }

        $collectionIds = DB::table('collection_tags')
            ->join('collections','collection_tags.collection_id', '=','collections.id')
            ->where('collection_tags.tag_id','=',$tagIndex)
            ->where('collections.deleted_at','=',null)
            ->where('collections.status','=','published')
            ->select("collections.id")
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get()
            ->toArray();
        $ids = [];
        foreach ($collectionIds as $id ){
            if(!in_array($id->id, $ids)){
                array_push($ids,$id->id);
            }
        }

        $userId = Auth::id();
        $tempcollections = Collection::find($ids);
        $collections = [];
        foreach ($tempcollections as $collection){
            $collection['reel'] = $collection->getMostPopularReel();
            $collection['reels'] = $collection->getReels();
            $collection['reelcount'] = count($collection->getReels());
            $collection->tags;
            $collection->user;
            $creatorID = $collection->user_id;
            $collection['is_creator'] = $userId === $creatorID ? true : false;

            $collection['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('collection_id', $collection->id)->exists();

            if(count($collection['reels'] ) > 0){
                array_push($collections,$collection);
            }
        }

        return response()->json([
            "response"=>compact(['collections','pageSeed'])
        ],200);

    }
}
