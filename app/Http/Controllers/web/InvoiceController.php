<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Invoice;
use App\Models\Order;
use Barryvdh\DomPDF\Facade as PDF;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InvoiceController extends Controller
{

    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    public function validateOrderStatus($order_number){
        $order = Order::where("order_number",$order_number)->firstOrFail();
        $invoice = Invoice::where("order_id",$order->id)->first();

        $user = Auth::user();

        if($user->id == $order->user_id){
            if($invoice != null){

                return $this->redirectByStatus($invoice);

            }else{
                $invoice = Helper::generateInvoiceFromOrder($order);

                return redirect()->route("editInvoice",["invoiceId"=>$invoice->id]);
            }
        }else{
            return redirect()->back();
        }
    }

    public function show($invoiceId){

        $invoice = Invoice::findOrFail($invoiceId);

        $user = Auth::user();

        if($user->id == $invoice->user_id){
            if(!$invoice->complete){
                return redirect()->route("editInvoice", ["invoiceId" => $invoice->id]);
            }else{
                $companyInfo = $this->getSiteGeneralSetting();

                $userInvoice = Invoice::with("order","order.orderItems","order.orderItems.reel","order.orderItems.licence")->where("id",$invoice->id)->first();

                return view("invoice.show",compact(["companyInfo","userInvoice"]));
            }
        }else{
            return redirect()->back();
        }

    }

    public function edit($invoiceId){
        $invoice = Invoice::findOrFail($invoiceId);;
        $user = Auth::user();

        if($user->id == $invoice->user_id){
            if ($invoice->complete) {
                return redirect()->route("showInvoice", ["invoiceId" => $invoice->id]);
            }else{
                $companyInfo = $this->getSiteGeneralSetting();

                $userInvoice = Invoice::with("order","order.orderItems","order.orderItems.reel","order.orderItems.licence")->where("id",$invoice->id)->first();

                return view("invoice.edit",compact(["companyInfo","userInvoice"]));
            }
        }else{
            return redirect()->back();
        }

    }

    /**
     * @param $invoice
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectByStatus($invoice): \Illuminate\Http\RedirectResponse
    {
        if ($invoice->complete) {
            return redirect()->route("showInvoice", ["invoiceId" => $invoice->id]);
        } else {
            return redirect()->route("editInvoice", ["invoiceId" => $invoice->id]);
        }
    }

    /**
     *
     */

    public function getSiteGeneralSetting(){
        return Helper::getSiteGeneralSetting();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveAndDownload(Request $request){
        $data = $request->validate([
            "name"=>"required",
            "address"=>"required",
            "invoiceId"=>"required",
        ]);

        $user = Auth::user();
        $invoice = Invoice::find($data["invoiceId"]);

        if($user->id == $invoice->user_id){
//            $user->update(['email'=> $email]);
            $invoice->update([
                "billing_name"=>$data['name'],
                "billing_address"=>$data['address'],
                "complete"=>true

            ]);
            $invoice->save();
            return response()->json([
                'response'=>[
                    "msg"=>"success"
                ]
            ],200);
        }else{
            abort(404);
        }
    }


    public function downloadInvoice(Invoice $invoice){

        $user = Auth::user();

        if($user->id === $invoice->user_id){

            $companyInfo = $this->getSiteGeneralSetting();

            $userInvoice = Invoice::with("order","order.orderItems","order.orderItems.reel","order.orderItems.licence")->where("id",$invoice->id)->first();

            return view('pdf.invoice', compact(["companyInfo","userInvoice"]));


//            $pdf = PDF::loadFile("http://www.google.com");

//            $pdf = PDF::loadView('pdf.invoice', compact(["companyInfo","userInvoice"]))
//                ->setOptions([
//                    'defaultFont'=>'CerebriSansPro,sans-serif',
//                    'isJavascriptEnabled'=>true,
//                    ]);
//            return $pdf->stream();
        }else{
            abort(403);
        }

    }

}
