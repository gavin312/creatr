<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Facade\FlareClient\Http\Response;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentMethodController extends Controller
{
    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    public function getPaymentInfo(){

        $user = Auth::user();
        $user->createOrGetStripeCustomer();
        $paymentMethods = $user->paymentMethods();
        $defaultMethod = $user->defaultPaymentMethod();
        $paymentIntent =  $user->createSetupIntent();

        return Response()->json([
            'response'=>compact(["paymentMethods","defaultMethod","paymentIntent"])
        ],200);
    }

    public function deletePaymentMethod(Request $request){
        $data = $request->validate([
            "payment_method_id"=>"required"
        ]);
        $user = Auth::user();

        $payment_method_id = $data["payment_method_id"];

        $paymentMethod = $user->findPaymentMethod($payment_method_id);

        $paymentMethod->delete();

        if((!$user->hasDefaultPaymentMethod())&&$user->hasPaymentMethod()){
            $newPaymentMethod = $user->paymentMethods()->first();
            $user->updateDefaultPaymentMethod($newPaymentMethod->id);
        }

        return Response()->json([
            'response'=>[
                "msg"=>"success!"
            ]
        ],200);
    }

    public function setupDefaultPaymentMethod(Request $request){
        $data = $request->validate([
            "payment_method_id"=>"required"
        ]);
        $user = Auth::user();

        $payment_method_id = $data["payment_method_id"];

        $user->updateDefaultPaymentMethod($payment_method_id);

        return Response()->json([
            'response'=>[
                "msg"=>"success!"
            ]
        ],200);

    }

    public function addPaymentMethod(){

    }
}
