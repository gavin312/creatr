<?php

namespace App\Http\Controllers\web;

use App\Library\Helper;
use App\Mail\GeneralMail;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\PayoutTransaction;
use App\Models\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Exceptions\IncompletePayment;

use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;
use Stripe\Exception\ApiErrorException;

class CheckoutController extends Controller
{

    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    public function show()
    {
        $user = Auth::user();

        $cart = Cart::where('user_id',$user->id)->where('status', 1)->first();

        $stripeCustomer = $user->createOrGetStripeCustomer();
        $paymentIntent =  $user->createSetupIntent();
        $paymentMethods = $user->paymentMethods();
        $defaultMethod = $user->defaultPaymentMethod();


        if(!$cart){
            $cartItems = null;
            return view("checkout.show",compact(["user","cartItems","paymentIntent",'paymentMethods','defaultMethod']));
        }else{
            $cartItems = CartItem::with("reel",'licence',"reel.licences")
                ->where("cart_id",$cart->id)
                ->get()
                ->map(function($cartItem){

                    $licence_id = $cartItem->licence_id;
                    $reel_id = $cartItem->reel_id;

                    $reel_licence_item = DB::Table("licence_reels")->where("licence_id",$licence_id)
                        ->where("reel_id",$reel_id)->first();
                    $price = $reel_licence_item->price;
                    $cartItem["price_num"] = $price;
                    $cartItem["price"] = number_format($price,2);
                    return $cartItem;
                });

            return view("checkout.show",compact(["user",'cartItems','paymentIntent','paymentMethods','defaultMethod']));
        }

    }


    public function verifyItems(Request $request){

        $data = $request->validate([
            "items"=>"required",
        ]);

        $items = $data["items"];

        $user = Auth::user();

        $cart = Cart::where('user_id',$user->id)->where('status', 1)->first();


        if(!$cart){

            return Response()->json([
                'response'=>[
                    'validate'=>false
                ]
            ],200);

        }else{
            $cartItems = CartItem::with("reel",'licence',"reel.licences")
                ->where("cart_id",$cart->id)
                ->get()
                ->map(function($cartItem){

                    $licence_id = $cartItem->licence_id;
                    $reel_id = $cartItem->reel_id;

                    $reel_licence_item = DB::Table("licence_reels")->where("licence_id",$licence_id)
                        ->where("reel_id",$reel_id)->first();
                    $price = $reel_licence_item->price;
                    $cartItem["price_num"] = $price;
                    $cartItem["price"] = number_format($price,2);
                    return $cartItem;
                });


            $pass = false;

            foreach ($cartItems as $cartItem){
                $found = false;

                foreach($items as $item){
                    $tempitem = json_decode($item);
                    if($cartItem->id == $tempitem->id){
                        $found = true;
                        $pass = true;
                        if($cartItem->reel_id != $tempitem->reel_id){
                            $pass = false;
                            break 2;
                        }
                        if($cartItem->licence_id != $tempitem->licence_id){
                            $pass = false;
                            break 2;
                        }
                        if($cartItem->quantity != $tempitem->quantity){
                            $pass = false;
                            break 2;
                        }
                        if($cartItem->price_num != $tempitem->price_num){
                            $pass = false;
                            break 2;
                        }
                    }
                }

                if(!$found){
                    $pass = false;
                    break;
                }
            }


            return Response()->json([
                'response'=>[
                    'validate'=>$pass
                ]
            ],200);
        }

    }


    public function checkOutWithStripeToken(Request $request){
        // order_placed_date is used to record the local time for the order.
        // It created when user clicked the 'PAY' button.
        $data = $request->validate([
            "payment_method_id"=>"required",
            "payment_method_name"=>"required",
            "order_placed_date"=>"required"
        ]);
        $user = Auth::user();


        $cart = Cart::where('user_id',$user->id)->where('status', 1)->first();

        if(!$cart){
            return Response()->json([
                'response'=>[
                    'success'=>false
                ]
            ],200);
        }else{
            $cartItems = CartItem::with("reel",'licence',"reel.licences")
                ->where("cart_id",$cart->id)
                ->get()
                ->map(function($cartItem){

                    $licence_id = $cartItem->licence_id;
                    $reel_id = $cartItem->reel_id;

                    $reel_licence_item = DB::Table("licence_reels")->where("licence_id",$licence_id)
                        ->where("reel_id",$reel_id)->first();
                    $price = $reel_licence_item->price;
                    $cartItem["price_num"] = $price;
                    $cartItem["price"] = number_format($price,2);
                    return $cartItem;
                });


            $amount = 0;

            foreach ($cartItems as $cartItem){
                $amount += $cartItem->price_num *  $cartItem->quantity;
            }


            $str = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);

            $order = $user->orders()
                ->create([
                    "cart_id"=>$cart->id,
                    "status"=>"pending",
                    "amount"=>$amount,
                    "order_number"=>$str,
                    "payment_method_name"=>$data["payment_method_name"],
                    "order_placed_date"=>$data["order_placed_date"]
                ]);

            foreach ($cartItems as $cartItem){
                for($index = 0; $index <  $cartItem->quantity ; $index ++){
                    $orderItem = $order->orderItems()
                        ->create([
                            "reel_id"=>$cartItem->reel->id,
                            "licence_id"=>$cartItem->licence_id,
                            "price"=>$cartItem["price_num"]
                        ]);
                }
            }


            // TODO: empty cart
            $cart->status = false;

            $cart->save();

            try {
                $payment = $user->charge($amount * 100  ,$data["payment_method_id"]);
                $payment = $payment->asStripePaymentIntent();
                $order->transaction_id = $payment->charges->data[0]->id;
                $order->status = 'completed';
                // generate Invoice
                $order->payment_intent = $data["payment_method_id"];
                $order->save();

                Helper::generateInvoiceFromOrder($order);
                $this->createTransactionsBasedOnOrder($order);
                $this->proceedTransaction($order);

                // Mailing: Send order confirmation email to customers.
                $order["orderItems"] = $cartItems;
                $order["user"] = $user;

                Log::info("Order Information",[$order]);
                if($user->email != null){
                    // TODO: still success without sending email
                    Mail::to($user->email)->send(new OrderMail($order));
                }


                return Response()->json([
                    'response'=>[
                        'success'=>true,
                        'order_number'=>encrypt($order->order_number),
                    ]
                ],200);

            }catch (IncompletePayment $exception){

                return Response()->json([
                    'response'=>[
                        'redirect'=>route(
                            'cashier.payment',
                            [$exception->payment->id, 'redirect' => route('paymentComplete',['order_number'=> encrypt($order->order_number) ])]
                        ),
                    ]
                ],200);
            }
        }
    }

    public function paymentComplete(Request $request){


        $data =  $request->validate([
            "order_number"=>"required",
            "success"=>"required"
        ]);

        $order_number = decrypt($data["order_number"]);

        $order = Order::where("order_number",$order_number)->first();

        $status = $data["success"] == "true";


        if($status){
            $payment_intent = $order->payment_intent;
            $order->status = 'completed';
            $order->save();
            Helper::generateInvoiceFromOrder($order);
            $this->createTransactionsBasedOnOrder($order);
            $this->proceedTransaction($order);


            $order->orderItems = $order->cart()->first()->cartItems()->with('reel','licence')->get();
            $user = $order->user()->first();
            $order->user = $user;
            if($user->email != null){
                Mail::to($user->email)->send(new OrderMail($order));
            }

            // generate Invoice
            return redirect()->route("showThankyou",["order_number"=>encrypt($order_number)]);
        }else{
            $order->status = 'fail';
            $cart = Cart::find($order->cart_id);
            $cart->status = true;
            $cart->save();
            $order->save();
            return redirect()->route("home");
        }

    }

    public function addPaymentMethod(Request $request){
        $data = $request->validate([
            "paymentMethod"=>"required",
            "setAsDefault"=>"required",
        ]);

        $user = Auth::user();


        $setAsDefault = $data["setAsDefault"];

        $paymentMethod = $data["paymentMethod"];


        if(!$user->hasPaymentMethod()){
            $setAsDefault = true;
        }

        $user->addPaymentMethod($paymentMethod);


        if($setAsDefault){
            $user->updateDefaultPaymentMethod($paymentMethod);
        }

        return Response()->json([
            'response'=>[
                'success'=>true
            ]
        ],200);
    }

    public function createTransactionsBasedOnOrder(Order $order){

        $orderItems = $order->orderItems()->with("reel","reel.user")->get();

        foreach ($orderItems as $orderItem){
            $totalAmount = intval( doubleval($orderItem->price )* 100);
            $platformAmount = $totalAmount * 0.08;
            $stripeAmount = $totalAmount *0.029 + 30;
            $payoutAmount = $totalAmount - $platformAmount - $stripeAmount;
            $orderItem->payoutTransactions()->create([
                "order_id"=>$order->id,
                "user_id"=>$orderItem->reel->user->id,
                "reel_id"=>$orderItem->reel_id,
                "licence_id"=>$orderItem->licence_id,
                "total_amount"=>$totalAmount,
                "platform_amount"=>$platformAmount,
                "stripe_amount"=>$stripeAmount,
                "payout_amount"=>$payoutAmount,
            ]);
        }
        return $orderItems;

    }

    public function proceedTransaction(Order $order){
        $transactions = $order->payoutTransactions()->get();

        $successUsers = [];
        $failUsers = [];
        foreach ($transactions as $transaction){
            $result = Helper::transfer($transaction);
            if($result["state"]){
                $transaction->transaction_id = $result["content"];
                $transaction->state = true;
                $transaction->save();

                if(!in_array($transaction->user_id,$successUsers)){
                    array_push($successUsers,$transaction->user_id);
                }
            }else{
                if(!in_array($transaction->user_id,$failUsers)){
                    array_push($failUsers,$transaction->user_id);
                }
            }
        }

        Log::info("user message",[$successUsers,$failUsers]);
        $this->sendEmailToSeller($successUsers,$failUsers);

    }





    public function sendEmailToSeller(Array $successUserIDs,Array $failUserIDs){

        $successUsers = User::find($successUserIDs);

        $failUsers = User::find($failUserIDs);


        Log::info("users message",[$successUsers,$failUsers]);

        if(!empty($successUsers)){
            foreach($successUsers as $successUser){
                if($successUser -> email){
                    $data = [
                        "title" => "Your Reel was purchased.",
                        "username" => $successUser->name,
                        "messageBody" => "Congrats!
Your video was purchased, and your payout has been processed by Stripe.
It might take 2-3 days for the transaction to go through.
"
                    ];
                    Mail::to($successUser -> email)->send(new generalMail($data));
                }
            }
        }

        if(!empty($failUsers)){
            foreach($failUsers as $failUser){
                if($failUser -> email){
                    $data = [
                        "title" => "Your Reel was purchased.",
                        "username" => $failUser->name,
                        "messageBody" => "Congrats!
Your video was purchased, and we tried to process your payment by Stripe.
However, there seems to be some errors with your payout info. Please double check in setting page.
Should there be any problem, please let me know."
                    ];
                    Mail::to($failUser -> email)->send(new generalMail($data));
                }
            }
        }



    }
}



