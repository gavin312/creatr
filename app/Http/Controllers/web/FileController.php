<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Library\ResponseMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;
use Vimeo\Laravel\Facades\Vimeo;

class FileController extends Controller
{
    //

    public function uploadUserAvatar(Request $request){

        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png,csv,txt,xlx,xls,pdf|max:2048'
        ]);
        if($request->file()) {
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $image = $request->file('file');
            $storageDirectory = public_path('/storage');
            $filePath = ('/users');
            $img = Image::make($image->path());
            $img->resize(512, 512, function ($const) {
                $const->aspectRatio();
            })->save($storageDirectory.$filePath.'/'.$file_name);

            $destinationPath = $filePath.'/'.$file_name;

            return response()->json(
                [
                    'filePath'=>$destinationPath
                ],200
            );
        }
    }


    public function  uploadThumbnailByBase64(Request $request){
        $base64 = $request->input('base64');
        $thumbnailPath =  Helper::getImageFromBase64($base64);
        return response()->json([
            "response"=>["path"=>$thumbnailPath]
        ],200);
    }

    public function uploadVideo(Request $request){
        $request->validate([
            'file' => 'required|mimes:mp4,3gp,mov,mpeg,mpg,mp2,mpe,vob,dat,qt,moov,qtvr,flc,wmv,avi|max:204800'
        ]);
        if($request->file()) {

            $file = $request->file('file');
            $file_name = time().'_'.$request->file->getClientOriginalName();


            $path = public_path('/storage').'/Public/';
            $file->move($path, $file_name);
            $destinationPath = $path.$file_name;
//            response is the video path

            $response = Vimeo::upload($destinationPath,[
                'name'=>$file_name,
                'description'=>$file_name,
                'privacy' => [
                    'view' => 'disable',
                    'embed'=> 'public',
                ],
                'embed'=>[
                    'buttons'=>[
                        'embed'=>false,
                        'like'=>false,
                        'scaling'=>false,
                        'share'=>false,
                        'watchlater'=>false,

                    ],
                    'logos'=>[
                        'custom'=>[
                            'active'=>false,
                            'sticky'=>false,
                        ],
                        'vimeo'=>false

                    ],
                    'title'=>[
                        'name'=>'hide',
                        'owner'=>'hide',
                        'portrait'=>'hide',
                    ]

                ]

            ]);

            if(Storage::exists('public/Public/'.$file_name)){
                Storage::delete('public/Public/'.$file_name);
            }else{
                Log::error('file does not exist',[$file_name,$destinationPath]);
            }
            $responseArray = explode('/',$response);

            $path = $response;
            $id = end($responseArray);

            Log::info('response Type',);
            return response()->json(
                [
                    'response'=>$response
                ],200
            );
        }
    }

    public function checkVideoUploadedStatus(Request $request){

        $videoId = $request->get('videoId');
        $response =  Vimeo::request('/videos/'.$videoId,[
            "fields"=>"transcode.status"
        ]);

        if($response['body']['transcode']['status']  == "complete"){
            if($this->testThumbnailsGenerated($videoId)){
                return response()->json(
                    [
                        'response'=>$response['body']['transcode']
                    ],200
                );
            }else{
                return response()->json(
                    [
                        'response'=>['status'=>'in_progress']
                    ],200
                );
            }
        }

        return response()->json(
                [
                   'response'=>$response['body']['transcode']
                ],200
            );
    }




    public function testThumbnailsGenerated($videoId){
        $response =  Vimeo::request('/videos/'.$videoId.'/pictures');

        $thumbNails = $response['body']['data'];

        if(count($thumbNails) > 0){
            return true;
        }else{
            return false;
        }

    }

    public function getThumbNailFromVideoId(Request $request){
        $videoId = $request->get('videoId');
        $response =  Vimeo::request('/videos/'.$videoId.'/pictures');

        $thumbNails = $response['body']['data'];
        $thumbNails_images = [];

        foreach ($thumbNails as $thumbNail){
            $sizes = $thumbNail['sizes'];
            $length = count($sizes);
            $selected = $sizes[$length - 1];
            $thumbnailPath =  Helper::downloadImageIntoThumbNails($selected['link']);
            array_push($thumbNails_images,$thumbnailPath);
        }
        return response()->json(
            [
                'response'=>[
                    "thumbnails"=>$thumbNails_images
                ]
            ],200
        );
    }


    public function createPreviewFromVideoID(Request $request){

        $videoId = $request->get('videoId');
        $response = Vimeo::request('/videos/'.$videoId.'/animated_thumbsets',['duration'=>3],'POST');
        return response()->json(
            [
                'response'=>$response
            ],200
        );
    }


    public function createPreviewFromPullApproach(Request $request){

        $videoId = $request->get('link');
        $response = Vimeo::request('/me/videos',['upload'=>[
            "approach"=>"pull",
            "size"=>1000,
            "link"=>$videoId
        ]],'POST');
        return response()->json(
            [
                'response'=>$response
            ],200
        );
    }


    public function getPreviewsFromVideoId(Request $request){

        $videoId = $request->get('videoId');
        $response =  Vimeo::request('/videos/'.$videoId.'/animated_thumbsets');

        $previews = $response['body']['data'];

        $sizes = $previews[0]["sizes"];

       usort($sizes,function($size1,$size2){
            if ($size1["file_size"] == $size2["file_size"]) {
                return 0;
            }
            return ($size1["file_size"]< $size2["file_size"]) ? -1 : 1;
        });

       $index = 0;
       $count = count($sizes);
       if($count > 2){
           $index = count($sizes) - 2;
       }else{
           $index = 0;
        }


        return response()->json(
            [
                'previews'=>$sizes[count($sizes) - 2]
            ],200
        );

    }


    public function getPreviewFromVideoId(Request $request){
        $videoId = $request->get('videoId');
        $response =  Vimeo::request('/videos/'.$videoId.'/animated_thumbsets');

        $previews = $response['body']['data'];
        $preview_gif = "" ;

        if(count($previews) > 0){
            $preview = $previews[0];
            $sizes = $preview['sizes'];
            $maxSize = null;
            foreach($sizes as $size){
                if($maxSize == null || $maxSize['file_size'] <= $size['file_size']){
                    $maxSize = $size;
                }

            }
            if($maxSize != null){
                $preview_gif =  Helper::downloadImageIntoPreviews($maxSize['link']);
                return $preview_gif;
            }

        }

        return null;
    }


    public function uploadThumbNailImage(Request $request){
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png|max:2048'
        ]);
        if($request->file()) {
            return response()->json(
                [
                    'filePath'=>$this->saveImageToFolder($request,'/thumbnails')
                ],200
            );
        }
    }

    public function uploadTitleLogoImage(Request  $request){
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png|max:2048'
        ]);
        if($request->file()) {
            return response()->json(
                [
                    'filePath'=>$this->saveImageToFolder($request,'/title_logos')
                ],200
            );
        }

    }

    public function saveImageToFolder(Request $request, String $path){
        if($request->file()) {
            $file_name = time().'_'.$request->file->getClientOriginalName();
            $image = $request->file('file');
            $storageDirectory = public_path('/storage');
            $filePath = ($path);
            Image::make($image->path())->resize(477, 270)->save($storageDirectory.$filePath.'/'.$file_name);

            $destinationPath = $filePath.'/'.$file_name;

            return $destinationPath;
        }
    }


}
