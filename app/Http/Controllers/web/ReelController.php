<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Category;
use App\Models\Quality;
use App\Models\Reel;
use App\Models\Wishlist;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Collection;
use Vimeo\Laravel\Facades\Vimeo;
use App\Jobs\SendGeneralEmailJob;

class ReelController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified'])->only(['create','store','destroy','edit','update','likeReel']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $mostPopularReel = Reel::orderBy('views',"DESC")->firstOrFail();
        $mostPopularReel['creatorName'] =  $mostPopularReel->getCreatorName();
        $count = Reel::where("status","published")->where("only_in_collection",false)->get()->count();
        $reelsInfo = json_encode(["reel_count" =>  $count]) ;
        return view('reel.index',compact(["mostPopularReel", "reelsInfo"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function create(Request $request)
    {
        $tags = Tag::orderBy('name','ASC')->get();
        $categories = Category::all();
        $collectionID = $request->get('collection');
        if(empty($collectionID)){
            return view('reel.create',compact(['tags','categories']));
        }else{
            $collection = Collection::find($collectionID);
            $collections = json_encode([$collection]);
            return view('reel.create',compact(['tags','categories','collections']));
        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'video_id' => 'required',
            'title'=>'required',
            'description'=>'sometimes',
            'status'=>'required',
            'thumbnail'=>'required',
            'tags'=>'sometimes',
            'collection'=>'sometimes',
            'category_id'=>'required',
            'only_in_collection'=>'sometimes',
            'platforms'=>'sometimes',
            'rate_id'=>'required',
            'licences'=>'required',
            'title_logo'=>'sometimes',
        ]);


        $tags = $data['tags'];
        $tagIDs = TagController::createTagsFromTagsArray($tags,$data['thumbnail']);


        $user = Auth::user();

        if(!$this->checkReelNameAvailable($data['title'])){
            return Response()->json([
                'response'=>[
                    'errormsg'=>"Title is Occupied! Please try Another one"
                ]
            ],409);
        }

        $reel = Reel::create([
            'video_id'=>$data['video_id'],
            'title'=>$data['title'],
            'description'=>$data['description'],
            'status'=>$data['status'],
            'thumbnail'=>$data['thumbnail'],
            'user_id'=>$user->id,
            'category_id'=>$data['category_id'],
            'rate_id'=>$data['rate_id'],
            'title_logo'=>$data['title_logo']
        ]);

        $response = Vimeo::request('/videos/'.$data['video_id'].'/animated_thumbsets',['duration'=>3],'POST');

        $this->getQualityDetailfromVimeoByVideoID($reel);

        $reel->tags()->sync($tagIDs);

        $platforms = $data['platforms'];
        if(count($platforms) > 0){
            $reel->platforms()->sync($platforms);
        }

        $licences = $data['licences'];
        $targetLicenceGroup = [];

        foreach ($licences as $licence){
            $targetLicenceGroup [$licence["licence_id"]] = ['price'=>$licence["price"]];
        }

        $reel->licences()->sync($targetLicenceGroup);

        if(!empty($data['collection'])){
            $reel->collections()->sync($data['collection']);
        }
        if(key_exists('only_in_collection',$data) && $data['only_in_collection']){
            $reel->only_in_collection = "1";
        }

        $this->getReelHandleFromReelTitle($reel);

        return Response()->json([
            'response'=>[
                'reelID'=>$reel->url_handle,
            ]
        ],200);

    }

    /**
     * Display the specified resource.
     *
     * @param String $reel_url_handle
     * @return Response
     */
    public function show(String $reel_url_handle)
    {
        $reel = Reel::where('url_handle',$reel_url_handle)->firstOrFail();
//        TODO：only in collection, draft, situation
        $tags = json_encode($reel->getTags()) ;
        $creator = User::find($reel->user_id);
        $profile = $creator->getProfile();

        $isCreator = false;
        $reel->views = $reel->views + 1;

        $reel->save();
        if(empty($reel->preview)){

            $reel->preview = $this->getPreviews($reel->video_id);
            $reel->save();
        }

        $isFollowing = false;
        $category = $reel->getCategory();
        $user = null;
        $showReel = true;
        if($reel->status == "draft" || $reel->only_in_collection == "1"){
            $showReel = false;
        }
        if (Auth::check()){

            $user = Auth::user();

            $isCreator = ($user->id === $reel->user_id);
            if(!$isCreator){
                $isFollowing = Helper::isFollowing($user->id,$reel->user_id);
            }else{
                $showReel = true;
            }
            $likeReel = Helper::reelLikedByUser($reel->id,$user->id);
        }else{
            $isCreator = false;
            $likeReel = false;
        }

        if(!$showReel){
            abort(404);
        }

        $reel->platforms;
        
        $reel_licences = $reel->licences;
        // Check if the reel is free
        if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
            $reel->qualities->makeVisible(['link']);
        } else {
            $reel->qualities;
        }
        $reel->rate;
        // $reel['qualities'] = $qualities;
        $comments = $reel->getComments()->toArray();
        $formatComment = Helper::formatComment($comments);
        $reelCount = $creator->getReelsCount();
        $creator_info = json_encode(compact(['creator','profile','isCreator','isFollowing','likeReel','user','reelCount']));
        $collections = json_encode( $reel->getCollections());
        return view('reel.show',compact(['tags','reel','creator_info','category','formatComment','collections']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param String $reel_url_handle
     * @return Response
     */
    public function edit(String $reel_url_handle)
    {


        $reel = Reel::with("rate",'platforms','licences')->where('url_handle',$reel_url_handle)->firstOrFail();

        if (Auth::check()){

            $user = Auth::user();

            $isCreator = ($user->id === $reel->user_id);

            if($isCreator){

                $tags = Tag::orderBy('name','ASC')->get();
                $categories = Category::all();
                $selectedTags = json_encode( $reel->getTags());
                $collections = json_encode($reel->getCollections());

                return view('reel.edit',compact(['reel','tags','categories','selectedTags','collections']));
            }else{

                return redirect()->back();
            }

        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reel  $reel
     * @return Response
     */
    public function update(Request $request, Reel $reel)
    {

        $data = $request->validate([
            'video_id' => 'required',
            'title'=>'required',
            'description'=>'sometimes',
            'status'=>'required',
            'thumbnail'=>'required',
            'tags'=>'sometimes',
            'collection'=>'sometimes',
            'category_id'=>'required',
            'platforms'=>'sometimes',
            'rate_id'=>'required',
            'licences'=>'required',
            'title_logo'=>'sometimes',

        ]);

        $user = Auth::user();

        $isCreator = ($user->id === $reel->user_id);

        if($isCreator){

            if(!$this->checkReelNameAvailable($data['title'],$reel->id)){
                return Response()->json([
                    'response'=>[
                        'errormsg'=>"Title is Occupied! Please try Another one"
                    ]
                ],409);
            }

            $reel['video_id'] = $data['video_id'];
            $reel['title'] = $data['title'];
            $reel['description'] = $data['description'];
            $reel['status'] = $data['status'];
            $reel['thumbnail'] = $data['thumbnail'];
            $reel['user_id'] = $user->id;
            $reel['category_id'] = $data['category_id'];
            $reel['rate_id'] = $data['rate_id'];
            $reel['title_logo'] = $data['title_logo'];
            $reel->save();
            $tags = $data['tags'];
            $tagIDs = TagController::createTagsFromTagsArray($tags,$data['thumbnail']);
            $reel->tags()->sync($tagIDs);
            $reel->collections()->sync($data['collection']);

            $platforms = $data['platforms'];
            if(count($platforms) > 0){
                $reel->platforms()->sync($platforms);
            }
            $licences = $data['licences'];
            $targetLicenceGroup = [];

            foreach ($licences as $licence){
                $targetLicenceGroup [$licence["licence_id"]] = ['price'=>$licence["price"]];
            }

            $reel->licences()->sync($targetLicenceGroup);

            $this->getReelHandleFromReelTitle($reel);

            return Response()->json([
                'response'=>[
                    'reelID'=>$reel->url_handle,
                ]
            ],200);
        }else{
            return Response()->json([
                'response'=>[
                    'msg'=>'Permission denied!'
                ]
            ],403);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reel  $reel
     * @return Response
     */
    public function destroy(Reel $reel)
    {

        if (Auth::check()){

            $user = Auth::user();

            $isCreator = ($user->id === $reel->user_id);

            if($isCreator){
                $reel->delete();
                return Response()->json([
                    'response'=>[
                        'msg'=>'Delete Success'
                    ]
                ],200);
            }else{
                return Response()->json([
                    'response'=>[
                        'msg'=>'Permission denied!'
                    ]
                ],403);
            }

        }

    }

    public function likeReel(Request $request){
        $user = Auth::user();
        $reel_id = $request->get("reel_id");
        $reel = Reel::find($reel_id);
        if(Helper::reelLikedByUser($reel->id,$user->id)){
            $reel->likeUsers()->detach($user->id);
            $msg = "Unlike Successfully! ";
            $like = false;
            $reel->likes = $reel->likes - 1;
            $reel->save();
        //unlike the reel

        }else{
            $reel->likeUsers()->attach($user->id);
            $msg = "Like Successfully! ";
            $reel->likes = $reel->likes + 1;
            $reel->save();
            $like = true;
            // like

            $temp_reel = Reel::with("user:id,email,name,activity_notification")->where("id", $reel_id)->first();
            $send_to_user = [
                "email" => $temp_reel["user"]["email"],
                "name" => $temp_reel["user"]["name"],
                "activity_notification" => $temp_reel["user"]["activity_notification"]
            ];

            if ($send_to_user["activity_notification"] == 1) {
                $data = [
                    'title' => 'New Notification',
                    'username' => $send_to_user["name"],
                    'messageBody' => 'Good job! Someone likes you reel ['.$temp_reel->title.']!'
                ];
                dispatch(new SendGeneralEmailJob($send_to_user["email"], $data));
            }
        }

        return Response()->json([
            'response'=>[
                'msg'=>$msg,
                'likes'=>$reel->likes,
                'like'=>$like
            ]
        ],200);

    }


    public function getPreviews($videoID){

        $response =  Vimeo::request('/videos/'.$videoID.'/animated_thumbsets');

        $body = $response['body'];

        if(!array_key_exists("error",$body)){

            $previews = $body['data'];
            $preview_gif = "" ;
            if(count($previews) > 0){
                $preview = $previews[0];
                $sizes = $preview['sizes'];
                $selectedSize =  null;

                if(count($sizes) > 0){
                    usort($sizes,function($size1,$size2){
                        if ($size1["file_size"] == $size2["file_size"]) {
                            return 0;
                        }
                        return ($size1["file_size"]< $size2["file_size"]) ? -1 : 1;
                    });

                    $index = 0;
                    $count = count($sizes);
                    if($count > 2){
                        $index = count($sizes) - 2;
                    }else{
                        $index = 0;
                    }

                    $selectedSize  = $sizes[$index];

                    if($selectedSize != null){
                        $preview_gif =  Helper::downloadImageIntoPreviews($selectedSize['link']);
                        return $preview_gif;
                    }
                }
            }
        }

        return "";
    }

    /**
     * get top twn most popular reels
     */
    public function  getMostPopularReels(){

        $reels = Reel::where('status','published')->orderBy('views','DESC')->take(10)->get();

        return response()->json([
            "response"=>compact(['reels'])
        ],200);


    }

    public function  getMostPopularReelsWithTags(){
        // TODO: ALLEN - Exclude wishlisted reel
        $userId = Auth::id();

        $reels = Reel::where('status','published')
            ->orderBy('views','DESC')
            ->take(10)
            ->get()
            ->map(function ($reel) use ($userId) {
                $reel['tags'] = $reel->getTags();
                $reel['creator'] = $reel->getCreatorName();
                
                $reel_licences = $reel->licences;
                // Check if the reel is free
                if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                    $reel->qualities->makeVisible(['link']);
                } else {
                    $reel->qualities;
                }
                $creatorID = $reel->user_id;
                $reel['is_creator'] = $userId === $creatorID ? true : false;
                $reel['is_in_wishlist'] = Wishlist::where('user_id', $userId)->where('reel_id', $reel->id)->exists();

                return $reel;
            })
            ->toArray();

        return response()->json([
            "response"=>compact(['reels'])
        ],200);
    }


    public function checkReelNameAvailable(String $name,$reelId = null){
        $validate = true;
        $reel = null;
        $reels = DB::table('reels')->where('title',$name)->get()->toArray();
        if(count($reels) == 0 ){
            return $validate;
        }else{
            if($reelId){
                $reel = Reel::find($reelId);
                if($reel->title == $name){
                    $validate = true;
                }else{
                    $validate = false;
                }
            }else{
                $validate = false;

            }
            return $validate;
        }
    }

    public function getAllReelsByRandomOrder(Request $request){
        $pageindex = intval($request->get("reelPageIndex")) ;
        $pageSeed = intval($request->get("reelPageSeed")) ;
        $pagesize = intval($request->get("reelPageSize")) ;

        $startIndex = ($pageindex-1) * $pagesize;

        if($pageSeed == ""){
            $pageSeed = strval(random_int(1,100));
        }
        //dd($pageindex,$pageSeed,$pagesize);
        $reels = Reel::where('status','published')
            ->where('only_in_collection','0')
            ->skip($startIndex)
            ->take($pagesize)
            ->inRandomOrder($pageSeed)
            ->get();
        foreach ($reels as $reel){
            $reel['tags'] = $reel->getTags();
            $reel['creator'] = $reel->getCreatorName();
            $reel_licences = $reel->licences;
            // Check if the reel is free
            if (count($reel_licences) == 1 && $reel_licences[0]['is_free'] == true) {
                $reel->qualities->makeVisible(['link']);
            } else {
                $reel->qualities;
            }
        }

        return response()->json([
            "response"=>compact(['reels','pageSeed'])
        ],200);
    }


    public function getReelHandleFromReelTitle(Reel $reel){
        $temp_url_handle = Helper::convertNameToHandle($reel->title);
        $url_handle = $temp_url_handle;
        if($temp_url_handle  == $reel->url_handle){
            return;
        }else{

            $num = 0;
            while(true){

                if (Helper::checkUrlOccupied($url_handle)){
                    $url_handle = $temp_url_handle.$num;
                }else{
                    $reel->url_handle = $url_handle;
                    $reel->save();
                    return;
                }
                $num++;

            }
        }
    }


    public function getQualityDetailfromVimeoByVideoID(Reel $reel){

        $response = Vimeo::request('/videos/'.$reel->video_id,[
            "fields"=>"download"
        ]);
        $reelid = $reel->id;

        $downloadItems = $response["body"]["download"];
        $qualityArray = [];

        foreach ($downloadItems as $downloadItem){
            $tempQuality = [
                "reel_id" =>$reelid,
                "quality"=>$downloadItem["quality"],
                "type"=>$downloadItem["type"],
                "width"=>$downloadItem["width"],
                "height"=>$downloadItem["height"],
                "expires"=>$downloadItem["expires"],
                "link"=>$downloadItem["link"],
                "size"=>$downloadItem["size"],
                "public_name"=>$downloadItem["public_name"],
                "size_short"=>$downloadItem["size_short"],
            ];

            $reelQuality = Quality::where("reel_id",$reelid)->where("size",$downloadItem["size"])->first();

            if(!$reelQuality){
                $reelQuality = new Quality();
                $reelQuality->fill($tempQuality);
                $reelQuality->save();
            }else{
                $reelQuality->expires = $downloadItem["expires"];
                $reelQuality->link = $downloadItem["link"];
                $reelQuality->save();
            }
            array_push($qualityArray,$downloadItem);
        }

    }

    public function getReelDetailfromVimeoByVideoID(){

        $reels = Reel::all();
        foreach ($reels as $reel){
            $this->getQualityDetailfromVimeoByVideoID($reel);
        }
    }
}
