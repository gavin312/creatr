<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Licence;
use App\Models\Reel;
use Facade\FlareClient\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Expr\Cast\Double;

class CartController extends Controller
{


    public function __construct() {
        $this->middleware(['auth','verified']);
    }

    function addItemToCart(Request $request){
        $data = $request->validate([
            "reel_id"=>"required",
            "licence_id"=>"required",
        ]);
        $user = Auth::user();
        $cart = Cart::where('user_id',$user->id)->where('status', 1)->first();
        if($cart == null){
            $cart = new Cart();
            $cart->fill([
                "user_id"=>$user->id,
                "status"=>1,
            ]);
            $cart->save();
            $cartItem = new CartItem();
            $cartItem->fill([
                "cart_id"=>$cart->id,
                "reel_id"=>$data["reel_id"],
                "licence_id"=>$data["licence_id"],
                "quantity"=>1,
            ]);

        }else{
            $cartItem = CartItem::where("cart_id",$cart->id)
                ->where("reel_id",$data["reel_id"])
                ->where("licence_id",$data["licence_id"])
                ->first();

            if($cartItem != null){
                $cartItem["quantity"] = $cartItem["quantity"] + 1;
            }else{
                $cartItem = new CartItem();
                $cartItem->fill([
                    "cart_id"=>$cart->id,
                    "reel_id"=>$data["reel_id"],
                    "licence_id"=>$data["licence_id"],
                    "quantity"=>1,
                ]);
            }
        }
        $cartItem->save();
        return Response()->json([
            'response'=>[
                'msg'=>'Success!'
            ]
        ],200);
    }

    function getCartItems(Request $request){
        $user = Auth::user();

        $cart = Cart::where('user_id',$user->id)->where('status', 1)->first();

        if(!$cart){
            return Response()->json([
                'response'=> []
            ],200);
        }else{
            $cartItems = CartItem::with("reel",'licence',"reel.licences")
                ->where("cart_id",$cart->id)
                ->get()
                ->map(function($cartItem){

                    $licence_id = $cartItem->licence_id;
                    $reel_id = $cartItem->reel_id;

                    $reel_licence_item = DB::Table("licence_reels")->where("licence_id",$licence_id)
                        ->where("reel_id",$reel_id)->first();
                    $price = $reel_licence_item->price;
                    $cartItem["price_num"] = $price;
                    $cartItem["price"] = number_format($price,2);
                    return $cartItem;
                })->toArray();
            return Response()->json([
                'response'=> compact(["cartItems"])
            ],200);
        }
    }

    function editCartItem(Request $request){
        $data = $request->validate([
            "cart_item_id"=>"required",
            "licence_id"=>"required",
            "quantity"=>"required",
        ]);

        // Find the existing cart item
        $cartItem = CartItem::find($data["cart_item_id"]);
        $reel_id= $cartItem->reel_id;
        $cart_id = $cartItem->cart_id;

        // Find whether the reel's licence is already in the cart.
        $targetLicenceItemsCollection = CartItem::where('reel_id',$reel_id)
            ->where('cart_id',$cart_id)
            ->where('licence_id',$data['licence_id'])
            ->get();

        $targetLicenceItems = $targetLicenceItemsCollection->toArray();
        
        // The modified cart item is changed to a new licence which is not in the cart yet,
        // Update this cart item with the new licence / quantity
        if(count($targetLicenceItems) == 0){
            $cartItem['licence_id'] = $data['licence_id'];
            $cartItem['quantity'] = $data['quantity'];
            $cartItem->save();

            // The modified cart item is changed to a new licence which is already in the cart. 
        } else if (count($targetLicenceItems) == 1){
            $firstItem = $targetLicenceItems[0];

            // Updating the cart item itself
            if($firstItem["id"] == $cartItem->id){
                $cartItem['quantity'] = $data['quantity'];
                $cartItem->save();

                // Deleting the 'from' licence's cart item, updating the 'to' licence's cart item's quantity
            } else{
                // TODO: Should set a max quantity?
                $newQuantity= $cartItem['quantity'] + $firstItem["quantity"];
                // Updating the only 'to' cart item.
                foreach ($targetLicenceItemsCollection as $targetCartItem) {
                    $targetCartItem["quantity"] = $newQuantity;
                    $targetCartItem->save();
                }
                
                $cartItem->delete();
            }
        }

        return Response()->json([
            'response'=>[
                'msg'=>'Success!'
            ]
        ],200);

    }

    function deleteCartItem(CartItem $cartItem){


        $cartItem->forceDelete();

        return Response()->json([
            'response'=>[
                'msg'=>'Success!'
            ]
        ],200);

    }

}
