<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Prize;
use Illuminate\Http\Request;

class PrizeController extends Controller
{
    public function getAllPrizes() {
        $prizes = Prize::orderBy("rank")->get();

        return Response()->json([
            "response"=>compact(["prizes"])
        ],200);
    }
}
