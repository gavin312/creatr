<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Collection;
use App\Models\Comment;
use App\Models\Reel;
use App\Models\User;
use Illuminate\Http\Request;
use App\Jobs\SendGeneralEmailJob;

class CommentController extends Controller
{


    public function __construct()
    {
        $this->middleware(['auth','verified'])->only(['store']);
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $type = $request->get('type');
        $contentId = $request->get('content_id');

        if($type === 'reel'){
            $reel = Reel::find($contentId);
            $comments = $reel->getComments()->toArray();
            $formatComment = Helper::formatComment($comments);
            return response()->json([
                'response'=>$formatComment
            ],200);
        }else{
            $collection = Collection::find($contentId);
            $comments = $collection->getComments()->toArray();
            $formatComment = Helper::formatComment($comments);
            return response()->json([
                'response'=>$formatComment
            ],200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'user_id'=>'required',
            'reel_id'=>'sometimes',
            'collection_id'=>'sometimes',
            'content'=>'required',
            'parent'=>'sometimes',
            'to_user_id'=>'sometimes',
        ]);

        $comment = Comment::create($data);

        // Send email for repply to the user of comment.
        if (array_key_exists('to_user_id', $data) == true && $data["to_user_id"] != null) {
            $send_to_user = User::find($data["to_user_id"], ['email', 'name', 'activity_notification']);
            if ($send_to_user->activity_notification == 1) {
                $data = [
                    'title' => 'New Comment',
                    'username' => $send_to_user->name,
                    'messageBody' => 'Someone replied to your comment!'
                ];
                dispatch(new SendGeneralEmailJob($send_to_user->email, $data));
            }

            // send email to the creator
        } else {
            $send_to_user = [];
            if (array_key_exists('reel_id', $data) == true) {
                $comment_item = Reel::with("user:id,email,name,activity_notification")->where("id", $data["reel_id"])->first();
            } else {
                $comment_item = Collection::with("user:id,email,name,activity_notification")->where("id", $data["collection_id"])->first();
            }
            $send_to_user["email"] = $comment_item["user"]["email"];
            $send_to_user["name"] = $comment_item["user"]["name"];
            $send_to_user["activity_notification"] = $comment_item["user"]["activity_notification"];

            if ($send_to_user["activity_notification"] == 1) {
                $data = [
                    'title' => 'New Notification',
                    'username' => $send_to_user["name"],
                    'messageBody' => 'You have received a new comment!'
                ];
                dispatch(new SendGeneralEmailJob($send_to_user["email"], $data));
            }
        }

        return response()->json([
            'response'=>[
                'msg'=>'Success! '
            ]
        ],200);

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
