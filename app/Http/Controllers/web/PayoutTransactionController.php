<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Mail\GeneralMail;
use App\Models\PayoutTransaction;
use App\Models\Licence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class PayoutTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    // showTransactionHistory
    public function show()
    {
        if (!Auth::check()) {
            return redirect()->back();
        }

        return view('transaction.show');
    }

    public function getMyPayoutTransactions(Request $request)
    {
        $licenceTypes = $request->get('checkedTypes', []);

        $dates = $request->validate([
            'from' => 'date_format:Y-m-d H:i:s|before_or_equal:now',
            'to' => 'date_format:Y-m-d H:i:s|before_or_equal:tomorrow|after_or_equal:from'
        ]);

        $dateIndex = 1;

        $pageSize = 10;
        $userId = Auth::id();

        $query = PayoutTransaction::with(
            "order:id,order_number,created_at,user_id",
            "order.user:id,name",
            "reel:id,title,thumbnail,url_handle,is_banned",
            "licence:id,type"
        )
            ->where('user_id', $userId);
//            ->where('state', 1);

        // TODO: filters
        if (count($licenceTypes) >0) {
            $query->whereIn('licence_id', $licenceTypes);
        }

        if (array_key_exists('from', $dates)) {
            $query = $query->where('created_at', '>=', $dates['from']);
        }

        if (array_key_exists('to', $dates)) {
            $query = $query->where('created_at', '<=', $dates['to']);
        }

        $transactions = $query->orderByDesc('created_at')->paginate($pageSize);

        $data = $transactions;
        $data = $data->makeHidden(['platform_amount', 'stripe_amount']);
        $transactions->data = $data;

        return response()->json([
            'response' => compact(['transactions'])
        ], 200);
    }

    public function getMyPayoutRevenue()
    {
        $userId = Auth::id();
        $calcDays = 7;
        $calcMonth = 30;
        $calcYear = now()->year;

        // Last 7 days
        $date = Carbon::now()->subDays($calcDays);
        // Last 30 days
        $monthDate = Carbon::now()->subDays($calcMonth);

        $query_days = PayoutTransaction::whereDay('created_at', '>=', $date)
            ->where('state', 1)
            ->where('user_id', $userId);
        $days_revenue = $query_days->sum('payout_amount');
        $days_transc = $query_days->count();

        $query_month = PayoutTransaction::whereMonth('created_at', ">=", $monthDate)
            ->where('state', 1)
            ->where('user_id', $userId);
        $month_revenue = $query_month->sum('payout_amount');
        $month_transc = $query_month->count();

        $query_this_year = PayoutTransaction::whereYear('created_at', $calcYear)
            ->where('state', 1)
            ->where('user_id', $userId);
        $this_year_revenue = $query_this_year->sum('payout_amount');
        $this_year_transc = $query_this_year->count();

        $query_all_time = PayoutTransaction::where('state', 1)
            ->where('user_id', $userId);
        $all_time_revenue = $query_all_time->sum('payout_amount');
        $all_time_transc = $query_all_time->count();

        $revenue["days"]["revenue"] = $days_revenue;
        $revenue["days"]["transactions"] = $days_transc;
        $revenue["month"]["revenue"] = $month_revenue;
        $revenue["month"]["transactions"] = $month_transc;
        $revenue["thisYear"]["revenue"] = $this_year_revenue;
        $revenue["thisYear"]["transactions"] = $this_year_transc;
        $revenue["allTime"]["revenue"] = $all_time_revenue;
        $revenue["allTime"]["transactions"] = $all_time_transc;

        return response()->json([
            'response' => compact(['revenue'])
        ], 200);
    }

    // return all transaction items' licence's id and type.
    public function getMyTransactionAllTypes()
    {
        // Exclude FREE licence
        $types = Licence::where('is_free', '!=', '1')->select(["id", "type"])->get();

        return response()->json([
            'response' => compact(['types'])
        ], 200);
    }

    /**
     *
     * admin retry transaction
     * @param PayoutTransaction $transaction
     */

    public function redoTransaction(PayoutTransaction $transaction){

        $user = Auth::user();
        if ($user->hasPermission("edit_payout_transactions")){

            $result = Helper::transfer($transaction);
            if($result["state"]){
                $transaction->transaction_id = $result["content"];
                $transaction->state = true;
                $transaction->save();
                $user = $transaction->user()->first();
                if($user -> email){
                    $data = [
                        "title" => "Your Reel was purchased.",
                        "username" => $user->name,
                        "messageBody" => "Congrats!
Your video was purchased, and your payout has been processed by Stripe.
It might take 2-3 days for the transaction to go through.
"
                    ];
                    Mail::to($user -> email)->send(new generalMail($data));
                }
               return redirect(route('voyager.payout-transactions.index'))->with(['message' => "Success.", 'alert-type' => 'success']);
            }else{
//                $user = $transaction->user()->first();
//
//                if($user -> email){
//                    $data = [
//                        "title" => "Your Reel was purchased.",
//                        "username" => $user->name,
//                        "messageBody" => "Congrats!
//Your video was purchased, and we tried to process your payment by Stripe.
//However, there seems to be some errors with your payout info. Please double check in setting page.
//Should there be any problem, please let me know.
//"];
//                    Mail::to($user -> email)->send(new generalMail($data));
//                }
                return redirect(route('voyager.payout-transactions.index'))->with(['message' => "The Transaction didn't go through. Please check your stripe setting.", 'alert-type' => 'error']);
            }
        }else{
            abort(404);
        }
    }



    /**
     * creatr remake transaction
     *
     */

    public function remakeTransaction(PayoutTransaction $transaction){
        $user = Auth::user();

        if ($user->id === $transaction->user_id){

            if($transaction->state == true){
                abort(404);
            }

            $result = Helper::transfer($transaction);
            if($result["state"]){
                $transaction->transaction_id = $result["content"];
                $transaction->state = true;
                $transaction->save();
                $user = $transaction->user()->first();
                if($user -> email){
                    $data = [
                        "title" => "Transaction Success!",
                        "username" => $user->name,
                        "messageBody" => "Congrats!
The transaction you requested is being processed.
It might take 2-3 days for the transaction to go through.
"
                    ];
                    Mail::to($user -> email)->send(new generalMail($data));
                }


                return response()->json([
                    "response"=>[
                        "success"=>true,
                        "message"=>"Transaction Success!"
                    ]
                ],200);
//                return redirect(route('voyager.payout-transactions.index'))->with(['message' => "Success.", 'alert-type' => 'success']);
            }else{

                $user = $transaction->user()->first();

                if($user -> email){
                    $data = [
                        "title" => "Transaction failed.",
                        "username" => $user->name,
                        "messageBody" => "Sorry!
We have tried to process you request. However, it failed to go through.
Please review your payout info to make everything correct and
retry the payment later.
"];
                    Mail::to($user -> email)->send(new generalMail($data));
                }

                return response()->json([
                    "response"=>[
                        "success"=>false,
                        "message"=>"Transaction failed!"
                    ]
                ],200);
            }
        }else{
            abort(404);
        }

    }
}
