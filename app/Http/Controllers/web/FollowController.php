<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Jobs\SendGeneralEmailJob;

class FollowController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth','verified'])->only(['follow']);
    }


    public function follow(Request $request){
        $target_id = $request->get("user_id");
        $user = User::find(Auth::user()->id);
        $user->users()->attach($target_id);

        $send_to_user = User::find($target_id, ['email', 'name', 'activity_notification']);
        if ($send_to_user->activity_notification == 1) {
            $data = [
                'title' => 'New Follower',
                'username' => $send_to_user->name,
                'messageBody' => 'You have a new follower, Good job!'
            ];
            dispatch(new SendGeneralEmailJob($send_to_user->email, $data));
        }

        return Response()->json([
            'response'=>[
                'msg'=>'Following Success'
            ]
        ],200);

    }

    public function unfollow(Request $request){
        $target_id = $request->get("user_id");
        $user = User::find(Auth::user()->id);
        $user->users()->detach($target_id);
        return Response()->json([
            'response'=>[
                'msg'=>'UnFollow Success'
            ]
        ],200);
    }

}
