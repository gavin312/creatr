<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Library\Helper;
use App\Models\Profile;
use App\Notifications\WelcomeEmailNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use PHPUnit\TextUI\Help;
use App\Models\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToGoogle(){
        return Socialite::driver('google')->redirect();
    }


//    TODO: change the google, facebook, credentials
    public function handleGoogleCallback(){
       return $this->registerOrLogin(Socialite::driver('google')->user(),'google');
    }

    public function redirectToFacebook(){
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback(){
        return $this->registerOrLogin(Socialite::driver('facebook')->user(),'facebook');
    }

    public function redirectToSnapChat(){
        return Socialite::driver('snapchat')->redirect();
    }

    public function handleSnapChatCallback(){
        return $this->registerOrLogin(Socialite::driver('snapchat')->user(),'snapchat');
    }


//    TODO: remember the id and login the used based on the id, if the email is the sama identify it as the same user, and auto fill other information
    public function registerOrLogin($data ,$loginMethod){
        $search_attribute = $loginMethod.'_id';
        $user = User::where($search_attribute, $data->id)->first();
        if(!$user){
            if(empty($data->email)){
                $user = new User();
                $user->name = Helper::clean($data->name);
                $user->email = $data->email;
                if( empty( $data->avatar) ){
                    $user->avatar = "users/default.png";
                }else{
                    $user->avatar = Helper::downloadImage($data->avatar);
                }
                $user->password = "";
                $user->role_id = 2;
                $user[$search_attribute] = $data->id;
                $user->save();
            }else{
                $user = User::where('email', $data->email)->first();
                if(!$user){
                    $user = new User();
                    $user->name = Helper::clean($data->name);
                    $user->email = $data->email;
                    if( empty( $data->avatar) ){
                        $user->avatar = "users/default.png";
                    }else{
                        $user->avatar = Helper::downloadImage($data->avatar);
                    }
                    $user->password = "";
                    $user->role_id = 2;
                    $user[$search_attribute] = $data->id;
                    $user->save();
                }else{
                    $user[$search_attribute] = $data->id;
                    $user->save();
                }
            }
            $profile = Profile::create([
                'user_id' => $user->id,
                'user_name'=> $user->name,
            ]);
            $profile->save();


            if($user->email != null && $user->email != ""){
                $temp = User::find($user->id);
                $temp->notify(new WelcomeEmailNotification());
            }
        }

        $user->markEmailAsVerified();

        Auth::login($user);

        return redirect()->route("home");
    }

}
