<?php

namespace App\Http\Controllers\Voyager;

use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\Models\ReportingReel;
use App\Models\Reel;
use App\Models\Quality;
use Vimeo\Laravel\Facades\Vimeo;
use App\Scopes\BannedReelScope;
use Illuminate\Support\Facades\Mail;
use App\Mail\GeneralMail;

class ReportingReelController extends VoyagerBaseController
{
  public function review()
  {
    $reporting = ReportingReel::where('id', \request("id"))->first();
    $reel_id = $reporting->reel_id;
    $reel = Reel::withoutGlobalScope(BannedReelScope::class)->find($reel_id);
    $reel_url = $reel->url_handle;

    return redirect(route("showReel", ["reel_url_handle" => $reel_url]));
  }

  public function solve()
  {
    //Get reporting by id and toggle the is_solved from SOLVED to PENDING and vice versa
    $reporting = ReportingReel::where('id', \request("id"))->first();
    if ($reporting->is_solved != 1) {
      $reporting->is_solved =  1;
      $reporting->save();
      return redirect(route('voyager.reporting-reels.index'))->with(['message' => "Success.", 'alert-type' => 'success']);
    } else {
      return redirect(route('voyager.reporting-reels.index'))->with(['message' => "The report is solved already.", 'alert-type' => 'info']);
    }
  }

  public function remove()
  {
    // 取出 reporting 记录
    $reporting = ReportingReel::where('id', \request("id"))->first();
    $reel_id = $reporting->reel_id;

    // 将被举报的reel 删除:
    // Retrieve the reel
    $reel = Reel::withoutGlobalScope(BannedReelScope::class)->find($reel_id);

    // 如果当前 reporting 已被标记为solved，点击removed按钮将不会执行任何操作。
    if ($reporting->is_solved == 1) {
      // TODO: For now, it redirects user. Maybe changing the button to disabled button will be better.
      return redirect(route('voyager.reporting-reels.index'))->with(['message' => "The report is solved already.", 'alert-type' => 'info']);
      
      // If the reel is already banned.
    } else if ($reel->is_banned == 1) {
      return redirect(route('voyager.reporting-reels.index'))->with(['message' => "The reel is banned already.", 'alert-type' => 'info']);

      // 如果当前 reporting 未被标记为solved，且is_banned 不为true(1)
    } else if ($reel->is_banned != 1) {
      // send delete request to Vimeo
      $response = Vimeo::request('/videos/' . $reel->video_id, [], 'DELETE');

      // Deleted successfully on Vimeo
      if ($response['status'] == 204) {
        // Set is_banned to true.
        $reel->is_banned = 1;
        $reel->save();

        // Delete the reel's qualities
        Quality::where('reel_id', $reel->id)->delete();

        // 同一reel 的所有举报记录标记为 solved
        $reporting_reels = ReportingReel::where('reel_id', $reel_id)->get();

        foreach ($reporting_reels as $reporting_reel) {
          if ($reporting_reel->is_solved != 1) {
            $reporting_reel->is_solved = 1;
            $reporting_reel->save();
          }
        }

        $reel->user;

        $data = [
          "title" => "Your reel violated our policy.",
          "username" => $reel->user->name,
          "messageBody" => "Sorry, your reel is violating our policy and we have confirmed. Therefore, 
          we have removed the reel from the website."
        ];

        // Mailing: send reported reel to the reel quthor
        Mail::to($reel->user->email)->send(new generalMail($data));
      }
      return redirect(route('voyager.reporting-reels.index'))->with(['message' => "Success.", 'alert-type' => 'success']);
    }
  }
}
