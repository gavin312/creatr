<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Library\ResponseMessage;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    function checkNameAvailable(Request $request){
        $userName = trim(request()->registerName);

        if(empty($userName)){
            return ResponseMessage::failMessage('UserName is Empty',[]);
        }
        $userArray = DB::table("users")->where('name',$userName)->get()->toArray();

        if(empty($userArray)){
            return ResponseMessage::successMessageWithCustomData('UserName is OK','status',true);
        }else{
            return ResponseMessage::failMessage('UserName is occupied',[]);
        }

    }

    function checkNewNameAvailable(Request $request){

        $user = User::find(request()->userid);

        $userName = trim(request()->newName);


        if(empty($userName)){
            return ResponseMessage::failMessage('UserName is Empty',[]);
        }

        if( strtolower( $user->name)  === strtolower($userName)){
            return ResponseMessage::successMessageWithCustomData('UserName is OK','status',true);
        }else{
            $userArray = User::where('name',$userName)->get()->toArray();

            if(empty($userArray)){
                return ResponseMessage::successMessageWithCustomData('UserName is OK','status',true);
            }else{
                return ResponseMessage::failMessage('UserName is occupied',[]);
            }
        }

    }

}
