<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayoutTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payout_transactions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("order_id")->index();
            $table->bigInteger('order_item_id')->index();
            $table->bigInteger("user_id")->index();
            $table->bigInteger("reel_id")->index();
            $table->bigInteger("licence_id")->index();
            $table->bigInteger("total_amount")->default(0)->comment("cent");
            $table->bigInteger("platform_amount")->default(0)->comment("cent");
            $table->bigInteger("stripe_amount")->default(0)->comment("cent");
            $table->bigInteger("payout_amount")->default(0)->comment("cent");
            $table->string("transaction_id")->default("")->nullable();
            $table->tinyInteger("state")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payout_transactions');
    }
}
