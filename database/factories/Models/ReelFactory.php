<?php

namespace Database\Factories;

use App\Models\Reel;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReelFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
