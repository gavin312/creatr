@extends('layouts.app')

@section('content')
<div class="container" style="padding-top: 200px; padding-bottom: 140px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card secondary-black-background p-3">
                <div class="card-header mega-solution-h3">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body mega-solution-body-light">
                    @if (session('resent'))
                        <div class="alert mega-solution-cap-light success-back-ground mb-4" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline mega-solution-body-heavy primary-font-color">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
