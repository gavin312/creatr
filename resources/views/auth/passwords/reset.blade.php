@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 pt-10 pb-5">
            <div id="app">
                <reset-password token="{{$token}}" email="{{$email}}"></reset-password>
            </div>
        </div>
    </div>
</div>
@endsection
