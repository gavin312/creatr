<nav class="navbar navbar-expand-md navbar-dark navbar-light dark-theme shadow-sm mega-solution-header">
    <div class="container-fluid px-5 computer-nav-bar">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img class="nav-bar-logo" src="{{Voyager::image(setting('site.navigationbar_logo'))}}">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>

        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav">

                <li class="nav-item  mega-solution-horizontal-center">
                    <a class="nav-link text-white mega-solution-p2-light"  href="{{route('categoryList')}}">{{__('Explore')}}</a>
                </li>

                <li class="nav-item   mega-solution-horizontal-center">
                    <a class="nav-link text-white  mega-solution-p2-light" target="_blank" href="https://blog.creatrhq.com/">{{__('Blog')}}</a>
                </li>

{{--                <li class="nav-item   mega-solution-horizontal-center">--}}
{{--                    <a class="nav-link text-white" target="_blank" href="https://studio.creatrhq.com/">{{__('Studio')}}</a>--}}
{{--                </li>--}}

            </ul>

            <ul class="ml-auto navbar-nav mr-auto navbar-search ">
                <li class="nav-item mega-solution-horizontal-start">
                        <search-container></search-container>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item mega-solution-horizontal-center ">
                        <div id="app">
                            <login-component></login-component>
                        </div>
                    </li>
                    <li class="nav-item mega-solution-horizontal-center ">
                        <div id="app">
                            <register-component></register-component>
                        </div>
                    </li>
                @else
                    <li class="nav-item mr-4">
                        <cart-component></cart-component>
                    </li>

{{--                    <li class="nav-item mr-4">--}}
{{--                        <notification-component></notification-component>--}}
{{--                    </li>--}}

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle creatr-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            <img src="{{Voyager::image( Auth::user()->avatar)}}" alt="User Avatar" class="creatr-user-avatar">
                        </a>

                        <div class="dropdown-menu dropdown-menu-right creatr-dropdown" aria-labelledby="navbarDropdown">
                            <profile-menu :user-info = "{{Auth::user()}}"></profile-menu>
                        </div>
                    </li>
                    <li class="nav-item mega-solution-horizontal-center ">
                        <a href="{{route('createReel')}}" class="primary-button upload-btn  mega-solution-body-heavy mega-solution-horizontal-center">Upload</a>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
    <div class="container-fluid px-1 mobile-nav-bar">
        <div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon mobile-toggle-icon"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}">
                <img class="nav-bar-logo" src="{{Voyager::image(setting('site.navigationbar_logo'))}}">
            </a>

        </div>

        <div class="mega-solution-horizontal-center">
            <ul class="ml-auto navbar-nav mr-auto navbar-search ">
                <li class="nav-item mega-solution-horizontal-start">
                    <search-container></search-container>
                </li>
            </ul>



            @guest
                <li class="nav-item mega-solution-horizontal-center ">
                    <div id="app">
                        <register-component></register-component>
                    </div>
                </li>
            @else
                <ul class="ml-auto navbar-nav mr-auto navbar-search pr-4">
                    <li class="nav-item mega-solution-horizontal-start">
                        <cart-component></cart-component>
                    </li>
                </ul>

                <li class="nav-item mega-solution-horizontal-center ">
                    <a href="{{route('createReel')}}" class="primary-button upload-btn  mega-solution-body-heavy mega-solution-horizontal-center">Upload</a>
                </li>
            @endguest
        </div>
        <div class="collapse navbar-collapse nav-info-full-screen" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->


                <div class="nav-item  mega-solution-horizontal-start">
                    <a class="nav-link text-white  mega-solution-p2-light" href="{{route('categoryList')}}">{{__('Explore')}}</a>
                </div>

                <div class="nav-item   mega-solution-horizontal-start">
                    <a class="nav-link text-white  mega-solution-p2-light" target="_blank" href="https://blog.creatrhq.com/">{{__('Blog')}}</a>
                </div>

{{--                <div class="nav-item   mega-solution-horizontal-start">--}}
{{--                    <a class="nav-link text-white" target="_blank" href="https://studio.creatrhq.com/">{{__('Studio')}}</a>--}}
{{--                </div>--}}



            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav">
                <!-- Authentication Links -->


                @guest

                @else

                    <profile-mobile-menu :user-info = "{{Auth::user()}}"></profile-mobile-menu>


                @endguest
            </ul>
        </div>
    </div>
</nav>
