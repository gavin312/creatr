<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="background-primary">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Cookiebot -->
{{--    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="1f82d76f-7352-4e03-abd4-afaf31d4205e" data-blockingmode="auto" type="text/javascript"></script>--}}
{{--    <script id="CookieDeclaration" src="https://consent.cookiebot.com/1f82d76f-7352-4e03-abd4-afaf31d4205e/cd.js" type="text/javascript" async></script>--}}

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYt9HVTLJPHY3y32Rh47WMCG1NJTxP0U0&libraries=places"></script>

    <!-- Google Tag Manager -->
    <script >(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KVJZGTH');</script>
    <!-- End Google Tag Manager -->

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" href="{{Voyager::image(setting('site.logo')) }}">
    <link rel="icon" type="image/png" href="{{ Voyager::image(setting('site.logo')) }}">
    <!-- Scripts -->
    <script data-cookieconsent="ignore" src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KVJZGTH"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="app">
        @include('layouts.navbar')
        <main class="pb-4 background-primary">
            @yield('content')

        </main>

        <creatr-footer  site-logo="{{Voyager::image(setting('site.logo'))}}"
                        tiktok-link="{{setting('site.tiktok_link')}}"
                        instagram-link="{{setting('site.instagram_link')}}"
                        site-description="{{setting('site.description')}}" ></creatr-footer>
    </div>
</body>
</html>
