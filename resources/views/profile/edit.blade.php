@extends('layouts.app')

@section('content')

        <div id="app">
            <edit-profile :user-info="{{$user}}" :profile ="{{$profile}}"></edit-profile>
        </div>
@endsection
