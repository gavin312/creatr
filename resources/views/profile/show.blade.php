@extends('layouts.app')

@section('content')

    <div id="app">
        <display-profile :user_info="{{$user_info}}"></display-profile>
    </div>
@endsection
