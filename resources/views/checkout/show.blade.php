@extends('layouts.checkout')

@section('content')
    <div id="app">

        @if($cartItems != null)
            <checkout-view :view-cart-items="{{$cartItems}}" :user="{{$user}}" client-secret="{{$paymentIntent->client_secret}}" :payment-methods="{{$paymentMethods}}" :default-payment-method="{{json_encode($defaultMethod)}}"></checkout-view>
        @else
            <checkout-view  :user="{{$user}}" client-secret="{{$paymentIntent->client_secret}}" :payment-methods="{{$paymentMethods}}" :default-payment-method="{{json_encode($defaultMethod)}}"></checkout-view>
        @endif
    </div>
@endsection
