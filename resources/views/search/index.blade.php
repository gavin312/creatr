@extends('layouts.app')

@section('content')
<div id="app">
    <search-page :search-basics="{{$searchBasics}}" :tags="{{$tags}}" :categories="{{$categories}}" :rates="{{$rates}}" :platforms="{{$platforms}}">
    </search-page>
</div>
@endsection