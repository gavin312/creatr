@extends('errors::customise')

@section('title', __('Forbidden'))
@section('code', '403')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message',  __($exception->getMessage() ?: 'Forbidden'))
@section('description', __('Try searching for something else.'))

