@extends('errors::customise')

@section('title', __('Not Found'))
@section('code', '404')

@section('image')
    <img src="/images/404.png" alt="404">
@endsection

@section('message', __('Oh no, this page isn’t available. '))
@section('description', __('Sorry but the page you were looking for couldn’t be found. Try searching for something else.'))
