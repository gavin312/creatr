@extends('errors::customise')

@section('title', __('Page Expired'))
@section('code', '419')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message',  __('Page Expired'))
@section('description', __('Try searching for something else.'))
