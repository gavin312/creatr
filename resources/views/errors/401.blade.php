@extends('errors::customise')

@section('title', __('Unauthorized'))
@section('code', '401')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message', __('Oh no, you are not authorised on this page. '))
@section('description', __(' Try searching for something else.'))

