<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{Voyager::image(setting('site.logo')) }}">
    <link rel="icon" type="image/png" href="{{ Voyager::image(setting('site.logo')) }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .main-wrapper {
            height: 100vh;
            padding-top: 40px;
        }
        .logo-container{
            width: 64px;
            height: 64px;
            margin-left: 60px;

        }
        .logo-container img{
            object-fit: cover;
        }
        .container .image-container{
            width: 418px;
            height: 292px;
        }
        .container .image-container img{
            width: 100%;
            height: 100%;
            object-fit: contain;
        }
        .text-gray{
            color: rgba(250, 250, 250, 0.7);
        }

        .text-disabled{
            color:rgba(242, 242, 242, 0.48)
        }

        .mt-48{
            margin-top: 48px;
        }

        .mt-64{
            margin-top: 64px;
        }
        .search-component{
            padding-bottom: 12px;
            border-bottom: 2px solid #fafafa;
        }
        .search-component .search-image{
            width: 24px;
            height: 24px;
            margin-right: 16px;
        }
        .without-underline:hover{
            text-decoration: none !important;
        }
    </style>
</head>
<body>

    <main class="pb-4 background-primary main-wrapper">
        <div class="container-fluid">
            <a href="/">
                <div class="logo-container pointer">
                    <img src="{{"/storage/".setting('site.logo')}}" alt="">
                </div>
            </a>

            <div class="container">
                <div class="row mt-64 ">
                    <div class="col-md-6 mega-solution-horizontal-center">
                        <div class="image-container">
                                @yield('image')
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="mega-solution-h3 mt-5">@yield('message')</h3>
                        <div class="mega-solution-p2-light mt-3 custom-description text-gray">@yield('description')</div>
                        <a href="/search" class="without-underline">
                            <div class="mt-48 search-component mega-solution-horizontal-start">
                                <img src="/images/search.png" alt="search" class="search-image">
                                <div class="search-component-text mega-solution-h3 text-disabled">Search Creatr</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
</body>
</html>
