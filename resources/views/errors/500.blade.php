@extends('errors::customise')

@section('title', __('Server Error'))
@section('code', '500')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message',  __('Server Error'))
@section('description', __('Server Error! We will work this out ASAP!'))
