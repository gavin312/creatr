@extends('errors::customise')

@section('title', __('Too Many Requests'))
@section('code', '429')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message',  __('Too Many Requests'))
@section('description', __('Please wait for a minute and try later!'))
