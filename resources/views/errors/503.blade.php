@extends('errors::customise')

@section('title', __('Service Unavailable'))
@section('code', '503')

@section('image')
    <img src="/images/404a.png" alt="404">
@endsection

@section('message',  __('Service Unavailable'))
@section('description', __('Service Unavailable! Please try again later'))
