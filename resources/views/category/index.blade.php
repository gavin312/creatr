@extends('layouts.app')

@section('content')
    <div id="app">
        <categories-list :categories = "{{$categories}}" :top-categories = "{{$topCategories}}" ></categories-list>

{{--        <display-tag :tag="{{$tag}}" :most-popular-tags="{{$mostPopularTags}}" ></display-tag>--}}

    </div>
@endsection
