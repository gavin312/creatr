@extends('layouts.app')

@section('content')
    <div id="app">

{{--        <div class="container-fluid pt-10">--}}
{{--            {{ Breadcrumbs::render('collections') }}--}}
{{--        </div>--}}
{{--        <h1 class="mega-solution-h1">Collections</h1>--}}
        <collection-list :most-popular-reel="{{$mostPopularReel}}" :collection-basic-info = "{{$collectionsInfo}}"></collection-list>
    </div>
@endsection
