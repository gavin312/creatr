@extends('layouts.app')

@section('content')

    <div id="app">
        <div class="container pt-10">
            {{ Breadcrumbs::render('collection', $collection) }}
        </div>
        <display-collection
            :collection-info="{{$collection_info}}"
            :tags="{{$tags}}"
            :creator-info="{{$creator_info}}"
            :category="{{$category}}"
            :comments = "{{$formatComment}}"
            :rate="{{$rate}}"
        ></display-collection>
    </div>
@endsection
