@extends('layouts.app')

@section('content')
    <div id="app">

        <edit-collection :collection-info="{{$collection_info}}"></edit-collection>

    </div>
@endsection
