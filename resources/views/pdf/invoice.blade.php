@extends('layouts.invoice')

@section('content')
    <div id="app">
        <invoice-view :invoice-info="{{ $userInvoice}}" :company-info ="{{json_encode($companyInfo)}}" mode="Print"></invoice-view>
    </div>
@endsection
