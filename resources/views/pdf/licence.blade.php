<html>
<head>
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <style>
        /**
            Set the margins of the page to 0, so the footer and the header
            can be of the full height and width !
         **/


        .page-break {
            page-break-after: always;
        }
        @page {
            margin: 0cm 0cm;
        }

        /** Define now the real margins of every page in the PDF **/
        body {
            /*margin-top: 2cm;*/
            /*margin-left: 2cm;*/
            /*margin-right: 2cm;*/
            /*margin-bottom: 2cm;*/
            margin-top: 104px;
            margin-bottom: 72px;
        }

        /** Define the header rules **/
        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 96px;

            /** Extra personal styles **/
            background-color: #1622AB;
            color: white;
            text-align: center;
            line-height: 1.5cm;
            padding-left: 64px;
            padding-right: 64px;

            /*display: flex;*/
            /*align-items: center;*/
            /*justify-content: space-between;*/
        }

        header .logo{
            width: 90px;
            height: 40px;
            margin-right: auto;
            margin-top: 32px;
            /*position: absolute;*/
        }

        header .title{
            /*position: absolute;*/
            width: 200px;
            height: 40px;
            margin-left: auto;

            /*right: 0px;*/
            margin-top: 40px;
        }
        /** Define the footer rules **/
        footer {
            position: fixed;
            bottom: 0cm;
            left: 0cm;
            right: 0cm;
            height: 52px;

            /** Extra personal styles **/

            color: white;
            text-align: center;
            padding-top: 24px;
            padding-left: 64px;
            padding-right: 64px;
            background-color: #f2f2f2;
        }

        footer .footer-title{
            position: absolute;
            margin-right: auto;
            height: 24px;
        }

        footer .footer-address{
            position: absolute;
            margin-right: auto;
            margin-left: 150px;
            text-align: left;
        }

        .mega-solution-sub-heavy {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: bold;
            font-size: 20px;
            line-height: 24px;
            color: white;
        }

        .mega-solution-cap {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 18px;
            color: #2D2D2D;
        }

        .color-black{
            color: #2D2D2D;
        }

        .mega-solution-cap-heavy {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: bold;
            font-size: 14px;
            line-height: 18px;
            color: #2D2D2D;
        }

        .mega-solution-cap-light {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: normal;
            font-size: 14px;
            line-height: 18px;
            color: #2D2D2D;
        }




    /*    Page 1*/

        .inner-page{
            width: 100%;
            padding-left: 64px;
            padding-right: 64px;
        }

        .page1-asset-title{
            margin-top: 32px;
        }

        .page1-asset-image-container{
            margin-top: 16px;
            height: 320px;
            background-color: #c4c4c4;
        }

        .page1-asset-image-container-img{
            height: 100%;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
            object-fit: cover;
        }
        .inner-page-table{
            width: 100%;
            background-color: #F2f2f2;
        }

        .inner-page-table .table-row{
            /*border-bottom: 1px solid #ECECEC;*/
        }

        .inner-page-table .table-row td{
            padding-top: 12px;
            padding-bottom: 12px;
            border-bottom: 1px solid #ECECEC;
        }

        .inner-page-table .table-row .td-left{
            width: 30%;
            padding-left: 16px;
        }
        .inner-page-table .table-row .td-right{
            width: 70%;
        }
        .page1-asset-title2{
            margin-top: 48px;
        }

        .mt-16{
            margin-top: 16px;
        }


        .mega-solution-h4 {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: bold;
            font-size: 24px;
            line-height: 28px;
            color: #2D2D2D;
        }

        .mega-solution-p2-heavy {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: bold;
            font-size: 16px;
            line-height: 24px;
            color: #2D2D2D;
        }

        .mega-solution-p2-light {
            font-family: "CerebriSansPro", sans-serif;
            font-style: normal;
            font-weight: normal;
            font-size: 16px;
            line-height: 24px;
            color: #2D2D2D;

        }
        .mt-48{
            margin-top: 32px;
        }
    </style>
</head>
<body>
<!-- Define header and footer blocks before your content -->

<header>
    <img src="{{$logo}}" alt="Nav Logo"  class="logo">
    <div class="title mega-solution-sub-heavy">
        License Certificate
    </div>
</header>

<footer>
    <div class="footer-title mega-solution-sub-heavy color-black">
        {{$companyInfo->entityName}}
    </div>
    <div class="footer-address mega-solution-cap-light">
        {{$companyInfo->companyAddressLine1}} {{$companyInfo->companyAddressLine2}} {{$companyInfo->companyCity}} {{$companyInfo->companyRegion}} {{$companyInfo->companyPostalCode}}
    </div>
</footer>

<!-- Wrap the content of your PDF inside a main tag -->
<main>
    {{--   page 1 --}}

    <div class="inner-page">
        <div class="page1-asset-title mega-solution-cap">ASSET DETAILS</div>
        <div class="page1-asset-image-container">
            <img src="{{$reelThumb}}" alt="image" class="page1-asset-image-container-img">
        </div>
        <table class="inner-page-table">
            <thead>
            </thead>
            <tbody>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Licensor Name</td>
                <td class="td-right mega-solution-cap-light">{{$reel->user->profile->user_name}}({{$reel->user->name}})</td>
            </tr>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Item title</td>
                <td class="td-right mega-solution-cap-light">{{$reel->title}}</td>
            </tr>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Item ID</td>
                <td class="td-right mega-solution-cap-light">{{$reel->id}}</td>
            </tr>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Item URL</td>
                <td class="td-right mega-solution-cap-light">my.creatrhq.com/reels/{{$reel->url_handle}}</td>
            </tr>
            </tbody>
        </table>

        <div class="page1-asset-title2 mega-solution-cap">PURCHASE DETAILS</div>
        <table class="inner-page-table mt-16">
            <thead>
            </thead>
            <tbody>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">License Type</td>
                <td class="td-right mega-solution-cap-light">{{$licence->type}}</td>
            </tr>
            @if($order->payment_method_name !== null)
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Licensee Name</td>
                <td class="td-right mega-solution-cap-light">{{$order->payment_method_name}}</td>
            </tr>
            @endif
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Platform username</td>
                <td class="td-right mega-solution-cap-light">{{$order->user->profile->user_name}}({{$order->user->name}})</td>
            </tr>

            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Order Number</td>
                <td class="td-right mega-solution-cap-light">{{$order->order_number}}</td>
            </tr>
            <tr class="table-row">
                <td class="td-left mega-solution-cap-heavy">Order Date</td>
                <td class="td-right mega-solution-cap-light">{{$order->created_at}}</td>
            </tr>
            </tbody>
        </table>
    </div>


    <div class="page-break"></div>



    {{--  page 2  --}}
    <div class="inner-page">
        <div class="mega-solution-h4 mt-48">Licensing</div>
        <div class="mega-solution-p2-light mt-16">Creatr hereby grants you (“User/Purchaser”) a non-exclusive, non-transferable right to use, modify (except as expressly prohibited herein) and reproduce Visual Content worldwide, in perpetuity, as expressly permitted by the following types of applicable licenses and subject to the limitations set forth in Creatr’s Terms and Conditions, and as set forth herein:</div>

        <div class="mega-solution-h4 mt-48">a. Private Usage</div>
        <div class="mega-solution-p2-heavy mt-16">Creatr grants the User/Purchaser a non-exclusive, perpetual, personal use license to view, download, display, and copy the content, subject to the following restrictions:</div>
        <ul class="mega-solution-p2-light mt-16">
            <li>§ The content is licensed for personal use only, not commercial use.</li>
            <li>§ The content may not be used in any way whatsoever in which you charge money, collect fees, or receive any form of remuneration.</li>
            <li>§ The content may not be resold, relicensed, sub-licensed, rented, leased, or used in advertising.</li>
            <li>§ Title and ownership, and all rights now and in the future, of and for the content remain exclusively with the content owner.</li>
            <li>§ There are no warranties, express or implied.</li>
            <li>§ The content is provided ‘as is’.</li>
            <li>§ Neither Creatr, the payment processing service, nor hosting service will be liable for any third-party claims or incidental, consequential, or other damages arising out of this license or the purchaser’s use of the content.</li>
        </ul>

        <div class="mega-solution-h4 mt-48">b. Commercial Usage</div>
        <div class="mega-solution-p2-heavy mt-16">Visual Content can be used vide the Commercial Usage License under the following terms:</div>
        <ul class="mega-solution-p2-light mt-16">
            <li>§ The licensed content can appear in up to 5,000 end products for sale</li>
            <li>§ Can be used on one business social media account owned and managed by the purchaser</li>
            <li>§ Unlimited physical advertisements for local markets</li>
            <li>§ Digital paid advertisements with unlimited impressions</li>
            <li>§ Broadcast and streaming for up to 500,000 lifetime viewers</li>
        </ul>

    </div>

    <div class="page-break"></div>

    {{--  page 3  --}}
    <div class="inner-page">

        <div class="mega-solution-h4 mt-48">c. Editorial Usage</div>
        <div class="mega-solution-p2-heavy mt-16">An Editorial License grants you the right to make a single, editorial use of an item of Editorial content, which single use may be distributed worldwide, in perpetuity. </div>
        <ul class="mega-solution-p2-light mt-16">
            <li>A “single use” for the purposes of this license permits the use of Editorial content in a single context (i.e., a news story, blog post, page of a publication) a single time, provided you shall have the right to distribute that use in-context across unlimited mediums and distribution channels (but may not permit any third-parties to redistribute on any mediums and distribution channels). For example, the use of Editorial content to illustrate a printed article, may be reused on a blog, on social media, etc., provided it is in-context to the original printed article and such ancillary use is on purchaser’s owned and operated platforms. Any use not in-context to the original printed article would require an additional license.</li>
            <li>An “editorial use” for the purposes of this license shall be a use made for descriptive purposes in a context that is newsworthy or of human interest and expressly excludes commercial uses such as advertising or merchandising of a product and/or service.</li>
            <li>All Editorial content shall be deemed “Editorial Use Only” for the purposes of this TOS.</li>
            <li>Not all Visual Content that is listed as Editorial content is available for license from the Creatr site, nor may it be available from all subscriptions. You understand that the Editorial content available for the license can change at any time and you shall have no right to demand to license any particular item of Editorial content. If you want to license any Editorial content not available for license from the website, please contact [Details]</li>
        </ul>
        <div class="mega-solution-h4 mt-48">d. Public Domain Dedication (Open to public use)</div>
        <ul class="mega-solution-p2-light mt-16">
            The Visual Content in question has been dedicated to the public domain by the individual who created the content and in doing so, the individual has waived all rights to the content under worldwide copyright law, including all related and neighboring rights allowed for to the extent of the law. Therefore, you may copy, modify, distribute and perform the Visual Content for any purpose, such as commercial purposes, without the need for prior permission to be obtained.
        </ul>
    </div>

</main>



</body>
</html>
