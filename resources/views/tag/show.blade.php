@extends('layouts.app')

@section('content')
    <div id="app">
        <display-tag :tag="{{$tag}}" :most-popular-tags="{{$mostPopularTags}}">

        </display-tag>
    </div>
@endsection
