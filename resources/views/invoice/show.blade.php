@extends('layouts.invoice')

@section('content')
    <div id="app">
        <invoice-view :invoice-info="{{ $userInvoice}}" :company-info ="{{json_encode($companyInfo)}}" mode="View"></invoice-view>
    </div>
@endsection
