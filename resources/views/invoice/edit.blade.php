@extends('layouts.invoice')

@section('content')
    <div id="app">
        <invoice-view :invoice-info="{{ $userInvoice}}" :company-info ="{{json_encode($companyInfo)}}" mode="Edit"></invoice-view>
    </div>
@endsection
