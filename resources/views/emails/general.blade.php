<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$emailContent['title']}}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <style>
        .mega-solution-email-font {
            font-family: "CerebriSansPro", sans-serif;
        }
    </style>
</head>

<body style="background-color:#fafafa; font-weight:400;">
    <div class="mega-solution-email-font" style="width: 640px; margin:auto;background-color:#ffffff;">
        <div style="padding:32px; background-color:#141414; text-align:center; color:white;">
            <img src="{{asset('images/logo.png')}}" height="64" width="64" alt="logo" />
            <h1 style="font-size: 24px; line-height:32px; font-weight: 700;">{{$emailContent['title']}}</h1>
        </div>
        <div style="padding:0 32px;">
            <table style="width: 100%;
                text-align:left;
                margin:auto;
                border-collapse:separate;
                border-spacing: 0 28px;
                font-size: 16px;
                line-height: 24px;
                color: #4f4f4f;
            ">
                <thead>
                    <tr>
                        <th colspan="3">Hi {{$emailContent['username']}},</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>
                            <pre class="mega-solution-email-font" style="margin:0;white-space:pre-wrap">{{$emailContent['messageBody']}}</pre>
                        </td>
                    </tr>
                    @if (array_key_exists('callToAction', $emailContent))
                    <tr>
                        <td style="text-align:center;">
                            <a href="{{$emailContent['callToAction']['link']}}" style="background-color: #EB1480;
                            padding: 8px 16px;
                            font-size: 16px;
                            color: white;
                            text-decoration:none;
                            border-radius: 4px;
                            ">
                                {{$emailContent['callToAction']['title']}}
                            </a>
                        </td>
                    </tr>
                    @endif
                    @if (array_key_exists('bottomContent', $emailContent))
                    <tr>
                        <td>
                            {{$emailContent['bottomContent']}}
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td>
                            Thank you, <br>
                            The Creatr Team
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="background-color: #f4f4f4; padding:16px 0; color: #4f4f4f; font-size:12px; line-height:16px; font-weight:400;">
            <table style="border-collapse:collapse; margin: auto;">
                <tbody>
                    <tr>
                        <td style="padding: 0 16px;">
                            <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f">Terms of Service</a>
                        </td>
                        <td style="padding: 0 16px; border-left: 1px solid #999999; border-right: 1px solid #999999;">
                            <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f">
                                Privacy Policy
                            </a>
                        </td>
                        <td style="padding: 0 16px;">
                            <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f">Contact us</a>
                        </td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td colspan="3" style="text-align: center;margin-top: 8px">
                            @Creatr {{date("Y")}}. All Right Reserved.
                        </td>
                    </tr>
                </tbody>

            </table>

        </div>
    </div>
</body>

</html>
