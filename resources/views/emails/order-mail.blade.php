<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Order Confirmation</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">

  <!-- Styles -->
  <style>
    .mega-solution-email-font {
      font-family: "CerebriSansPro", sans-serif;
    }
    .mega-solution-p1-heavy {
      font-family: "CerebriSansPro", sans-serif;
      font-style: normal;
      font-weight: 600;
      font-size: 20px;
      line-height: 32px;
      letter-spacing: -0.15%;
      color: white;
    }
    .mega-solution-h4 {
      font-family: "CerebriSansPro", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 24px;
      line-height: 32px;
      letter-spacing: -0.5%;
      color: white;
    }
    .mega-solution-p2-light {
      font-family: "CerebriSansPro", sans-serif;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      line-height: 24px;
      letter-spacing: -0.1%;
      color: white;
    }
    .mega-solution-p2-heavy {
      font-family: "CerebriSansPro", sans-serif;
      font-style: normal;
      font-weight: 600;
      font-size: 16px;
      line-height: 24px;
      letter-spacing: -0.15%;
      color: white;
    }
    .mega-solution-p3-light {
      font-family: "CerebriSansPro", sans-serif;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 20px;
      letter-spacing: -0.1%;
      color: white;
    }
    .line-clamp {
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      overflow: hidden;
      word-wrap: break-word;
    }
  </style>
</head>

<body style="background-color:#f4f4f4; font-weight:300;">
  <div style="width: 640px; margin:auto;background-color:#ffffff;" class="mega-solution-email-font">
    <div style="padding:28px 0 24px; background-color:#141414; text-align:center; color:white;">
      <img src="{{asset('images/logo.png')}}" height="48" width="48" style="margin-bottom: 18px;" />
      <h1 class="mega-solution-h4" style="margin:0;">Order confirmation</h1>
      <p class="mega-solution-p2-light" style="margin:8px 0 16px;">Thank you for your purchase, {{$order->user->name}}!
        <br>
        Your support means a lot for the creators.
      </p>
      <a href="https://my.creatrhq.com/order" style="display:inline-block; background-color: #EB1480; padding: 8px 16px; font-size: 16px; line-height:20px; font-weight: 600; color: white;text-decoration:none; border-radius:4px;">View my order</a>
    </div>
    <div style="padding:0 32px;">
      <table style="width: 100%; text-align:left; margin:auto; border-collapse:collapse;">
        <thead>
          <tr>
            <th colspan="3" style="padding-top: 32px;font-size:14px;font-weight:600;line-height:18px;letter-spacing:1px">ORDER DETAILS</th>
          </tr>
        </thead>

        <tbody>
          @foreach($order->orderItems as $orderItem)
          <tr style="border-bottom:1px dashed #ECECEC;">
            <td style="padding: 24px 0; width:25%;">
              <img src="{{asset('storage/'.$orderItem->reel->thumbnail)}}" width="126" height="96" />
            </td>
            <td style="padding: 24px 0; vertical-align:top; height:1px;">
              <table style="height:100%; width:100%; table-layout:fixed;">
                <tr>
                  <td class="mega-solution-p2-heavy line-clamp" style="color:#333333; ">{{$orderItem->reel->title}}</td>
                </tr>
                <tr>
                  <td class="mega-solution-p3-light" style="color:#333333">ID: {{$orderItem->id}}</td>
                </tr>
                <tr>
                  <td class="mega-solution-p3-light" style="color:#333333">{{$orderItem->licence->type}} x {{$orderItem->quantity}}</td>
                </tr>
              </table>
            </td>
            <td style="padding: 24px 0;text-align:right; vertical-align:top; width: 10%;">
              <div style="padding: 4px 0; line-height: 24px;">
              @php
              $itemTotal = $orderItem->price * $orderItem->quantity;
              $itemTotalWithDecimal = number_format((float)$itemTotal, 2, '.','');
              @endphp
              ${{$itemTotalWithDecimal}}
              </div>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>

      <table style="width: 100%; text-align:left; margin:auto; border-collapse:collapse;">
        <tbody>
          <tr class="mega-solution-p3-light" style="color:#333333;">
            <td colspan="2" style="padding: 32px 0 8px;">Subtotal</td>
            <td style="padding: 32px 0 8px;text-align:right;">${{number_format((float)$order->amount, 2, '.','')}}</td>
          </tr>
          <tr class="mega-solution-p1-heavy" style="color:#333333;">
            <td colspan="2">Total</td>
            <td style="padding: 4px 0;text-align:right;"><strong>${{number_format((float)$order->amount, 2, '.','')}}</strong></td>
          </tr>

          <tr style="border-bottom:1px solid #ECECEC ">
            <td colspan="100%" style="padding-top:32px;">
            </td>
          </tr>

          <tr>
            <td colspan="3" style="padding: 32px 0 16px 0;font-size:14px;font-weight:600;line-height:18px;letter-spacing:1px;">ORDER SUMMARY</td>
          </tr>
          <tr>
            <td colspan="2">Order number:</td>
            <td style="padding: 4px 0;text-align:right;">{{$order->order_number}}</td>
          </tr>
          <tr>
            <td colspan="2">Order date:</td>
            <td style="padding: 4px 0;text-align:right;">{{$order->order_placed_date}}</td>
          </tr>
          <tr>
            <td colspan="2">Payment method:</td>
            <td style="padding: 4px 0;text-align:right;">Credit Card</td>
          </tr>
          <tr>
            <td colspan="2">Email:</td>
            <td style="padding: 4px 0;text-align:right;">{{$order->user->email}}</td>
          </tr>

          <tr style="border-bottom:1px solid #ECECEC ">
            <td colspan="100%" style="padding-top:32px;">
            </td>
          </tr>

          <tr>
            <td style="padding:24px 0;" colspan="3">
              <div style="padding: 8px; border-radius:8px; background-color: #F6f7fe;">
                <img src="{{asset('images/invoice_icon.png')}}" height="16" width="16" style="margin-right: 4px; vertical-align:middle;" /><span style="vertical-align:middle;">Need an invoice? <a style="color:#1622AB;text-decoration:none;" href="{{'https://my.creatrhq.com/order/thankyou/'.encrypt($order->order_number)}}">Get it from here</a></span>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div style="background-color: #fafafa; padding:16px 0; color: #4f4f4f; font-size:12px; line-height:16px; font-weight:400;">
        <table style="border-collapse:collapse; margin: auto;">
            <tbody>
                <tr>
                    <td style="padding: 0 16px;">
                        <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f; text-decoration:none;">Terms of Service</a>
                    </td>
                    <td style="padding: 0 16px; border-left: 1px solid #999999; border-right: 1px solid #999999;">
                        <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f; text-decoration:none;">
                            Privacy Policy
                        </a>
                    </td>
                    <td style="padding: 0 16px;">
                        <a href="https://creatrhq.com/terms-of-service/" style="color: #4f4f4f; text-decoration:none;">Contact us</a>
                    </td>
                </tr>
                <tr></tr>
                <tr>
                    <td colspan="3" style="text-align: center; padding-top: 8px">
                        @Creatr {{date("Y")}}. All Right Reserved.
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
  </div>
</body>

</html>
