<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Welcome to Creatr!</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">

  <style>
    .mega-solution-email-font {
      font-family: "CerebriSansPro", sans-serif;
    }
  </style>
</head>

<body style="background-color:#ffffff; font-weight:400;color:#ffffff;">
  <div class="mega-solution-email-font" style="padding:32px 72px 48px; width: 640px; margin:auto;background-color:#141414;font-weight:400;color:#ffffff;">
    <div style="text-align:center; color:white;">
      <img src="{{asset('images/logo.png')}}" height="64" width="64" alt="logo" />
      <h1 style="font-size: 32px; line-height:40px; font-weight: 700;">Welcome to Creatr!</h1>
      <p style="font-size: 16px; line-height: 24px;">Great to see you here, {{$username}}!</p>
      <div style="margin:40px 0; background-color:#9ff9f0; height:2px; width:100%;"></div>
    </div>
    <div>
      <table style="width: 100%; text-align:left; margin:auto; border-collapse:separate; border-spacing: 0 28px; font-size: 16px; line-height: 20px;">
        <tbody>
          <tr>
            <th style="font-size:20px; line-height:24px;">
              Get the most of Creatr!
            </th>
          </tr>
          <tr>
            <td>
              <img src="{{asset('images/movie_filter_black.png')}}" height="32" width="32" style="margin-right: 16px; vertical-align:middle;" /> <span style="vertical-align:middle;">Explore authentic video content</span>
            </td>
          </tr>
          <tr>
            <td>
              <img src="{{asset('images/cloud_download_black.png')}}" height="32" width="32" style="margin-right: 16px; vertical-align:middle;" /> <span style="vertical-align:middle;">Use them with worry-free licensing</span>
            </td>
          </tr>
          <tr>
            <td>
              <img src="{{asset('images/person_search_black.png')}}" height="32" width="32" style="margin-right: 16px; vertical-align:middle;" /> <span style="vertical-align:middle;">Discover and Work With the World’s Top Creators</span>
            </td>
          </tr>
          <tr>
            <td style="padding: 52px 0; text-align:center;">
              <a href="https://my.creatrhq.com/" style="background-color: #EB1480; padding: 8px 72px; font-size: 16px; color: white;text-decoration:none;">Explore</a>
            </td>
          </tr>

          <tr>
            <td>
              Creatr is a platform that content creators(Youtube, Tiktok, Instagram, etc.) use to exhibit, share, and license their work directly to their audience so that they can make a living doing what they love.
            </td>
          </tr>

          <tr>
            <td>
              Enjoy your journey, <br>
              The Creatr Team
            </td>
          </tr>

        </tbody>
      </table>
    </div>
  </div>
</body>

</html>
