@extends('layouts.app')

@section('content')
    <div id="app">
        @if( empty($collections))
            <create-reel :tags="{{$tags}}" :categories = "{{$categories}}"></create-reel>
        @else()
            <create-reel :tags="{{$tags}}" :categories = "{{$categories}}" :collections="{{$collections}}"></create-reel>
        @endif

    </div>
@endsection
