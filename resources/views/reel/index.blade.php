@extends('layouts.app')

@section('content')
    <div id="app">

        <reel-list :most-popular-reel="{{$mostPopularReel}}" :reel-basic-info="{{$reelsInfo}}"></reel-list>
    </div>
@endsection
