@extends('layouts.app')

@section('content')
    <div id="app">

        <edit-reel
            :tags="{{$tags}}"
            :categories = "{{$categories}}"
            :reel-selected-tags = "{{$selectedTags}}"
            :reel="{{$reel}}"
            :collections = "{{$collections}}"
        ></edit-reel>

    </div>
@endsection
