@extends('layouts.app')

@section('content')

    <div id="app">
        <div class="container pt-10">
            {{ Breadcrumbs::render('reel', $reel) }}
        </div>

        <display-reel :reel="{{$reel}}"
                      :tags="{{$tags}}"
                      :creator-info="{{$creator_info}}"
                      :video-category="{{$category}}"
                      :collections="{{$collections}}"
                      :comments = "{{$formatComment}}"
        ></display-reel>
    </div>
@endsection
