@extends('layouts.app')

@section('content')
<div id="app">
    <div class="container-fluid pt-10">
        {{ Breadcrumbs::render('creators') }}
    </div>
    <creators-list></creators-list>
</div>
@endsection