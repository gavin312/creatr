module.exports = {
	error:'',
	isJSON : function (str){
		if (typeof str == 'string') {
			try {
				var obj=JSON.parse(str);
				if(typeof obj == 'object' && obj ){
					return true;
				}else{
					return false;
				}
			} catch(e) {

				return false;
			}
		}
	},
	isNumber : function (checkVal){
		var reg = /^-?[1-9][0-9]?.?[0-9]*$/;
		return reg.test(checkVal);
	},
	isEmail : function (checkVal){
		var reg = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
		return reg.test(checkVal)
	},

	passwordPass:function(password){

        let pass1 = (password.length >=8)
        let pass2 = new RegExp("[a-z]").test(password)
        let pass3 = new RegExp("[A-Z]").test(password)
        let pass4 = new RegExp("[0-9]").test(password)
        let pass5 = new RegExp("[$@$!%*#?&._]").test(password)

		return (pass1&&pass2&&pass3&&pass4&&pass5)
	},
	isAustralianPhoneNumber:function(checkVal){
		var reg =  /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/;
		return reg.test(checkVal)
	},
    isAmericanPhoneNumber:function(checkVal){
	    var reg = /^(\([0-9]{3}\) |[0-9]{3})[0-9]{3}[0-9]{4}$/;
	    return reg.test(checkVal)
    },
    isAmericanAccountNumber:function(checkVal){
        var reg = /^[0-9]{7,14}$/;
        return reg.test(checkVal)
    },
    isAmericanRoutingNumber:function(checkVal){
        var reg = /^[0-9]{9,9}$/;
        return reg.test(checkVal)
    },
    isAmericanSSNValid:function(checkVal){
        var reg = /[0-9]{3}-[0-9]{2}-[0-9]{4}$/;
        return reg.test(checkVal)
    },
    isAmericanPostalCodeValid:function (checkVal){
        var reg =  /^[0-9]{5}$/;
        return reg.test(checkVal)
    },
    isAValidName:function (checkVal) {
        var reg = RegExp("^[a-z0-9_-]{6,15}$");
        return reg.test(checkVal)
    }
}
