/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";
import "vue-multiselect/dist/vue-multiselect.min.css"
import * as VueSpinnersCss from "vue-spinners-css";
import VueAwesomeSwiper from "vue-awesome-swiper";
import "swiper/swiper-bundle.css";
import VueScrollbar from "vue-scrollbar-live";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import { isMobile, isTablet } from "mobile-device-detect";
import VueLazyload from "vue-lazyload";
import Vue from "vue";
import Multiselect, {multiselectMixin} from "vue-multiselect";

require("./bootstrap");

window.Vue = require("vue").default;

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.use(VueLazyload);
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
Vue.use(Multiselect);
Vue.use(VueToast);
Vue.use(VueSpinnersCss);
Vue.use(VueAwesomeSwiper);
Vue.use(require("vue-resource"));
Vue.component("pagination", require("laravel-vue-pagination"));
Vue.component("scrollbar", VueScrollbar);
Vue.component(
  "example-component",
  require("./components/ExampleComponent.vue").default
);
Vue.component(
  "register-component",
  require("./components/MegaSolutionRegisterComponent/MegaSolutionRegisterComponent.vue")
    .default
);
Vue.component(
  "login-component",
  require("./components/MegaSolutionLoginComponent/MegaSolutionLoginComponent.vue")
    .default
);
Vue.component(
  "reset-password",
  require("./components/MegaSolutionResetPasswordComponent/MegaSolutionResetPasswordComponent.vue")
    .default
);
Vue.component(
  "creatr-footer",
  require("./components/MegaSolutionFooterComponent/MegaSolutionFooterComponent.vue")
    .default
);
Vue.component(
  "profile-menu",
  require("./components/MegaSolutionProfileDropDownMenu/MegaSolutionProfileDropDownMenu.vue")
    .default
);
Vue.component(
  "profile-mobile-menu",
  require("./components/MegaSolutionMobileProfileMenu/MegaSolutionMobileProfileMenu.vue")
    .default
);
Vue.component(
  "edit-profile",
  require("./components/MegaSolutionEditProfileComponent/MegaSolutionEditProfileComponent.vue")
    .default
);
Vue.component(
  "upload-video",
  require("./components/MegaSolutionUploadVideoComponent/MegaSolutionUploadVideoComponent.vue")
    .default
);
Vue.component(
  "video-player",
  require("./components/MegaSolutionViemoPlayer/MegaSolutionViemoPlayer.vue")
    .default
);
Vue.component(
  "create-reel",
  require("./components/MegaSolutionCreateReelComponent/MegaSolutionCreateReelComponent.vue")
    .default
);
Vue.component(
  "edit-reel",
  require("./components/MegaSolutionEditReelComponent/MegaSolutionEditReelComponent.vue")
    .default
);

Vue.component(
  "drag-video",
  require("./components/MegaSolutionDropZone/MegaSolutionDropZone.vue").default
);
Vue.component(
  "display-reel",
  require("./components/MegaSolutionReelDetailComponent/MegaSolutionReelDetailComponent.vue")
    .default
);
Vue.component(
  "display-profile",
  require("./components/MegaSolutionProfileDetailComponent/MegaSolutionProfileDetailComponent.vue")
    .default
);

Vue.component(
  "edit-collection",
  require("./components/MegaSolutionEditCollectionComponent/MegaSolutionEditCollectionComponent.vue")
    .default
);
Vue.component(
  "display-collection",
  require("./components/MegaSolutionCollectionDetailComponent/MegaSolutionCollectionDetailComponent.vue")
    .default
);
Vue.component(
  "upload-list",
  require("./components/MegaSolutionUploadListComponent/MegaSolutionUploadListComponent")
    .default
);

Vue.component(
  "display-tag",
  require("./components/MegaSolutionShowTagComponent/MegaSolutionShowTagComponent")
    .default
);
Vue.component(
  "display-category",
  require("./components/MegaSolutionShowCategoryComponent/MegaSolutionShowCategoryComponent")
    .default
);
Vue.component(
  "home-component",
  require("./components/MegaSolutionHomeComponent/MegaSolutionHomeComponent.vue")
    .default
);
Vue.component(
  "categories-list",
  require("./components/MegaSolutionCategoriesListComponent/MegaSolutionCategoriesListComponent")
    .default
);
Vue.component(
  "signup-view",
  require("./components/MegaSolutionSignUpViewComponent/MegaSolutionSignUpViewComponent")
    .default
);
Vue.component(
  "search-container",
  require("./components/MegaSolutionSearchComponent/MegaSolutionSearchComponent")
    .default
);
Vue.component(
  "search-page",
  require("./components/MegaSolutionSearchPageComponent/MegaSolutionSearchPageComponent")
    .default
);
Vue.component(
  "reel-list",
  require("./components/MegaSolutionReelListViewComponent/MegaSolutionReelListViewComponent")
    .default
);
Vue.component(
  "collection-list",
  require("./components/MegaSolutionCollectionListViewComponent/MegaSolutionCollectionListViewComponent")
    .default
);

Vue.component(
  "creators-list",
  require("./components/MegaSolutionCreatorsListViewComponent/MegaSolutionCreatorsListViewComponent")
    .default
);

Vue.component(
  "display-wishlist",
  require("./components/MegaSolutionShowWishlistComponent/MegaSolutionShowWishlistComponent")
    .default
);

Vue.component(
    "notification-component",
    require("./components/MegaSolutionNotificationComponent/MegaSolutionNotificationComponent")
        .default
);

Vue.component(
    "cart-component",
    require("./components/MegaSolutionCartComponent/MegaSolutionCartComponent")
        .default
);

Vue.component(
    "checkout-view",
    require("./components/MegaSolutionCheckOutViewComponent/MegaSolutionCheckOutViewComponent")
        .default
)


Vue.component(
    "thankyou-view",
    require("./components/MegaSolutionOrderThankYouPageComponent/MegaSolutionOrderThankYouPageComponent")
        .default
)

Vue.component(
    "order-history-view",
    require("./components/MegaSolutionOrderHistoryPageComponent/MegaSolutionOrderHistoryPageComponent")
        .default
)

Vue.component(
    "invoice-view",
    require("./components/MegaSolutionInvoiceComponent/MegaSolutionInvoiceComponent")
        .default
)

Vue.component(
  "transaction-history-view",
  require("./components/MegaSolutionTransactionHistoryPageComponent/MegaSolutionTransactionHistoryPageComponent")
      .default
)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: "#app",

});
