<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use App\Mail\WelcomeMail;
use App\Http\Controllers\web;
use App\Mail\GeneralMail;
use App\Mail\OrderMail;
use Illuminate\Support\Facades\Mail;
use App\Jobs\SendGeneralEmailJob;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function () {
    return view('test');
});

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/email', function () {
    return new WelcomeMail("Gavin");
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/reporting-reels/solve',[\App\Http\Controllers\Voyager\ReportingReelController::class, "solve"])->name('reporting-reels.solve');
    Route::get('/reporting-reels/remove',[\App\Http\Controllers\Voyager\ReportingReelController::class, "remove"])->name('reporting-reels.remove');
    Route::get('/reporting-reels/review',[\App\Http\Controllers\Voyager\ReportingReelController::class, "review"])->name('reporting-reels.review');
    Route::get('/retryTransaction/{transaction}',[web\PayoutTransactionController::class,"redoTransaction"])->name('refreshTransaction');
    Voyager::routes();
});


Route::get('/login/google', [\App\Http\Controllers\Auth\LoginController::class, "redirectToGoogle" ]);
Route::get('/login/google/callback', [\App\Http\Controllers\Auth\LoginController::class, "handleGoogleCallback" ]);

Route::get('/login/facebook', [\App\Http\Controllers\Auth\LoginController::class, "redirectToFacebook" ]);
Route::get('/login/facebook/callback', [\App\Http\Controllers\Auth\LoginController::class, "handleFacebookCallback" ]);

Route::get('/login/snapchat', [\App\Http\Controllers\Auth\LoginController::class, "redirectToSnapChat" ]);
Route::get('/login/snapchat/callback', [\App\Http\Controllers\Auth\LoginController::class, "handleSnapChatCallback" ]);

Auth::routes(['verify' => true]);

Route::prefix('home')->group(function () {
//    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/getItemByPage', [App\Http\Controllers\HomeController::class, 'getItemByPage'])->name('home.getItemByPage');
    Route::get('/getPopularCreators',[\App\Http\Controllers\HomeController::class, 'getPopularCreators'])->name('home.getPopularCreators');
});
Route::prefix('banners')->group(function () {
    Route::get('/', [web\BannerController::class, 'index'])->name('banners.index');
});

Route::get('/hq/{user_name}',[web\ProfileController::class,'show'])->name('showProfile');
Route::get('/hq/{user_name}/edit',[web\ProfileController::class,'edit'])->name('editProfile');


Route::post('/uploadAvatar', [web\FileController::class, 'uploadUserAvatar'])->name('uploadUserAvatar');
Route::post('/uploadVideo', [web\FileController::class, 'uploadVideo'])->name('uploadVideo');
Route::post('/uploadThumbNailImage', [web\FileController::class, 'uploadThumbNailImage'])->name('uploadThumbNailImage');
Route::post('/uploadTitleLogoImage', [web\FileController::class, 'uploadTitleLogoImage'])->name('uploadTitleLogoImage');


Route::post('/creator/profile/updateProfileSection', [web\ProfileController::class, 'updateProfileSection'])->name('updateProfileSection');
Route::post('/creator/profile/updateProfileSocialLink', [web\ProfileController::class, 'updateProfileSocialLink'])->name('updateProfileSocialLink');
Route::post('/creator/profile/changePassword', [web\ProfileController::class, 'changePassword'])->name('changePassword');
Route::post('/creator/profile/changeEmail', [web\ProfileController::class, 'changeEmail'])->name('changeEmail');
Route::post('/creator/profile/changeNotificationSetting', [web\ProfileController::class, 'changeNotificationSetting'])->name('changeNotificationSetting');

Route::get('/creator/getReelsByCreator',[web\ProfileController::class,'getReelsByCreator'])->name('getReelsByCreator');
Route::get('/creator/getCollectionsByCreator',[web\ProfileController::class,'getCollectionsByCreator'])->name('getCollectionsByCreator');

//reels page
Route::get('/reels/create',[web\ReelController::class,'create'])->name('createReel');
Route::post('/reels/store',[web\ReelController::class,'store'])->name('storeReel');
Route::delete('/reels/delete/{reel}',[web\ReelController::class,'destroy'])->name('deleteReel');
Route::get('/reels/edit/{reel_url_handle}',[web\ReelController::class,'edit'])->name('editReel');
Route::post('/reels/likeReel',[web\ReelController::class,'likeReel'])->name('likeReel');
Route::get('/reels/getMostPopularReels',[web\ReelController::class,'getMostPopularReels'])->name("getMostPopularReels");
Route::get('/reels/getMostPopularReelsWithTags',[web\ReelController::class,'getMostPopularReelsWithTags'])->name("getMostPopularReelsWithTags");
Route::get("/reels/getAllReelsByRandomOrder",[web\ReelController::class,'getAllReelsByRandomOrder'])->name("getAllReelsByRandomOrder");
Route::put('/reels/{reel}',[web\ReelController::class,'update'])->name('updateReel');
Route::get('/reels/{reel_url_handle}',[web\ReelController::class,'show'])->name('showReel');
Route::get('/reels',[web\ReelController::class,'index'])->name('showReelList');

//Follow and un follow
Route::post('/creator/follow',[web\FollowController::class,'follow'])->name('followUser');
Route::post('/creator/unfollow',[web\FollowController::class,'unfollow'])->name('unfollowUser');

//Collections Page

Route::post('/collections/store',[web\CollectionController::class,'store'])->name('storeCollection');
Route::get('/collections/edit/{collection_url_handle}',[web\CollectionController::class,'edit'])->name('editCollection');
Route::delete('/collections/delete/{collection}',[web\CollectionController::class,'destroy'])->name('showCollection');
Route::post('/collections/likeCollection',[web\CollectionController::class,'likeCollection'])->name('likeCollection');
Route::get('/collections/getAllCollectionsByRandomOrder',[web\CollectionController::class,'getAllCollectionsByRandomOrder'])->name("getAllCollectionsByRandomOrder");
Route::put('/collections/{collection}',[web\CollectionController::class,'update'])->name('updateCollection');
Route::get('/collections/{collection_url_handle}',[web\CollectionController::class,'show'])->name('showCollection');
Route::get('/collections',[web\CollectionController::class,'index'])->name('showCollectionList');

// category
Route::get('/categories/getCategories',[web\CategoryController::class,'getCategories'])->name('getCategories');
Route::get('/categories',[web\CategoryController::class,'index'])->name('categoryList');
Route::get('/categories/getCategoryReels',[web\CategoryController::class,'getCategoryReels'])->name('getCategoryReels');
Route::get('/categories/getCategoryCollections',[web\CategoryController::class,'getCategoryCollections'])->name('getCategoryCollections');
Route::get('/categories/{category_title}',[web\CategoryController::class,'show'])->name('displayCategory');
//User
Route::get('/creator/getUserLoginStatus',[web\UserController::class,'getUserLoginStatus'])->name('getUserLoginStatus');
Route::get('/creator/getReels',[web\UserController::class,'getUserReels'])->name('getUserReels');
Route::get('/creator/getCollections',[web\UserController::class,'getUserCollections'])->name('getUserCollections');
Route::get('/creator/getRandomReelsOtherThan',[web\UserController::class,'getrandomReelsOtherThan']) ->name("getrandomReelsOtherThan");

// Creators
Route::get('/creators/getCreators',[web\CreatorController::class, 'getCreators'])->name('getCreators');
Route::get('/creators', [web\CreatorController::class, 'index'])->name('showCreatorsListView');

//Comment
Route::post('/comments/store',[web\CommentController::class,'store'])->name('storeComment');
Route::get('/comments',[web\CommentController::class,'index'])->name('getComments');


Route::get('/uploads/',[web\UploadController::class,'index'])->name('getUploadList');
Route::get('/uploads/getUserUploadReels',[web\UserController::class,"getUserUploadReels"])->name("getUserUploadReels");
Route::get('/uploads/getUserUploadCollections',[web\UserController::class,"getUserUploadCollections"])->name("getUserUploadCollections");


// Tags
Route::get('/tag/getTagReels',[web\TagController::class,'getTagReels'])->name('getTagReels');
Route::get('/tag/getTagCollections',[web\TagController::class,'getTagCollections'])->name('getTagCollections');
Route::get('/tags/{tag_title}',[web\TagController::class,'show'])->name('showTag');

//Search
Route::get('/search',[web\SearchController::class,'show'])->name('showSearch');
Route::get('/search/getReels',[web\SearchController::class,'getReels'])->name('searchReels');
Route::get('/search/getCollections',[web\SearchController::class,'getCollections'])->name('searchCollections');
Route::get('/search/getCreators',[web\SearchController::class,'getCreators'])->name('searchCreators');


// Wishlist
Route::get('/wishlist',[web\WishlistController::class,'index'])->name('showWishlist');
// Add reel to wishlist
Route::get('/wishlist/getItemFromWishlist',[web\WishlistController::class,'getItemWishlistStatus'])->name('getItemWishlistStatus');
Route::post('/wishlist/addToWishlist', [web\WishlistController::class, 'addToWishlist'])->name('addToWishlist');
Route::post('/wishlist/removeFromWishlist', [web\WishlistController::class, 'removeFromWishlist'])->name('removeFromWishlist');
Route::get('/wishlist/getWishlistedReelsFromWishlist', [web\WishlistController::class, 'getWishlistedReelsFromWishlist'])->name('getWishlistedReelsFromWishlist');
Route::get('/wishlist/getWishlistedCollectionsFromWishlist', [web\WishlistController::class, 'getWishlistedCollectionsFromWishlist'])->name('getWishlistedCollectionsFromWishlist');


Route::get('/phpinfo',function(){
    return view('phpinfo');
})->name('getPhpInfo');

Route::get('/download/testvideo',function(){
    return Redirect::to("https://player.vimeo.com/play/2815160731?s=599088994_1633394343_3c5bb9d73deee613121f0c4e8d2db110&loc=external&context=Vimeo%5CController%5CApi%5CResources%5CVideoController.&download=1&filename=1630960200_creatr-promo-reel-01-final.mp4164.mp4");
});

//Rates
Route::get('/rate/getAllRates',[web\RateController::class,'getAllRates'])->name('getAllRates');

//Licences
Route::get('/licence/getAllLicences',[web\LicenceController::class,'getAllLicences'])->name('getAllLicences');

//Platforms
Route::get('/platform/getAllPlatforms',[web\PlatformController::class,'getAllPlatforms'])->name('getAllPlatforms');

//CART

Route::prefix('cart')->group(function () {
    Route::post('/addItemToCart',[web\CartController::class,'addItemToCart'])->name('addItemToCart');
    Route::get('/getCartItems',[web\CartController::class,'getCartItems'])->name('getCartItems');
    Route::post('/editCartItem',[web\CartController::class,'editCartItem'])->name('editCartItem');
    Route::delete('/deleteCartItem/{cartItem}',[web\CartController::class,'deleteCartItem'])->name('deleteCartItem');
});


Route::prefix('checkout')->group(function(){
    Route::get('/',[web\CheckoutController::class,'show'])->name('showCheckout');
    Route::get('/verifyItems',[web\CheckoutController::class,'verifyItems'])->name('verifyItems');
    Route::post('/checkOutWithStripeToken',[web\CheckoutController::class,'checkOutWithStripeToken'])->name('checkOutWithStripeToken');
    Route::post('/addPaymentMethod',[web\CheckoutController::class,'addPaymentMethod'])->name('addPaymentMethod');
});


Route::prefix('order')->group(function(){
    Route::get('/',[web\OrderController::class,'show'])->name('showOrderHistory');
    Route::get('/thankyou/{order_number}',[web\OrderController::class,'showThankyou'])->name('showThankyou');
    Route::get('/myOrders',[web\OrderController::class,'getMyOrders'])->name('getMyOrders');
    Route::get('/getMyOrdersAllCreators',[web\OrderController::class,'getMyOrdersAllCreators'])->name('getMyOrdersAllCreators');
    Route::get('/paymentcomplete',[web\CheckoutController::class,'paymentComplete'])->name('paymentComplete');

});

Route::prefix('transaction')->group(function() {
    Route::get('/',[web\PayoutTransactionController::class,'show'])->name('showTransactionHistory');
    Route::get('/myPayoutTransactions',[web\PayoutTransactionController::class,'getMyPayoutTransactions'])->name('getMyPayoutTransactions');
    Route::get('/myPayoutRevenue', [web\PayoutTransactionController::class,'getMyPayoutRevenue'])->name('getMyPayoutRevenue');
    Route::get('/getMyTransactionAllTypes', [web\PayoutTransactionController::class,'getMyTransactionAllTypes'])->name('getMyTransactionAllTypes');
    Route::put('/remakeTransaction/{transaction}',[web\PayoutTransactionController::class,'remakeTransaction'])->name("remakeTransactionByCreatr");
});

Route::prefix('paymentmethods')->group(function(){
    Route::get('/getUserPaymentMethods',[web\PaymentMethodController::class,'getPaymentInfo'])->name('getPaymentInfo');
    Route::put('/setDefaultCard',[web\PaymentMethodController::class,'setupDefaultPaymentMethod'])->name('setupDefaultPaymentMethod');
    Route::delete('/deletePaymentMethod',[web\PaymentMethodController::class,'deletePaymentMethod'])->name('deletePaymentMethod');
    Route::post('/addPaymentMethod',[web\PaymentMethodController::class,'addPaymentMethod'])->name('addPaymentMethod');
});

Route::prefix('reporting')->group(function(){
    Route::post('/reportReel',[web\ReportingController::class,'reportReel'])->name('reportReel');
    Route::post('/reportCollection',[web\ReportingController::class,'reportCollection'])->name('reportCollection');
    Route::get('/getAllReportingTypes',[web\ReportingController::class,'getAllReportingTypes'])->name('getAllReportingTypes');
});
Route::prefix('invoice')->group(function(){
    Route::get("/orders/{order_number}",[web\InvoiceController::class,'validateOrderStatus'])->name('validateOrderStatus');
    Route::get("/edit/{invoiceId}",[web\InvoiceController::class,'edit'])->name('editInvoice');
    Route::get("/show/{invoiceId}",[web\InvoiceController::class,'show'])->name('showInvoice');
    Route::post("/saveAndDownload",[web\InvoiceController::class,'saveAndDownload'])->name('saveAndDownload');
    Route::get("/download/{invoice}",[web\InvoiceController::class,'downloadInvoice'])->name('saveAndDownload');
});

Route::prefix('orderItemLicence')->group(function(){
    Route::get("/show/{orderItem}",[web\LicenceController::class,'getOrderItemLicence'])->name('getOrderItemLicence');
});

Route::prefix('orderItemDownload')->group(function(){
    Route::get("/download/{orderItem}",[web\OrderItemController::class,'downloadReel'])->name('downloadOrderItem');
    Route::get("/downloadFreeReel/{reel}",[web\OrderItemController::class,'downloadFreeReel'])->name('downloadFreeReel');
});

Route::prefix('payout')->group(function(){
    Route::post("/create",[web\PayoutController::class,'createAccount'])->name('createPayoutAccount');
    Route::put("/update",[web\PayoutController::class,'updateAccount'])->name('updatePayoutAccount');
    Route::get("/getAccount",[web\PayoutController::class,'getPayoutAccount'])->name('getPayoutAccount');
    Route::get("/getAccountDetail",[web\PayoutController::class,'getPayoutAccountDetail'])->name('getPayoutAccountDetail');
    Route::post("/uploadVefiedFile",[web\PayoutController::class,'uploadVefiedFile'])->name('uploadVefiedFile');
});
