<?php // routes/breadcrumbs.php

// Note: Laravel will automatically resolve `Breadcrumbs::` without
// this import. This is nice for IDE syntax and refactoring.
use App\Models\Collection;
use App\Models\Reel;
use Diglactic\Breadcrumbs\Breadcrumbs;

// This import is also not required, and you could replace `BreadcrumbTrail $trail`
//  with `$trail`. This is nice for IDE type checking and completion.
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

// Home
Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('Home', route('home'));
});

Breadcrumbs::for("reels",function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push("Reels", route('showReelList'));
});

Breadcrumbs::for('reel', function (BreadcrumbTrail $trail, Reel $reel) {
    $trail->parent('reels');
    $trail->push($reel->title, route('showReel', $reel));
});


Breadcrumbs::for("collections",function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push("Collections", route('showCollectionList'));
});

Breadcrumbs::for('collection', function (BreadcrumbTrail $trail, Collection $collection) {
    $trail->parent('collections');
    $trail->push($collection->title, route('showCollection', $collection));
});

Breadcrumbs::for('creators', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push("All creators", route('showCreatorsListView'));
});
