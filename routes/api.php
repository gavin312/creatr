<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\v1\UserController;
use App\Library\MailerliteHelper;
use \App\Http\Controllers\web;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::prefix('/user')->group(function(){
    Route::get('/checkNameAvailable','App\Http\Controllers\api\v1\UserController@checkNameAvailable');
    Route::get('/checkNewNameAvailable','App\Http\Controllers\api\v1\UserController@checkNewNameAvailable');
});

Route::get('/mailgroups',function (){
    $mailLiterHelper = new MailerliteHelper();
    return $mailLiterHelper->getSubscriptionGroups();
});

Route::post('/addSubscriber',function (){
    $mailLiterHelper = new MailerliteHelper();
    $email = request()->get('email');
    return $mailLiterHelper->subscribeCreatr($email);
});

Route::get('/checkVideoUploadedStatus','App\Http\Controllers\web\FileController@checkVideoUploadedStatus');

//uploadThumbNailImage
Route::get('/getThumbNailFromVideoId','App\Http\Controllers\web\FileController@getThumbNailFromVideoId');

Route::get('/getPreviewFromVideoId','App\Http\Controllers\web\FileController@getPreviewFromVideoId');
//
//Route::get('/createPreviewFromVideoID','App\Http\Controllers\web\FileController@createPreviewFromVideoID');

Route::post('/uploadThumbnailByBase64','App\Http\Controllers\web\FileController@uploadThumbnailByBase64');

//Route::get('/convertName',function (){
//
//    $originalString = \request()->get("from");
//
//    $finalString = \App\Library\Helper::convertNameToHandle($originalString);
//    return response()->json([
//        'to'=>$finalString,
//    ],200);
//});

Route::get('/gavintest',function(){

    return public_path();
});


//Route::get('/updateReelsHandle',function (){
//    $reels = \App\Models\Reel::all();
//    $reelController = new \App\Http\Controllers\web\ReelController();
//    foreach ($reels as $reel){
//        $reelController->getReelHandleFromReelTitle($reel);
//    }
//    return response()->json([
//        'status'=>true,
//    ],200);
//});
//
//
//Route::get('/updateCollectionsHandle',function (){
//    $collections = \App\Models\Collection::all();
//    $collectionController = new \App\Http\Controllers\web\CollectionController();
//    foreach ($collections as $collection){
//        $collectionController->getCollectionHandleFromCollectionTitle($collection);
//    }
//    return response()->json([
//        'status'=>true,
//    ],200);
//});


Route::get('/getUserInfo',function(){

    $user_id = \request()->get('user_id');
    $user = \App\Models\User::find($user_id);
    return response()->json([
        'user'=>$user,
        'followers'=>$user->getFollowers(),
        'followings'=>$user->getFollowings(),
    ],200);

});

Route::get('isFollowing',function (){
   $follower_id = \request()->get('follower_id');
   $following_id = \request()->get('following_id');
   return \App\Library\Helper::isFollowing($follower_id,$following_id);
});

Route::get("getPreviewsFromVideoId",'App\Http\Controllers\web\FileController@getPreviewsFromVideoId' );

Route::get("getReelDetailFromReelID",[App\Http\Controllers\web\ReelController::class,"getReelDetailfromVimeoByVideoID"]);

//Route::get('createPreviewFromPullApproach','App\Http\Controllers\web\FileController@createPreviewFromPullApproach');


Route::get("createTestAccount",[web\PayoutController::class,"createTestAccount"]);

Route::get("testGenerateInvoice",[web\CheckoutController::class, "generateInvoiceFromOrder"]);

Route::get("removeSubscriberByEmail",function(){

    $helper = new MailerliteHelper();
    return $helper->removeSubscriberByEmail("salamender336@gmail.com");
});


Route::get("sendTestEmailToGavin",function(){
    $user = \App\Models\User::find(25);
    $user->notify(new \App\Notifications\GeneralMessage("abcdefg"));
});


Route::get("testPayout",function (){


    $user = \App\Models\User::find(25);
    $payoutMethod = $user->payoutMethods()->first();

    $stripe = new \Stripe\StripeClient(
        env("STRIPE_SECRET")
    );
   $response =  $stripe->transfers->create([
        'amount' => 400,
        'currency' => 'usd',
        'destination' => "acct_1Jxym0RX6qBqXNWQ",
    ]);
    return $response;
});

Route::get("testOrderGroup",function(){
    $order = \App\Models\Order::find(58);
    $transactions = $order->payoutTransactions()->groupBy()->get();
    return response()->json([
        'transactions'=>$transactions,
    ],200);

});
